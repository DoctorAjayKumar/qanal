% @doc
%       Every operation, calculation, and concept, no matter how
%       arbitrarily complex, reduces to adding integers together.
%       There are no new concepts in QAnal. Everything is just
%       putting lipstick on adding integers together.
%
%   -- Dr. Ajay Kumar PHD, Founder of QAnal
%
% behavior module for rings
%
% not quite rings, but eh
% @end
-module(qanal_ring).

% copy
% -export([additive_inverse/1, conj/1, plus/2, one/0, times/2, zero/0]).
% -export([ai/1, minus/2, pow/2, prod/1, sq/1, sum/1]).
% mt = module type
-type mt() :: any().

-callback additive_inverse(mt()) -> mt().
-callback conj(mt())             -> mt().
-callback one()                  -> mt().
-callback plus(mt(), mt())       -> mt().
-callback times(mt(), mt())      -> mt().
-callback zero()                 -> mt().

% derivable
% alias for additive inverse
-callback ai(mt())                     -> mt().
-callback minus(mt(), mt())            -> mt().
-callback pow(mt(), non_neg_integer()) -> mt().
-callback prod([mt()])                 -> mt().
-callback sq(mt())                     -> mt().
-callback sum([mt()])                  -> mt().

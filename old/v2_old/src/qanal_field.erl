% @doc
%       Every operation, calculation, and concept, no matter how
%       arbitrarily complex, reduces to adding integers together.
%       There are no new concepts in QAnal. Everything is just
%       putting lipstick on adding integers together.
%
%   -- Dr. Ajay Kumar PHD, Founder of QAnal
%
% QAnal field is a QAnal ring that also has division
% @end
-module(qanal_field).

-type mt() :: any().

%-export([multiplicative_inverse/1]).
%-export([divide/2, mi/1]).

% minimal
-callback multiplicative_inverse(mt()) -> mt().

% derived
-callback divide(mt(), mt()) -> mt().
-callback mi(mt()) -> mt().

% @doc
%       Every operation, calculation, and concept, no matter how
%       arbitrarily complex, reduces to adding integers together.
%       There are no new concepts in QAnal. Everything is just
%       putting lipstick on adding integers together.
%
%   -- Dr. Ajay Kumar PHD, Founder of QAnal
%
% qx_m: [Q]Anal [X]Anal [M]atrices
%
% {rc, R, C} is 1-indexed in both entries
% @end
-module(qx_m).

% types
-export_type([m/0,
              idx0/0, idx1/0,
              rc/0,
              row/0, col/0,
              shape/0]).

%% getters and setters
-export([shape/1, arith_mod/1, arr/1]).
-export([rcth/2, rcput/3,
         cth/2,  cput/3,
         rth/2,  rput/3]).

% constructors
-export([zeros/2, zeros_sq/2,
         idn/2]).
%-export([idn/2, from_diag_list/1, from_lists/1, to_lists/1]).

% conversion
-export([from_col_lists/1, to_col_lists/1,
         from_row_lists/1, to_row_lists/1]).

% arithmetic
-export([plus/2, times/2]).
-export([pow/2]).
-export([ai/1, minus/2]).

%% arrayish
-export([map/2, foldl/3]).
-export([transpose/1]).
% TODO: matrix equals
%-export([equals/2]).

%% helpers
-export([rc_to_idx0/2, idx0_to_rc/2]).

% main type
-record(m,
        {shape     :: shape(),
         arith_mod :: atom(),
         arr       :: array()}).
-type m() :: #m{}.

% extraneous types
-type col()   :: v().
-type idx0()  :: non_neg_integer().
-type idx1()  :: pos_integer().
-type rc()    :: {rc, idx1(), idx1()}.
-type row()   :: v().
-type shape() :: {shape, pos_integer(), pos_integer()}.

% semantic types
-type array()     :: array:array().
-type arith_mod() :: atom().
-type entry()     :: term().
-type v()         :: qx_v:v().

%% typespecs
% getters/setters
% low level
-spec shape(m()) -> shape().
-spec arith_mod(m()) -> arith_mod().
-spec arr(m()) -> array().
% get/set specific element
-spec rcth(rc(), m()) -> entry().
-spec rcput(rc(), entry(), m()) -> m().
%% get/set col
-spec cth(idx1(), m()) -> v().
-spec cth_list(idx1(), m()) -> [entry(), ...].
-spec cput(idx1(), row(), m()) -> m().
% get/set row
-spec rth(idx1(), m()) -> v().
-spec rth_list(idx1(), m()) -> [entry(), ...].
-spec rput(idx1(), row(), m()) -> m().
% constructor
-spec zeros(shape(), arith_mod()) -> m().
-spec zeros_sq(pos_integer(), arith_mod()) -> m().
% conversions
-spec from_col_lists([[entry(), ...], ...]) -> m().
-spec to_col_lists(m()) -> [[entry(), ...], ...].
-spec from_row_lists([[entry(), ...], ...]) -> m().
-spec to_row_lists(m()) -> [[entry(), ...], ...].
% arithmetic
-spec plus(m(), m()) -> m().
-spec times(m(), m()) -> m().

% arrayish
-spec map(Fun, m()) -> m()
        when Fun      :: fun((RC, Val, ArithMod) -> NewVal),
             RC       :: rc(),
             Val      :: term(),
             ArithMod :: arith_mod(),
             NewVal   :: term().

-spec foldl(Fun, InitAcc, m()) -> Acc
        when Fun      :: fun((RC, Val, ArithMod, Acc) -> NewAcc),
             RC       :: rc(),
             Val      :: term(),
             ArithMod :: arith_mod(),
             InitAcc  :: Acc,
             Acc      :: any(),
             NewAcc   :: Acc.
-spec transpose(m()) -> m().
% helpers
-spec rc_to_idx0(rc(), shape()) -> idx0().
-spec idx0_to_rc(idx0(), shape()) -> rc().

%% getters/setters
% low level
shape({m, Shape, _AM, _Arr}) ->
    Shape.

arith_mod({m, _Shape, AM, _Arr}) ->
    AM.

arr({m, _Shape, _AM, Arr}) ->
    Arr.

% get/set element
rcth(RC = {rc, R, C}, #m{shape = Shape = {shape, NR, NC}, arr = Arr})
            when (1 =< R), (R =< NR),
                 (1 =< C), (C =< NC)
            ->
    I0 = rc_to_idx0(RC, Shape),
    array:get(I0, Arr).

rcput(RC = {rc, R, C}, Val, M = #m{shape     = Shape = {shape, NR, NC},
                                   arith_mod = MatrAM,
                                   arr       = Arr})
            when (1 =< R), (R =< NR),
                 (1 =< C), (C =< NC)
            ->
    I0     = rc_to_idx0(RC, Shape),
    ValAM  = qanal:arith_mod(Val),
    true   = qanal:arith_mod_matches(ValAM, MatrAM),
    NewArr = array:set(I0, Val, Arr),
    Result = M#m{arr = NewArr},
    Result.

% get/set col
cth(ThisC1, Matr = {m, {shape, NR, NC}, MatrAM, _MatrArr})
            when (1 =< ThisC1), (ThisC1 =< NC)
            ->
    % -spec qx_v:map(Fun, v()) -> v()
    %         when Fun      :: fun((Idx1, Val, ArithMod) -> NewVal),
    %              Idx1     :: idx1(),
    %              Val      :: term(),
    %              ArithMod :: arith_mod(),
    %              NewVal   :: term().
    VectZeros = qx_v:zeros(NR, MatrAM),
    Mapper =
        fun(ThisR1, _Zero, _ArithMod) ->
            ThisRC = {rc, ThisR1, ThisC1},
            ThisVal = rcth(ThisRC, Matr),
            ThisVal
        end,
    ThisRow = qx_v:map(Mapper, VectZeros),
    ThisRow.

cth_list(ThisC1, Matr) ->
    qx_v:to_list(cth(ThisC1, Matr)).

cput(ThisC1,
     RowVect = {v, VectLen,         VectAM, _VectArr},
     Matr    = {m, {shape, NR, NC}, MatrAM, _MatrArr})
        when (1 =< ThisC1), (ThisC1 =< NC),
             (VectLen =:= NR),
             VectAM =:= MatrAM
        ->
    % -spec qx_v:foldl(Fun, InitAcc, v()) -> Acc
    %         when Fun      :: fun((Idx1, Val, ArithMod, Acc) -> NewAcc),
    %              Idx1     :: idx1(),
    %              Val      :: term(),
    %              ArithMod :: arith_mod(),
    %              InitAcc  :: Acc,
    %              Acc      :: any(),
    %              NewAcc   :: Acc.
    PutVal =
        fun(ThisR1, ThisVectVal, _VectAM, AccumulatedMatrix) ->
            ThisRC = {rc, ThisR1, ThisC1},
            NewAccum = rcput(ThisRC, ThisVectVal, AccumulatedMatrix),
            NewAccum
        end,
    ResultMatr = qx_v:foldl(PutVal, Matr, RowVect),
    ResultMatr.

% get/set row
rth(ThisR1, Matr = {m, {shape, NR, NC}, MatrAM, _MatrArr})
            when (1 =< ThisR1), (ThisR1 =< NR)
            ->
    % -spec qx_v:map(Fun, v()) -> v()
    %         when Fun      :: fun((Idx1, Val, ArithMod) -> NewVal),
    %              Idx1     :: idx1(),
    %              Val      :: term(),
    %              ArithMod :: arith_mod(),
    %              NewVal   :: term().
    VectZeros = qx_v:zeros(NC, MatrAM),
    Mapper =
        fun(ThisC1, _Zero, _ArithMod) ->
            ThisRC = {rc, ThisR1, ThisC1},
            ThisVal = rcth(ThisRC, Matr),
            ThisVal
        end,
    ThisRow = qx_v:map(Mapper, VectZeros),
    ThisRow.

rth_list(ThisR1, Matr) ->
    qx_v:to_list(rth(ThisR1, Matr)).

rput(ThisR1,
     RowVect = {v, VectLen,         VectAM, _VectArr},
     Matr    = {m, {shape, NR, NC}, MatrAM, _MatrArr})
        when (1 =< ThisR1), (ThisR1 =< NR),
             (VectLen =:= NC),
             VectAM =:= MatrAM
        ->
    % -spec qx_v:foldl(Fun, InitAcc, v()) -> Acc
    %         when Fun      :: fun((Idx1, Val, ArithMod, Acc) -> NewAcc),
    %              Idx1     :: idx1(),
    %              Val      :: term(),
    %              ArithMod :: arith_mod(),
    %              InitAcc  :: Acc,
    %              Acc      :: any(),
    %              NewAcc   :: Acc.
    PutVal =
        fun(ThisC1, ThisVectVal, _VectAM, AccumulatedMatrix) ->
            ThisRC = {rc, ThisR1, ThisC1},
            NewAccum = rcput(ThisRC, ThisVectVal, AccumulatedMatrix),
            NewAccum
        end,
    ResultMatr = qx_v:foldl(PutVal, Matr, RowVect),
    ResultMatr.


%% constructors
zeros(Shape = {shape, NR, NC}, ArithMod) ->
    Zero = ArithMod:zero(),
    Size = NR * NC,
    Array = array:new([{default, Zero},
                       {size, Size},
                       {fixed, true}]),
    {m, Shape, ArithMod, Array}.

zeros_sq(NR = NC, ArithMod) ->
    zeros({shape, NR, NC}, ArithMod).


% @doc nxn identity matrix
idn(NR, ArithMod) ->
    % lists:foldl(Fun, Acc0, List) -> Acc1
    %        Types:
    %           Fun = fun((Elem :: T, AccIn) -> AccOut)
    %           Acc0 = Acc1 = AccIn = AccOut = term()
    %           List = [T]
    %           T = term()
    One = ArithMod:one(),
    Zeros = zeros_sq(NR, ArithMod),
    RCs = [{rc, N, N} || N <- lists:seq(1, NR)],
    Foldel =
        fun(DiagRC = {rc, N, N}, AccumulatedMatrix) ->
            % put a 1 along the diagonal
            NewAccum = rcput(DiagRC, One, AccumulatedMatrix),
            NewAccum
        end,
    Result = lists:foldl(Foldel, Zeros, RCs),
    Result.


%% conversion

% conversions: to/from col lists
from_col_lists(Cols = [FirstCol = [Val11 | _] | _]) ->
    NC       = length(Cols),
    NR       = length(FirstCol),
    ArithMod = qanal:arith_mod(Val11),
    Shape    = {shape, NR, NC},
    Zeros    = zeros(Shape, ArithMod),
    insert_cols(Cols, Zeros, 1).

% inserting a new col
insert_cols([ThisColLs | RestCols],
            ThisM = #m{shape = {shape, NR, NC}},
            ThisC1)
        when (1 =< ThisC1), (ThisC1 =< NC),
             (length(ThisColLs) =:= NR)
        ->
    ThisColVect = qx_v:from_list(ThisColLs),
    NewM        = cput(ThisC1, ThisColVect, ThisM),
    NewC1       = ThisC1 + 1,
    insert_cols(RestCols, NewM, NewC1);
% done inserting cols, we must also be at col MaxCol + 1}
insert_cols([], M = #m{shape = {shape, _NR, NC}}, ThisC1)
        when ThisC1 =:= NC + 1
        ->
    M.

to_col_lists(M) ->
    to_col_lists(M, 1, []).


to_col_lists(Matr = #m{shape = {shape, _NR, NC}},
             ThisC1,
             AccumCols)
        when (1 =< ThisC1), (ThisC1 < NC)
        ->
    ThisColLs    = cth_list(ThisC1, Matr),
    NewC1        = ThisC1 + 1,
    NewAccumCols = [ThisColLs | AccumCols],
    to_col_lists(Matr, NewC1, NewAccumCols);
to_col_lists(Matr = #m{shape = {shape, _NR, NC}},
             ThisC1,
             AccumCols)
        when ThisC1 =:= NC
        ->
    ThisColLs     = cth_list(ThisC1, Matr),
    NewAccumCols = [ThisColLs | AccumCols],
    lists:reverse(NewAccumCols).


% conversions: to/from row lists
from_row_lists(Rows = [FirstRow = [Val11 | _] | _]) ->
    NR       = length(Rows),
    NC       = length(FirstRow),
    ArithMod = qanal:arith_mod(Val11),
    Shape    = {shape, NR, NC},
    Zeros    = zeros(Shape, ArithMod),
    insert_rows(Rows, Zeros, 1).

% inserting a new row
insert_rows([ThisRowLs | RestRows],
            ThisM = #m{shape = {shape, NR, NC}},
            ThisR1)
        when (1 =< ThisR1), (ThisR1 =< NR),
             (length(ThisRowLs) =:= NC)
        ->
    ThisRowVect = qx_v:from_list(ThisRowLs),
    NewM        = rput(ThisR1, ThisRowVect, ThisM),
    NewR1       = ThisR1 + 1,
    insert_rows(RestRows, NewM, NewR1);
% done inserting rows, we must also be at row MaxRow + 1}
insert_rows([], M = #m{shape = {shape, NR, _NC}}, ThisR1)
        when ThisR1 =:= NR + 1
        ->
    M.

to_row_lists(M) ->
    to_row_lists(M, 1, []).


to_row_lists(Matr = #m{shape = {shape, NR, _NC}},
             ThisR1,
             AccumRows)
        when (1 =< ThisR1), (ThisR1 < NR)
        ->
    ThisRowLs    = rth_list(ThisR1, Matr),
    NewR1        = ThisR1 + 1,
    NewAccumRows = [ThisRowLs | AccumRows],
    to_row_lists(Matr, NewR1, NewAccumRows);
to_row_lists(Matr = #m{shape = {shape, NR, _NC}},
             ThisR1,
             AccumRows)
        when ThisR1 =:= NR
        ->
    ThisRowLs    = rth_list(ThisR1, Matr),
    NewAccumRows = [ThisRowLs | AccumRows],
    lists:reverse(NewAccumRows).

%% arithmetic
plus(ML = #m{shape = ShapeMatches, arith_mod = AM_matches},
     MR = #m{shape = ShapeMatches, arith_mod = AM_matches}) ->
    %-spec map(Fun, m()) -> m()
    %        when Fun      :: fun((RC, Val, ArithMod) -> NewVal),
    %             RC       :: rc(),
    %             Val      :: term(),
    %             ArithMod :: arith_mod(),
    %             NewVal   :: term().
    ArithMod = AM_matches,
    Mapper =
        fun(RC, ValL, _ArithMod) ->
            ValR = rcth(RC, MR),
            NewVal = ArithMod:plus(ValL, ValR),
            NewVal
        end,
    Result = map(Mapper, ML),
    Result.


times(ML = #m{shape = {shape, NR_L, NC_L},       arith_mod = AM_matches},
      MR = #m{shape = {shape,       NC_L, NC_R}, arith_mod = AM_matches}) ->
    %-spec map(Fun, m()) -> m()
    %        when Fun      :: fun((RC, Val, ArithMod) -> NewVal),
    %             RC       :: rc(),
    %             Val      :: term(),
    %             ArithMod :: arith_mod(),
    %             NewVal   :: term().
    ArithMod    = AM_matches,
    ResultShape = {shape, NR_L, NC_R},
    Zeros       = zeros(ResultShape, ArithMod),
    Mapper =
        fun({rc, ResultR1, ResultC1}, _Zero, _ArithMod) ->
            % Result[i,j] = row[i] dot col[j]
            RowL_R1     = rth(ResultR1, ML),
            ColR_C1     = cth(ResultC1, MR),
            ResultEntry = qx_v:ip(RowL_R1, ColR_C1),
            ResultEntry
        end,
    ResultMatrix = map(Mapper, Zeros),
    ResultMatrix.

pow(M = #m{shape = {shape, NR, NR}, arith_mod = AM}, N) ->
    IDN = idn(NR, AM),
    pow(M, N, IDN).

pow(_, 0, Accum) ->
    Accum;
pow(M, N, Accum) when is_integer(N), 1 =< N ->
    NewN     = N - 1,
    NewAccum = times(M, Accum),
    pow(M, NewN, NewAccum).

minus(X, Y) ->
    plus(X, ai(Y)).

ai(X) ->
    MinusOne =
        fun(_RC, Val, ArithMod) ->
            ArithMod:ai(Val)
        end,
    map(MinusOne, X).

%% arrayish

%-spec map(Fun, m()) -> m()
%        when Fun      :: fun((RC, Val, ArithMod) -> NewVal),
%             RC       :: rc(),
%             Val      :: term(),
%             ArithMod :: arith_mod(),
%             NewVal   :: term().
map(UsrFun, M = #m{shape = Shape, arith_mod = ArithMod, arr = Arr}) ->
    % UsrFun :: fun((RC, Val, ArithMod) -> NewVal),
    UsrFun2 =
        fun(I0, Val) ->
            RC = idx0_to_rc(I0, Shape),
            UsrFun(RC, Val, ArithMod)
        end,
    % array:map
    %   map(Function, Array :: array(Type1)) -> array(Type2)
    %     Types:
    %       Function = fun((Index :: array_indx(), Type1) -> Type2)
    NewArr = array:map(UsrFun2, Arr),
    NewMatr = M#m{arr = NewArr},
    NewMatr.

% -spec foldl(Fun, InitAcc, m()) -> Acc
%         when Fun      :: fun((RC, Val, ArithMod, Acc) -> NewAcc),
%              RC       :: rc(),
%              Val      :: term(),
%              ArithMod :: arith_mod(),
%              InitAcc  :: Acc,
%              Acc      :: any(),
%              NewAcc   :: Acc.
foldl(UsrFun, InitAcc, #m{shape = Shape, arith_mod = ArithMod, arr = Arr}) ->
    % UsrFun :: fun((RC, Val, ArithMod, Acc) -> NewAcc),
    UsrFun2 =
        fun(I0, Val, Acc) ->
            RC = idx0_to_rc(I0, Shape),
            NewAcc = UsrFun(RC, Val, ArithMod, Acc),
            NewAcc
        end,
    % array:foldl
    %  foldl(Function, InitialAcc :: A, Array :: array(Type)) -> B
    %         Types:
    %            Function =
    %                fun((Index :: array_indx(), Value :: Type, Acc :: A) -> B)
    Result = array:foldl(UsrFun2, InitAcc, Arr),
    Result.


transpose(Matrix = #m{shape = {shape, NR, NC}, arith_mod = AM}) ->
    TransShape = {shape, NC, NR},
    TransZeros = zeros(TransShape, AM),
    % -spec map(Fun, m()) -> m()
    %         when Fun      :: fun((RC, Val, ArithMod) -> NewVal),
    %              RC       :: rc(),
    %              Val      :: term(),
    %              ArithMod :: arith_mod(),
    %              NewVal   :: term().
    Mapper =
        fun({rc, TransR1, TransC1}, _Zero, _AM) ->
            OrigR1 = TransC1,
            OrigC1 = TransR1,
            OrigRC = {rc, OrigR1, OrigC1},
            Value  = rcth(OrigRC, Matrix),
            Value
        end,
    TransMatrix = map(Mapper, TransZeros),
    TransMatrix.


%% helpers
rc_to_idx0({rc, R1, C1}, {shape, _NR, NC}) ->
    % array:
    %       0 1 2 3     R0 = 0
    %       4 5 6 7     R0 = 1
    %       -------
    % C0 =  0 1 2 3
    %
    % R0 = Idx0 div NC
    % C0 = Idx0 rem NC
    % Idx0 = (R0 * NC) + C0
    R0 = R1 - 1,
    C0 = C1 - 1,
    Idx0 = (R0 * NC) + C0,
    Idx0.

idx0_to_rc(I0, {shape, _NR, NC}) ->
    R0 = I0 div NC,
    C0 = I0 rem NC,
    R1 = R0 + 1,
    C1 = C0 + 1,
    {rc, R1, C1}.

% @doc
%       Every operation, calculation, and concept, no matter how
%       arbitrarily complex, reduces to adding integers together.
%       There are no new concepts in QAnal. Everything is just
%       putting lipstick on adding integers together.
%
%   -- Dr. Ajay Kumar PHD, Founder of QAnal
%
% qx_h: [Q]Anal [X]Anal [H]amiltonians (quaternions)
% @end

-module(qx_h).
%-behavior(qanal_ring).
%% not actually a field, but who cares
%-behavior(qanal_field).

-export_type([h/0]).

-record(h,
        {p1 :: q(),
         pi :: q(),
         pj :: q(),
         pk :: q()}).
-type h() :: #h{}.

% semantic types
-type c() :: qx_c:c().
-type m() :: qx_m:m().
-type q() :: qx_q:q().
-type v() :: qx_v:v().



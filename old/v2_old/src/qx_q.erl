% @doc
%       Every operation, calculation, and concept, no matter how
%       arbitrarily complex, reduces to adding integers together.
%       There are no new concepts in QAnal. Everything is just
%       putting lipstick on adding integers together.
%
%   -- Dr. Ajay Kumar PHD, Founder of QAnal
%
% qx_q: [Q]Anal [X]Anal [Q]uotients
% @end
-module(qx_q).
-behavior(qanal_field).
-behavior(qanal_ord).
-behavior(qanal_ring).

%%% rationaly things
-export_type([q/0]).
% accessors
-export([top/1,
         bot/1]).
% constructors
-export([q/2,
         from_integer/1]).
% conversions
-export([to_integer/1]).


%%% qanal_field exports
-export([multiplicative_inverse/1]).
-export([divide/2, mi/1]).

%%% qanal_ord exports
-export([le/2, eq/2]).
-export([lt/2, ge/2, gt/2]).

%%% qanal_ring exports
-export([additive_inverse/1, plus/2, one/0, times/2, zero/0]).
-export([ai/1, conj/1, minus/2, pow/2, prod/1, sq/1, sum/1]).


%%% types

-record(q,
        {top :: integer(),
         bot :: pos_integer()}).

-type q() :: #q{}.

% constructors
-spec q(integer(), integer()) -> q().
-spec from_integer(integer()) -> q().

% conversions
-spec to_integer(q()) -> integer().


%%%
%%% rationally things
%%%

%% accessors

top(#q{top=T}) -> T.
bot(#q{bot=B}) -> B.


%% constructors

% no dividing by zero
q(_, 0) ->
    error(badarg);
% 0 divided by anything is the integer 0
q(0, _)  ->
    {q, 0, 1};
% anything divided by 1 is just an integer
q(X, 1) ->
    {q, X, 1};
% bottom should be positive
q(T, B) when B < 0 ->
    q(-1 * T,
       -1 * B);
% both now are nonzero, and the bottom is positive
q(T, B) ->
    % divide both by the gcd
    D  = qx_z:gcd(T, B),
    TT = T div D,
    BB = B div D,
    #q{top = TT,
        bot = BB}.


from_integer(N) ->
    q(N, 1).


%%% conversions

% crashes if not an integer
to_integer({q, N, 1}) ->
    N.




%%%
%%% field callbacks
%%%
-include("qanal_field_derive.hrl").

multiplicative_inverse({q, T, B}) ->
    q(B, T).


%%%
%%% ord callbacks
%%%
-include("qanal_ord_derive.hrl").

eq(X, X) ->
    true;
eq(_, _) ->
    false.

le({q, TL, BL}, {q, TR, BR}) ->
    % bottoms are positive, so we don't change the ordering by multiplying left
    % and right by bottoms
    (TL * BR) =< (TR * BL).


%%%
%%% ring callbacks
%%%

-include("qanal_ring_derive.hrl").

additive_inverse({q, T, B}) ->
    q(-1 * T, B).

conj(Q) ->
    Q.

one() ->
    from_integer(1).

plus({q, TL, BL}, {q, TR, BR}) ->
    % multiply left by BR/BR
    % multiply right by BL/BL
    % that way have common denominator
    % then new top = TL*BR + TR*BL
    %      now bot = BL*BR
    T = (TL * BR) + (TR * BL),
    B = BL * BR,
    q(T, B).

times({q, TL, BL}, {q, TR, BR}) ->
    % this one is easy: TL*TR / BL*BR
    q(TL * TR,
      BL * BR).

zero() ->
    from_integer(0).

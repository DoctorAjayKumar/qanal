% @doc
%       Every operation, calculation, and concept, no matter how
%       arbitrarily complex, reduces to adding integers together.
%       There are no new concepts in QAnal. Everything is just
%       putting lipstick on adding integers together.
%
%   -- Dr. Ajay Kumar PHD, Founder of QAnal
%
% qx_v: [Q]Anal [X]Anal [V]ectors
%
% vectors as in vector space vectors: dimension (length) >= 1
% @end
-module(qx_v).

-export_type([v/0]).

% getters and setters
-export([len/1, arith_mod/1, arr/1]).
-export([nth/2, put/3]).
-export([equals/2]).

% constructors
-export([zeros/2]).

% conversions
-export([from_list/1,
         to_list/1]).

% arithmetic
-export([ip/2,
         quadrance/1,
         additive_inverse/1,
         plus/2,
         minus/2]).

% arrayish operations
-export([map/2,
         foldl/3]).
%         zip/2,
%         zip_with/3]).



%% types
-record(v,
        {len       :: pos_integer(),
         % mod = module where the arithmetic functions are defined
         arith_mod :: atom(),
         arr       :: array()}).

-type v()   :: #v{}.

% semantic types
-type array()     :: array:array().
-type arith_mod() :: atom().
-type idx1()      :: pos_integer().
-type len()       :: pos_integer().
-type val()       :: qanal:scalar().


% specs for getter/setter
-spec len(v())       -> pos_integer().
-spec arith_mod(v()) -> arith_mod().
-spec arr(v())       -> array().

-spec nth(idx1(), v())        -> v().
-spec put(idx1(), val(), v()) -> v().

% constructor specs
-spec zeros(len(), arith_mod())      -> v().

% conversions
-spec from_list([val(), ...])   -> v().
-spec to_list(v())             -> [val(), ...].

% arith specs
-spec ip(v(), v())          -> qanal:scalar().
-spec quadrance(v())        -> qanal:scalar().
-spec additive_inverse(v()) -> v().
-spec plus(v(), v())        -> v().
-spec minus(v(), v())       -> v().


% TODO rethink these, should have sparse and non/sparse options; all
% the normal maps and folds from the array module
%
% arrayish specs
-spec map(Fun, v()) -> v()
        when Fun      :: fun((Idx1, Val, ArithMod) -> NewVal),
             Idx1     :: idx1(),
             Val      :: term(),
             ArithMod :: arith_mod(),
             NewVal   :: term().

-spec foldl(Fun, InitAcc, v()) -> Acc
        when Fun      :: fun((Idx1, Val, ArithMod, Acc) -> NewAcc),
             Idx1     :: idx1(),
             Val      :: term(),
             ArithMod :: arith_mod(),
             InitAcc  :: Acc,
             Acc      :: any(),
             NewAcc   :: Acc.

%%%
%%% accessors
%%%
len(#v{len = L})             -> L.
arith_mod(#v{arith_mod = M}) -> M.
arr(#v{arr = T})             -> T.


% nth(N, QV) -> Value
nth(N, #v{len = Len, arr = Arr}) when (1 =< N) andalso (N =< Len) ->
    Idx0 = N - 1,
    array:get(Idx0, Arr).



% put(N, Value, QV) -> NewQV
put(N, Val, QV = #v{arith_mod = VectArithMod,
                    len = L,
                    arr = Arr})
        when ((1 =< N) andalso (N =< L))
        ->
    ValArithMod = qanal:arith_mod(Val),
    true = qanal:arith_mod_matches(ValArithMod, VectArithMod),
    I0 = N - 1,
    NewArr = array:set(I0, Val, Arr),
    NewVect = QV#v{arr = NewArr},
    NewVect.


% zeros(Len, ArithMod) -> QV
zeros(Len, ArithMod) when (Len > 0) ->
    Zero = ArithMod:zero(),
    Len      = Len,
    ArithMod = ArithMod,
    Arr      = array:new([{size, Len},
                          {default, Zero},
                          {fixed, true}]),
    #v{len       = Len,
        arith_mod = ArithMod,
        arr       = Arr}.

% @doc
% equals: checks if two vectors are "equal", meaning:
%
%   - same length
%   - same types
%   - arrays contain the same elements at the same spots
%
% will return false if given nonsense arguments
% @end
equals(#v{len = LenMatches, arith_mod = AM_Matches, arr = ArrL},
       #v{len = LenMatches, arith_mod = AM_Matches, arr = ArrR}) ->
    MaxIdx0 = LenMatches - 1,
    array_equal(ArrL, ArrR, 0, MaxIdx0, true);
equals(_, _) ->
    false.


% shortcut as soon as we hit a counterexample
array_equal(_ArrL, _ArrR, _ThisIdx0, _MaxIdx0, false) ->
    false;
% if it's the last element, just do the lsat comparison
array_equal(ArrL, ArrR, AtTheEnd, AtTheEnd, true) ->
    LastElemL = array:get(AtTheEnd, ArrL),
    LastElemR = array:get(AtTheEnd, ArrR),
    LastElemL =:= LastElemR;
% not the last element, check it and move along
array_equal(ArrL, ArrR, ThisIdx0, MaxIdx0, true) ->
    ThisElemL = array:get(ThisIdx0, ArrL),
    ThisElemR = array:get(ThisIdx0, ArrR),
    NewIdx0   = ThisIdx0 + 1,
    NewAccum  = ThisElemL =:= ThisElemR,
    array_equal(ArrL, ArrR, NewIdx0, MaxIdx0, NewAccum).


%%%
%%% conversions
%%%

% from_list(List) -> QV
% nonempty list
from_list(List = [X | _Xs]) ->
    ArithMod = qanal:arith_mod(X),
    Zero     = ArithMod:zero(),
    Arr      = array:fix(array:from_list(List, Zero)),
    % assertion
    Len      = erlang:length(List),
    Len      = array:size(Arr),
    #v{len = Len,
        arith_mod = ArithMod,
        arr = Arr}.


% to_list(QV) -> List
to_list(#v{arr = Arr}) ->
    array:to_list(Arr).


%%%
%%% arithmetic
%%%

% ip = inner product = dot product
%
% multiply every value on the left componentwise by the conjugate of
% the value on the right, then take the sum
ip(QVL = #v{len = LenMatches, arith_mod = ArithModMatches},
   QVR = #v{len = LenMatches, arith_mod = ArithModMatches}) ->
    % UsrFun :: fun((Idx1, Val, ArithMod, Acc) -> NewAcc)
    Zero = ArithModMatches:zero(),
    MyFun =
        fun(Idx1, ValL, ArithMod, Acc) ->
            ValL     = ValL,
            ValR     = nth(Idx1, QVR),
            ValRConj = ArithMod:conj(ValR),
            Summand  = ArithMod:times(ValL, ValRConj),
            NewAcc   = ArithMod:plus(Acc, Summand),
            NewAcc
        end,
    % -spec foldl(Fun, InitAcc, v()) -> Acc
    foldl(MyFun, Zero, QVL).


quadrance(QV) ->
    ip(QV, QV).


additive_inverse(QV) ->
    % -spec map(Fun, v()) -> v()
    %       when Fun :: fun((Idx1, Val, ArithMod) -> NewVal)
    MyFun =
        fun(_Idx1, Val, ArithMod) ->
            ArithMod:additive_inverse(Val)
        end,
    map(MyFun, QV).


% there's two ways to do this: a joggerlicious fold, or joggerlicious
% list cancer. The list cancer is probably faster, but the fold is
% cleaner, so we're going with the fold
plus(QVL = #v{len = LenMatches, arith_mod = ArithModMatches},
     QVR = #v{len = LenMatches, arith_mod = ArithModMatches}) ->
    % -spec foldl(Fun, InitAcc, v()) -> Acc
    %         when Fun      :: fun((Idx1, Val, ArithMod, Acc) -> NewAcc)
    InitAcc = zeros(LenMatches, ArithModMatches),
    MyFun =
        fun(Idx1, ValL, ArithMod, Acc) ->
            ValL   = ValL,
            ValR   = nth(Idx1, QVR),
            NewVal = ArithMod:plus(ValL, ValR),
            NewAcc = put(Idx1, NewVal, Acc),
            NewAcc
        end,
    foldl(MyFun, InitAcc, QVL).



minus(QVL, QVR) ->
    plus(QVL, additive_inverse(QVR)).


%%%
%%% arrayish
%%%

map(UsrFun, QV = #v{arith_mod = ArithMod, arr = Arr}) ->
    % UsrFun :: fun((Idx1, Val, ArithMod) -> NewVal)
    UsrFun2 =
        fun(I0, Val) ->
            N = I0 + 1,
            UsrFun(N, Val, ArithMod)
        end,
    % array:map
    %   map(Function, Array :: array(Type1)) -> array(Type2)
    %     Types:
    %       Function = fun((Index :: array_indx(), Type1) -> Type2)
    NewArr = array:map(UsrFun2, Arr),
    NewVect = QV#v{arr = NewArr},
    NewVect.



% foldl
foldl(UsrFun, InitAcc, #v{arith_mod = ArithMod, arr = Arr}) ->
    % UsrFun :: fun((Idx1, Val, ArithMod, Acc) -> NewAcc)
    % array:foldl
    %  foldl(Function, InitialAcc :: A, Array :: array(Type)) -> B
    %         Types:
    %            Function =
    %                fun((Index :: array_indx(), Value :: Type, Acc :: A) -> B)
    UsrFun2 =
        fun(I0, Val, Acc) ->
            N = I0 + 1,
            NewAcc = UsrFun(N, Val, ArithMod, Acc),
            NewAcc
        end,
    Result = array:foldl(UsrFun2, InitAcc, Arr),
    Result.

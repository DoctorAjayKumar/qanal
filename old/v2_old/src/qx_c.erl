% @doc
%       Every operation, calculation, and concept, no matter how
%       arbitrarily complex, reduces to adding integers together.
%       There are no new concepts in QAnal. Everything is just
%       putting lipstick on adding integers together.
%
%   -- Dr. Ajay Kumar PHD, Founder of QAnal
%
% qx_c: [Q]Anal [X]Anal [C]omplex numbers
%
% Complex numbers over the rationals. Nothing fancier needed at this
% point
% @end
-module(qx_c).
-behavior(qanal_ring).
-behavior(qanal_field).

-export_type([c/0]).
% core constructors
-export([c/2, i/0]).
% accessors
-export([re/1, im/1]).
% conversion
-export([to_vect/1, to_matr/1]).
% arithmetic
-export([quadrance/1, turn/1]).
% ring arithmetic
-export([additive_inverse/1, conj/1, plus/2, one/0, times/2, zero/0]).
-export([ai/1, minus/2, pow/2, prod/1, sq/1, sum/1]).
% field arithmetic
-export([multiplicative_inverse/1]).
-export([divide/2, mi/1]).

%-export([multiplicative_inverse/1]).
%-export([divide/2, mi/1]).
-record(c,
        {re :: q(),
         im :: q()}).
-type c() :: #c{}.

% semantic types
-type m() :: qx_m:m().
-type q() :: qx_q:q().
-type v() :: qx_v:v().

% constructors
-spec c(q(), q()) -> c().
-spec i()         -> c().

% conversions
-spec to_vect(c()) -> v().
-spec to_matr(c()) -> m().

% ring callbacks
-spec additive_inverse(c()) -> c().
-spec one()                 -> c().
-spec plus(c(), c())        -> c().
-spec times(c(), c())       -> c().
-spec zero()                -> c().
-spec ai(c())                     -> c().
-spec minus(c(), c())             -> c().
-spec pow(c(), non_neg_integer()) -> c().
-spec prod([c()])                 -> c().
-spec sum([c()])                  -> c().


% helpers
q(X, Y) ->
    qx_q:q(X, Y).

qai(X) ->
    qx_q:ai(X).

qdiv(X, Y) ->
    qx_q:divide(X, Y).

qminus(X, Y) ->
    qx_q:minus(X, Y).

qplus(X, Y) ->
    qx_q:plus(X, Y).

qone() ->
    qx_q:one().

qsq(X) ->
    qx_q:sq(X).

qtimes(X, Y) ->
    qx_q:times(X, Y).

qzero() ->
    qx_q:zero().

% constructors
% reduces the fractions just in case
c({q, TR, BR}, {q, TL, BL}) ->
    #c{re = q(TR, BR),
       im = q(TL, BL)}.

i() ->
    c(qzero(), qone()).

% accessors
re({c, Re, _}) ->
    Re.

im({c, _, Im}) ->
    Im.


% complexy things
quadrance({c, Re, Im}) ->
    qplus(qsq(Re), qsq(Im)).

% turn (x, y) is the sine squared of the modulus of (x, y)
% sine = opposite / hypotenuse
% turn = opposite^2 / hyp^2
% turn = y^2 / quadrance
turn(C = {c, _Re, Im}) ->
    qdiv(qsq(Im), quadrance(C)).


%%% ring arithmetic
-include("qanal_ring_derive.hrl").

additive_inverse({c, Re, Im}) ->
    MinusRe = qx_q:ai(Re),
    MinusIm = qx_q:ai(Im),
    c(MinusRe, MinusIm).

conj({c, Re, Im}) ->
    c(Re, qai(Im)).

one() ->
    c(qone(), qzero()).

plus({c, A, B}, {c, C, D}) ->
    c(qplus(A, C), qplus(B, D)).

times({c, A, B}, {c, C, D}) ->
    % (A + Bi)(C + Di) = (AC - BD) + (AD + BC)i
    Re = qminus(qtimes(A, C), qtimes(B, D)),
    Im = qplus(qtimes(A, D), qtimes(B, C)),
    c(Re, Im).

zero() ->
    c(qzero(), qzero()).

%%% field arithmetic
-include("qanal_field_derive.hrl").

multiplicative_inverse(C = {c, Re, Im}) ->
    % 1 / (z = x + iy) = x/quadrance(z) - y*i/quadrance(z)
    Quad = quadrance(C),
    ResultRe = qdiv(Re, Quad),
    ResultIm = qai(qdiv(Im, Quad)),
    c(ResultRe, ResultIm).


%%% conversions
to_vect({c, Re, Im}) ->
    qx_v:from_list([Re, Im]).

% @doc
% When in doubt, follow Wildberger's convention: we choose it so that
% multiplying [row vector] * [matrix] produces the correct action on
% the row vectors
%
% in particular, this means
%
%   to_matrix({c, X, Y}) ->
%       [[ X  Y]
%        [-Y  X]]
%
% Let's multiply the row-vector corresponding to 1 by the matrix
% corresponding to i and verify that it produces the correct row
% vector:
%
%
%              [[ 0  1]
%               [-1  0]]
%
%   [[ 1  0]]  [[ 0  1]]
%
% The convention is arbitrary. However, in practice, the convention
% where the vector is a row vector on the left ends up being cleaner.
%
% To get the correct action on column vectors, one would need to take
% the transpose of that matrix.
% @end
to_matr({c, X, Y}) ->
    qx_m:from_row_lists([[     X,  Y],
                         [qai(Y),  X]]).

% @doc
%       Every operation, calculation, and concept, no matter how
%       arbitrarily complex, reduces to adding integers together.
%       There are no new concepts in QAnal. Everything is just
%       putting lipstick on adding integers together.
%
%   -- Dr. Ajay Kumar PHD, Founder of QAnal
%
% Top-level QAnal module, contains general convenience functions
% such as
%
% - tm/*: typematch functions
%       NOT YET IMPLEMENTED
%
%       These are used for when the operands are supposed to
%       be THE SAME TYPE, but you're too lazy to specify what it is.
%
%       tm/* automatically selects the correct type, and crashes if
%       the types don't match.
%
%       Example:
%
%           erl> % A and B are rationals
%           erl> A = qanal:q(3, 4).
%           erl> B = qanal:q(5, 6).
%           erl> qanal:tm(qanal, plus, [A, B]).
%           {q, 19, 12}.
%           erl> qanal:tm(qanal, plus, [A, 3]).
%           ** exception error: no function clause matching qx_q:plus({q,3,4},3) (src/qx_q.erl, line 148)
%
%       this I think will almost just be an alias for apply,
%       because of the crash-by-default convention. then use
%       maybe/* to get an error tuple if you need one. Maybe
%       there's a line at the beginning like
%
%           tm(Mod, Fun, Args) ->
%               true = the_types_match(Args),
%               ...
%
%       then you get a more canonical error (badmatch)
%
% - tc/*: typecoerce functions
%       NOT YET IMPLEMENTED
%
%       these are used for when the operands are supposed to
%       be COMPATIBLE TYPES.
%
%       tc/* coerces the result into the lcm [footnote] of the types
%       in the list of arguments.
%
%       Example:
%
%           erl> % A and B are rationals
%           erl> A = qanal:q(3, 4).
%           erl> qanal:tc(qanal, plus, [A, 3]).
%           {q, 15, 4}.
%
% - just/* and maybe/*:
%       IMPLEMENTED
%
%       The functions just/* and maybe/* are inspired by a mailing
%       list question by Joe Armstrong:
%
%             http://erlang.2086793.n4.nabble.com/Must-and-May-convention-td4721725.html
%
%       There are two ways to handle errors:
%             - crashes
%             - error tuples
%
%       Different usage cases dictate which one is better. Joe's idea
%       is
%             - may (Function, Arguments) will crash
%             - must(Function, Arguments) will return an error tuple
%
%       To The Founder's ear, that terminology seems backwards.
%       However, The Founder is also not going to reverse a
%       convention of Joe Armstrong.  In either case, it's bad
%       terminology because there's a confusing sign convention.
%
%       It's a good idea with bad terminology.
%
%       Instead, QAnal uses the terminology:
%
%             - just (Fun, Args) crashes
%             - maybe(Fun, Args) has an error tuple
%
%       This kind of sort of mirrors more the Haskell naming
%       convention, which is of course familiar to everyone.
%
%       As a general rule, all of the code in QAnal crashes by
%       default instead of using error tuples.
%
%       Adopting crash-by-default makes for simpler code because we
%       don't have to hand-code 2 cases for every function, and
%       things are more predictable.
%
%       The functions just/2 and just/3 are aliases for
%       erlang:apply/2 and erlang:apply/3 respectively.
%
%       If you want an error tuple, use maybe/2 and maybe/3. These
%       are just wrappers around try/catch.
%
%       IMPORTANT CAVEAT:
%             maybe/* only catches ERRORS
%
%             There are 3 types of catchable things in Erlang:
%
%                 - errors
%                 - exits
%                 - throws
%
%             maybe/2 and maybe/3 catch all errors indiscriminately,
%             and do not care about exits and throws
%
%             catching exits and throws is retarded. exit-catching is
%             the job of a supervisor. The whole premise of throws is
%             retarded.
%
%       Example:
%
%             1> qanal:q(1, 1).
%             {q,1,1}
%             2> qanal:q(1, 0).
%             ** exception error: bad argument
%                  in function  qx_q:q/2 (src/qx_q.erl, line 77)
%             3> qanal:just(qanal, q, [1, 0]).
%             ** exception error: bad argument
%                  in function  qx_q:q/2 (src/qx_q.erl, line 77)
%             4> qanal:maybe(qanal, q, [1, 0]).
%             {error,badarg}
%             5> qanal:maybe(qanal, q, [1, 1]).
%             {ok,{q,1,1}}
%
%       Non-example:
%
%             6> BadFun = fun() -> exit('I love The Founder').
%             * 1: syntax error before: '.'
%             6> BadFun = fun() -> exit('I love The Founder') end.
%             #Fun<erl_eval.45.79398840>
%             7> BadFun().
%             ** exception exit: 'I love The Founder'
%             8> qanal:just(BadFun, []).
%             ** exception exit: 'I love The Founder'
%             9> qanal:maybe(BadFun, []).
%             ** exception exit: 'I love The Founder'
%                  in function  shell:apply_fun/3 (shell.erl, line 907)
%                  in call from qanal:maybe/2 (src/qanal.erl, line 120)
%
%       Catching exits is the job of a supervisor.
%
% [footnote]:
%     in lcm = "least common multiple", the term "least" is properly
%     thought of as meaning "least" with respect to the divisibility
%     relation on the integers; i.e. it's the lowest common ancestor
%     on the Hasse diagram of integer divisibility. For instance,
%
%                 12
%                /  \
%               4    6
%                \  /
%                  2
%
%     is part of the divisibility graph. 12 is the smallest common
%     parent of 4 and 6, and 2 is the largest common child. In this
%     case,
%
%             lcm(4, 6) = 12.
%             gcd(4, 6) =  2.
%
%     This only kind-of-sort of disagrees with the ordinary order
%     relation on the integers in the following way:
%
%             0 is at the top of the divisibility graph
%
%             that is, with respect to the divisibility relation =<,
%             it is true for every X that X =< 0.
%
%             For instance, with respect to the divisibility graph, 4
%             =< 0.
%
%             This is why gcd(-4, 0) = 4, and lcm(-4, 0) = 0.
% @end
%
% TODO:
%   - better pretty printer
%   - reader
%   - general equals
-module(qanal).

-export_type([scalar/0,
              q/0]).
-export([arith_mod/1, arith_mod_matches/2,
         c/2,
         q/2,
         plus/2, times/2,
         ip/2,
         conj/1,
         list_to_vect/1, vect_to_list/1,
         col_lists_to_matr/1, matr_to_col_lists/1,
         row_lists_to_matr/1, matr_to_row_lists/1,
         matr_transpose/1,
         vect_equals/2,
         matr_equals/2,
         pp/1, pf/1, uf/1,
         just/1,  just/2,  just/3,
         maybe/1, maybe/2, maybe/3]).
-export([additive_inverse/1,ai/1, sq/1,minus/2,am_one/1, am_zero/1]).
-export([divide/2,mi/1,am/1,multiplicative_inverse/1]).

% there is a partial order-by-inclusion on the types (all integers
% are rationals, all rationals are complex, etc), so need to think
% about a language for type coercion

-type z() :: integer().
-type q() :: qx_q:q().
-type c() :: qx_c:c().
-type v() :: qx_v:v().
-type m() :: qx_m:m().
-type scalar() :: z()
               | q()
               | c()
               .

-type amtypes() :: z()
                 | q()
                 | c()
                 | v()
                 | m()
                 .

-spec arith_mod(amtypes()) -> atom().
-spec c(q(), q()) -> c().
-spec q(z(), z()) -> q().
-spec just(Fun) -> Result
            when Fun    :: fun(),
                 Result :: term().
-spec just(Fun, Args) -> Result
            when Fun    :: fun(),
                 Args   :: [term()],
                 Result :: term().
-spec just(Mod, Fun, Args) -> Result
            when Mod    :: atom(),
                 Fun    :: atom(),
                 Args   :: [term()],
                 Result :: term().
-spec maybe(Fun) -> {ok, Result}
                  | {error, Error}
            when Fun    :: fun(),
                 Result :: term(),
                 Error  :: term().
-spec maybe(Fun, Args) -> {ok, Result}
                        | {error, Error}
            when Fun    :: fun(),
                 Args   :: [term()],
                 Result :: term(),
                 Error  :: term().
-spec maybe(Mod, Fun, Args) -> {ok, Result}
                             | {error, Error}
            when Mod    :: atom(),
                 Fun    :: atom(),
                 Args   :: [term()],
                 Result :: term(),
                 Error  :: term().


% detect the arithmetic module corresponding to a QAnal term
arith_mod(Z) when erlang:is_integer(Z) ->
    qx_z;
arith_mod({q, _Top, _Bot}) ->
    qx_q;
arith_mod({c, _Re, _Im}) ->
    qx_c;
arith_mod({v, _Len, _AM, _Arr}) ->
    qx_v;
arith_mod({m, _Shape, _AM, _Arr}) ->
    qx_m.

% convenience
arith_mod_matches(X, X) ->
    true;
arith_mod_matches(_, _) ->
    false.


c(X, Y) ->
    qx_c:c(X, Y).

q(X, Y) ->
    qx_q:q(X, Y).

conj(X) ->
    AM = arith_mod(X),
    AM:conj(X).

plus(X, Y) ->
    AM = arith_mod(X),
    AM:plus(X, Y).

times(X, Y) ->
    AM = arith_mod(X),
    AM:times(X, Y).

% pretty printer
% TODO: make this prettier, add a reader; this is good enough for now
pp(X) ->
    io:format("~ts~n", [pf(X)]).

% pretty formatter
pf(X) ->
    unicode:characters_to_list(uf(X)).

% ugly formatter (deep lists)
uf(Z) when erlang:is_integer(Z) ->
    erlang:integer_to_list(Z);
uf({q, Top, Bot}) ->
    ["(q ", uf(Top), " ", uf(Bot), ")"];
uf({c, Re, Im}) ->
    ["(c ", uf(Re), " ", uf(Im), ")"];
% ugly format a nonempty list
uf(List) when is_list(List)->
    UF_Item = fun(Item) -> [" ", uf(Item)] end,
    ["(list", lists:map(UF_Item, List), ")"];
uf(Vect = {v, _Len, _Am, _Arr}) ->
    VectLs = vect_to_list(Vect),
    ["(v ", uf(VectLs), ")"];
uf(Matr = {m, _Shape, _Am, _Arr}) ->
    MatrLs = matr_to_row_lists(Matr),
    ["(m ", uf(MatrLs), ")"].



% ugly format list no quote (nonempty list)
%
% uf_list_nq([1,2,3,4]) -> "(1 2 3 4)"

ip(X, Y) ->
    qx_v:ip(X, Y).

list_to_vect(List) ->
    qx_v:from_list(List).

vect_to_list(Vect) ->
    qx_v:to_list(Vect).

% converting matrices
col_lists_to_matr(List) ->
    qx_m:from_col_lists(List).

matr_to_col_lists(Matr) ->
    qx_m:to_col_lists(Matr).

row_lists_to_matr(List) ->
    qx_m:from_row_lists(List).

matr_to_row_lists(Matr) ->
    qx_m:to_row_lists(Matr).

matr_transpose(Matr) ->
    qx_m:transpose(Matr).

%% equals
vect_equals(V1, V2) ->
    qx_v:equals(V1, V2).

matr_equals(_M1, _M2) ->
    error(nyi).


% just and maybe
just(Fun) ->
    Fun().

just(Fun, Args) ->
    erlang:apply(Fun, Args).

just(Mod, Fun, Args) ->
    erlang:apply(Mod, Fun, Args).

maybe(Fun) ->
    try just(Fun) of
        Result ->
            {ok, Result}
    catch
        error:Error ->
            {error, Error}
    end.

maybe(Fun, Args) ->
    try just(Fun, Args) of
        Result ->
            {ok, Result}
    catch
        error:Error ->
            {error, Error}
    end.

maybe(Mod, Fun, Args) ->
    try just(Mod, Fun, Args) of
        Result ->
            {ok, Result}
    catch
        error:Error ->
            {error, Error}
    end.

% more convenience functions
additive_inverse(X) ->
    M = arith_mod(X),
    M:additive_inverse(X).

ai(X) ->
    additive_inverse(X).

sq(X) ->
    Am = arith_mod(X),
    Am:sq(X).

minus(X, Y) ->
    plus(X, ai(Y)).

am_one(X) ->
    AM = arith_mod(X),
    AM:one().

am_zero(X) ->
    AM = arith_mod(X),
    AM:zero().

divide(X, Y) ->
    times(X, mi(Y)).

mi(Y) ->
    multiplicative_inverse(Y).

am(X) ->
    arith_mod(X).

multiplicative_inverse(Y) ->
    M = am(Y),
    M:multiplicative_inverse(Y).

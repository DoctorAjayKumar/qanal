% @doc
%       Every operation, calculation, and concept, no matter how
%       arbitrarily complex, reduces to adding integers together.
%       There are no new concepts in QAnal. Everything is just
%       putting lipstick on adding integers together.
%
%   -- Dr. Ajay Kumar PHD, Founder of QAnal
%
% qx_sd: [Q]Anal [X]Anal [S]imple [D]ual numbers.
%
% This is the data structure we use for simple differentiation
% (e.g. Newton's method).
-module(qx_sd).

-export_type([sd/0]).
-export([scalar/1,
         epsilon/1,
         spe/1,
         scalar_plus_epsilon/1]).
-export([cs/1, ce/1, se/1]).
-export([plus/2, minus/2, ai/1, additive_inverse/1,
         times/2, pow/2]).

-type arith_mod() :: atom().
-type m()         :: qx_m:m().
-record(sd,
        {matrix :: m()}).
-type sd()    :: #sd{}.


-spec am(qanal:scalar()) -> arith_mod().
-spec zero_of_scalar_am(qanal:scalar()) -> qanal:scalar().
-spec scalar(qanal:scalar()) -> sd().
-spec epsilon(arith_mod()) -> sd().
-spec spe(qanal:scalar()) -> sd().
-spec scalar_plus_epsilon(qanal:scalar()) -> sd().
-spec plus(sd(), sd()) -> sd().
-spec minus(sd(), sd()) -> sd().
-spec ai(sd()) -> sd().
-spec additive_inverse(sd()) -> sd().
-spec times(sd(), sd()) -> sd().
-spec pow(sd(), non_neg_integer()) -> sd().

%% helpers
am(X) ->
    qanal:arith_mod(X).

zero_of_scalar_am(X) ->
    apply(am(X), zero, []).

%one_of_scalar_am(X) ->
%    apply(am(X), one, []).

%% constructors

scalar(X) ->
    Zero = zero_of_scalar_am(X),
    M = qx_m:from_row_lists([[   X, Zero],
                             [Zero,    X]]),
    {sd, M}.

epsilon(ArithMod) ->
    One  = apply(ArithMod, one, []),
    Zero = apply(ArithMod, zero, []),
    M    = qx_m:from_row_lists([[Zero, Zero],
                                [ One, Zero]]),
    {sd, M}.

spe(X) ->
    Scalar  = scalar(X),
    Epsilon = epsilon(am(X)),
    plus(Scalar, Epsilon).

scalar_plus_epsilon(X) ->
    spe(X).

%% accessors

component_scalar({sd, M}) ->
    qx_m:rcth({rc, 1, 1}, M).

component_epsilon({sd, M}) ->
    qx_m:rcth({rc, 2, 1}, M).

cs(SD) ->
    component_scalar(SD).

ce(SD) ->
    component_epsilon(SD).

se(SD) ->
    {se, cs(SD), ce(SD)}.

%% arithmetic

plus({sd, X}, {sd, Y}) ->
    {sd, qx_m:plus(X, Y)}.

minus({sd, X}, {sd, Y}) ->
    {sd, qx_m:minus(X, Y)}.

ai({sd, X}) ->
    {sd, qx_m:ai(X)}.

additive_inverse(SD) ->
    ai(SD).

times({sd, X}, {sd, Y}) ->
    {sd, qx_m:times(X, Y)}.

pow({sd, X}, N) ->
    {sd, qx_m:pow(X, N)}.

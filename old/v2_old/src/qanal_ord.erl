% @doc
%       Every operation, calculation, and concept, no matter how
%       arbitrarily complex, reduces to adding integers together.
%       There are no new concepts in QAnal. Everything is just
%       putting lipstick on adding integers together.
%
%   -- Dr. Ajay Kumar PHD, Founder of QAnal
% @end
% assumes total order
-module(qanal_ord).

% -export([le/2, eq/2]).
% -export([lt/2, ge/2, gt/2]).

% mt = module_type
-type mt() :: any().

% minimal
-callback le(mt(), mt()) -> boolean().
-callback eq(mt(), mt()) -> boolean().

% derived
-callback lt(mt(), mt()) -> boolean().
-callback ge(mt(), mt()) -> boolean().
-callback gt(mt(), mt()) -> boolean().

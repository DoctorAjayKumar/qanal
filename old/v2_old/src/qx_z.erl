% @doc
% qx_z: [Q]Anal [X]Anal integer [Z] library
-module(qxz).

-export_type([z/0, znn/0, zp/0]).

-export([gcd/2,
         modulo/2]).

-type z()   :: integer().
-type znn() :: non_negative_integer().
-type zp()  :: pos_integer().




%%%%%%%%%%%
%%% API %%%
%%%%%%%%%%%

-spec gcd(z(), z()) -> znn().
% @doc greatest common divisor of two integers

gcd(X, Y) ->
    gcdplus(abs(X), abs(Y)).



-spec modulo(z(), z()) -> znn().
% @doc modulo(A, B) returns A "beaten down" mod B. Specifically, this
% always returns a number with the range 0 =< R < B.

% if M is negative, do X mod abs(M)
modulo(X, M) when is_integer(X), is_integer(M), M < 0 ->
    modulo(X, abs(M));
modulo(X, M) when is_integer(X), is_integer(M), M > 0 ->
    XremM = X rem M,
    case 0 =< XremM of
        true  -> XremM;
        false -> M + XremM
    end.



%%%%%%%%%%%%%%%%%
%%% INTERNALS %%%
%%%%%%%%%%%%%%%%%

-spec gcdplus(znn(), znn()) -> znn().
% @private gcd on two non-negative integers

% we want to assume A >= B
gcdplus(A, B) when A < B ->
    gcdplus(B, A);
gcdplus(X, 0) ->
    X;
gcdplus(A, B) ->
    % q = quotient
    % r = remainder
    % A = qB + R where R = modulo(A, B) and q = A div B
    %
    % 0 =< R < B
    %
    % therefore r = A - qB
    % any number that divides both A and B (such as gcd(A, B))
    % also divides R
    %
    % therefore
    %
    % gcd(A, B) = gcd(B, R)
    R = modulo(A, B),
    gcdplus(B, R).


%%%
%%% qanal_ord behavior
%%%

-include("qanal_ord_derive.hrl").

le(X, Y) ->
    X =< Y.

eq(X, X) ->
    true;
eq(_, _) ->
    false.



%%%
%%% qanal_ring behavior
%%%

-include("qanal_ring_derive.hrl").

additive_inverse(X) ->
    -1 * X.

conj(X) ->
    X.

plus(X, Y) ->
    X + Y.

one() ->
    1.

times(X, Y) ->
    X * Y.

zero() ->
    0.

% derivable function definitions

ai(X) ->
    additive_inverse(X).

%% minus
minus(X, Y) ->
    plus(X, additive_inverse(Y)).


%% pow
pow(X, N) ->
    pow(X, N, one()).

pow(_, 0, Acc) ->
    Acc;
pow(X, N, Acc) ->
    NewAcc = times(X, Acc),
    pow(X, N - 1, NewAcc).


%% prod
prod(List) ->
    prod(List, one()).

prod([], Acc) ->
    Acc;
prod([X | Xs], Acc) ->
    NewAcc = times(X, Acc),
    prod(Xs, NewAcc).


sq(X) ->
    times(X, X).

%% sum
sum(List) ->
    sum(List, zero()).

sum([], Acc) ->
    Acc;
sum([X | Xs], Acc) ->
    NewAcc = plus(X, Acc),
    sum(Xs, NewAcc).

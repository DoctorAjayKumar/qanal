lt(X, Y) ->
    (not eq(X, Y)) andalso le(X, Y).

ge(X, Y) ->
    not lt(X, Y).

gt(X, Y) ->
    not le(X, Y).

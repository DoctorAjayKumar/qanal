divide(X, Y) ->
    times(X, multiplicative_inverse(Y)).

mi(X) ->
    multiplicative_inverse(X).

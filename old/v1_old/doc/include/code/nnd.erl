#!/usr/bin/env escript

-mode(compile).
-compile(nowarn_unused_function).

log_base(Base, N) ->
    math:log(N) /
    math:log(Base).

nnd_nod(NOD, OB, NB) ->
    erlang:floor(
        (NOD - 1)
        * log_base(NB, OB)
    ) + 1.

nnd_n(N, OB, NB) ->
    erlang:floor(
        erlang:floor(log_base(OB, N))
        * log_base(NB, OB)
    ) + 1.

dashline() ->
    ok = io:format("~s~n", [[$- || _<-lists:seq(1,80)]]),
    ok.

main(_) ->
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % nnd_nod
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    dashline(),
    ok = io:format("nnd_nod~n"),
    dashline(),
    %--------------------------------------------------------------------------
    ok =
        io:format(
            "4-digit binaries map to 4-digit binaries~n"
            "nnd_nod(NOD=4, OB=2, NB=2)~n"
            "   should be: 4~n"
            "          is: ~p~n~n",
            [nnd_nod(4, 2, 2)]
        ),
    ok =
        io:format(
            "4-digit decimals map to 4-digit decimals~n"
            "nnd_nod(NOD=4, OB=10, NB=10)~n"
            "   should be: 4~n"
            "          is: ~p~n~n",
            [nnd_nod(4, 10, 10)]
        ),
    ok =
        io:format(
            "57-digit numbers in base 17 map to 57-digit numbers in base-17~n"
            "nnd_nod(NOD=57, OB=17, NB=17)~n"
            "   should be: 57~n"
            "          is: ~p~n~n",
            [nnd_nod(57,17,17)]
        ),
    %--------------------------------------------------------------------------
    ok =
        io:format(
            "4-digit binaries map to 2-digit octals~n"
            "nnd_nod(NOD=4, OB=2, NB=8)~n"
            "   should be: 2~n"
            "          is: ~p~n~n",
            [nnd_nod(4, 2, 8)]
        ),
    %--------------------------------------------------------------------------
    ok =
        io:format(
            "2-digit octals map to 4-digit binaries~n"
            "nnd_nod(NOD=2, OB=8, NB=2)~n"
            "   should be: 4~n"
            "          is: ~p~n~n",
            [nnd_nod(2, 8, 2)]
        ),
    %--------------------------------------------------------------------------
    ok =
        io:format(
            "3-digit decimals map to 7-digit binaries~n"
            "nnd_nod(NOD=3, OB=10, NB=2)~n"
            "   should be: 7~n"
            "          is: ~p~n~n",
            [nnd_nod(3, 10, 2)]
        ),
    %--------------------------------------------------------------------------
    ok =
        io:format(
            "7-digit binaries map to 2-digit decimals~n"
            "nnd_nod(NOD=7, OB=2, NB=10)~n"
            "   should be: 2~n"
            "          is: ~p~n~n",
            [nnd_nod(7, 2, 10)]
        ),
    %--------------------------------------------------------------------------

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % nnd_n
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %--------------------------------------------------------------------------
    dashline(),
    ok = io:format("nnd_n~n"),
    dashline(),

    %--------------------------------------------------------------------------
    ok =
        io:format(
            "4-digit binaries map to 2-digit octals~n"
            "nnd_n(N=2#1000, OB=2, NB=8)~n"
            "   should be: 2~n"
            "          is: ~p~n~n",
            [nnd_n(2#1000, 2, 8)]
        ),
    ok =
        io:format(
            "4-digit binaries map to 2-digit octals~n"
            "nnd_n(N=2#1111, OB=2, NB=8)~n"
            "   should be: 2~n"
            "          is: ~p~n~n",
            [nnd_n(2#1111, 2, 8)]
        ),
    %--------------------------------------------------------------------------
    ok =
        io:format(
            "2-digit octals map to 4-digit binaries~n"
            "nnd_n(N=8#10, OB=8, NB=2)~n"
            "   should be: 4~n"
            "          is: ~p~n~n",
            [nnd_n(8#10, 8, 2)]
        ),
    ok =
        io:format(
            "2-digit octals map to 4-digit binaries~n"
            "nnd_n(N=8#17, OB=8, NB=2)~n"
            "   should be: 4~n"
            "          is: ~p~n~n",
            [nnd_n(8#17, 8, 2)]
        ),
    %--------------------------------------------------------------------------
    ok =
        io:format(
            "3-digit decimals map to 7-digit binaries~n"
            "nnd_n(N=100, OB=10, NB=2)~n"
            "   should be: 7~n"
            "          is: ~p~n~n",
            [nnd_n(100, 10, 2)]
        ),
    ok =
        io:format(
            "3-digit decimals map to 7-digit binaries~n"
            "nnd_n(N=999, OB=10, NB=2)~n"
            "   should be: 7~n"
            "          is: ~p~n~n",
            [nnd_n(999, 10, 2)]
        ),
    %--------------------------------------------------------------------------
    ok =
        io:format(
            "7-digit binaries map to 2-digit decimals~n"
            "nnd_n(N=2#1000000, OB=2, NB=10)~n"
            "   should be: 2~n"
            "          is: ~p~n~n",
            [nnd_n(2#1000000, 2, 10)]
        ),
    ok =
        io:format(
            "7-digit binaries map to 2-digit decimals~n"
            "nnd_n(N=2#1111111, OB=2, NB=10)~n"
            "   should be: 2~n"
            "          is: ~p~n~n",
            [nnd_n(2#1111111, 2, 10)]
        ),
    %--------------------------------------------------------------------------
    ok.


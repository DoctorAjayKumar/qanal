%       Every operation, calculation, and concept, no matter how arbitrarily
%       complex, reduces to adding integers together. There are no new concepts
%       in QAnal. Everything is just putting lipstick on adding integers
%       together.
%
%   -- Dr. Ajay Kumar PHD, Founder of QAnal
%=============================================================================%
\chapter{[XAnal] Why is the matrix multiplication rule what it is?}

\ajayquote
%=============================================================================%

The short version is this:

\begin{quote}
    There are many contexts in which the data structure of a rectangular array
    of numbers naturally arises.

    In most of those contexts, one needs a way to compose the rectangular
    arrays.

    The vast majority of useful composition rules correspond to matrix
    multiplication.
\end{quote}

%-----------------------------------------------------------------------------%
\section{Example: complex number arithmetic}
%-----------------------------------------------------------------------------%

The point of this section is not to give a complete, self-contained explanation
of complex number arithmetic or the associated linear algebra, but rather to
convince you that the standard operations of complex number arithmetic map
quite well into the standard operations of linear algebra.

A \term{complex number}, as far as we are concerned, is a pair of rational
numbers $(x, y)$. The multiplication rule for two complex numbers is as follows

\begin{displaymath}
    (x, y)\cdot(x', y') = (xx' - yy', xy' + yx')
\end{displaymath}

Every complex number $(x, y)$ corresponds to the matrix

\begin{displaymath}
    \matr{
        x & -y \\
        y & x
    }
\end{displaymath}

We can check that multiplying these matrices gives us the correct composition
rule

\begin{align*}
    \matr{
        x & -y \\
        y & x
    }
    \matr{
        x' & -y' \\
        y' & x'
    }
        & =
        \matr{
            xx' - yy' & -xy' - yx' \\
            yx' + y'x & -yy' + xx'
        } \\
        & =
        \matr{
            xx' - yy' & -(xy' + yx') \\
            xy' + yx' & xx' - yy' \\
        } \\
\end{align*}

For instance, the complex number $(1,0)$ is the mutliplicative identity:

\begin{align*}
    (x, y)\cdot (1, 0) &= (x\cdot 1 - y \cdot 0, x\cdot 0 + y \cdot 1) \\
                       &= (x, y) \\
\end{align*}


You can (and should) check that it works in the other direction. Additionally,
as a sanity check, the complex number $(1, 0)$, using our rule, maps to the $2
\times 2$ identity matrix. So far so good.

If you think of the vectors $(x, y)$ as lying in the plane, and identify the
vector $(x, y)$ with the column matrix $\matr{x \\ y}$, then matrices act on
vectors in the plane the same way complex number multiplication does.

In particular, multiplying a complex-number-as-a-matrix by the column vector
$\matr{1 \\ 0}$ recovers the complex-number-as-a-vector

\begin{align*}
    \matr{x & -y \\ y & x}
    \matr{1 \\ 0}
        &= \matr{1x - 0y \\ 1y + 0x} \\
        &= \matr{x \\ y} \\
\end{align*}

Let us for now call $Z = \matr{x & -y \\ y & x}$, for linguistic ease.

We denote by $i$ the point $(0, 1)$. It should be noted that $i^2 = (-1, 0)$.
Generally, the point $(x, y)$ can be denoted by $x + yi$. The normal rules of
algebra map perfectly onto this convention.

Linear algebra operations map well into the complex numbers. For
instance, let us compute the eigenvalues of $Z$.

\begin{definition}
    A number $\lambda$ is an \term{eigenvalue} of a matrix $M$ if there is some
    vector $v$ which is not the $\v 0$-vector such that $Mv = \lambda v$. The
    vector $v$ is called an \term{eigenvector} of $M$.
\end{definition}

We do not admit the trivial solution $v = \v 0$ as a solution to the eigenvalue
equation.

In practice, you find the eigenvalues first, and then compute eigenvectors once
you have find eigenvalues. Let us solve for the eigenvalues of $Z$:

\begin{align*}
    Zv
        &= \lambda v \\
        &=
            \lambda I_2 v
            && \text{($I_2 = 2 \times 2$ identity matrix)}
            \\
    Zv - \lambda I_2 v
        &=
            \v 0
            && \text{($\v 0$ is the column \emph{vector} with all $0$ entries)}
            \\
    (Z - \lambda I_2) v 
        &= \\
    \parens{
        \matr{x & -y \\ y & x}
        -
        \matr{\lambda & 0 \\ 0 & \lambda}
    }v
        &= \\
    \matr{x-\lambda & -y \\ y & x-\lambda}v
        &= \\
\end{align*}


I am going to introduce some facts now, which I will cleanly establish later.

If, for any matrix, a vector $v$ that is not $\v 0$ maps to the $\v 0$ vector,
that is $Mv = \v 0$ if $v \ne \v 0$, then the matrix $M$ is not invertible. A
matrix $M$ is not invertible if and only if its \term{determinant} is $0$. The
determinant of a $2 \times 2$ matrix $M = \matr{a & b \\ c & d}$ is denoted
$\abs M$ and is given by the formula

\[
    \abs M = ad - bc
\]

You will in due time see where that formula comes from. For now, just roll with
me. Therefore, since we've established the matrix $Z - \lambda I_2$ is not
invertible, it must follow that its determinant is 0. Plugging our formula for
$Z - \lambda I_2$ into the $2 \times 2$ determinant formula gives

\begin{align*}
    \abs{Z - \lambda I_2}
        &= 0 \\
        &= (x - \lambda)^2 + y^2 \\
        &= (x - \lambda)(x - \lambda) + y^2 \\
        &=
            \parens{
                x^2 - 2x\lambda + \lambda^2
            }
            +
            y^2
            \\
        &=
            (1)\lambda^2 + (-2x)\lambda + (x^2 + y^2)
            \\
        &= 0 \\
\end{align*}

Remember, for our purposes, $x$ and $y$ should be thought of as fixed, and we
are solving for $\lambda$. By the way,

\begin{definition}
    For any square matrix $M$, the polynomial given by $\abs{M - \lambda I}$ is
    called the \term{characteristic polynomial}. The values $\lambda$ such that
    the characteristic polynomial is $0$ are the eigenvalues of $M$.
\end{definition}

Let us remember the handy dandy quadratic formula from high school. If
$f(\lambda) = a\lambda^2 + b\lambda + c$, then the roots of $f$ (values of
$\lambda$ such that $f(\lambda) = 0$) are given by

\[
    \lambda = \frac{-b \pm \sqrt{b^2 - 4ac}}{2a}
\]

Or in more algebra-friendly form

\[
    \lambda = \frac{1}{2a}\braces{-b \pm \brackets{b^2 - 4ac}^{1/2}}
\]

Let us denote the characteristic polynomial of $Z$ as a function of $\lambda$
by $\chi_Z(\lambda)$. As we just calculated,

\begin{align*}
    \chi_Z(\lambda) &= (1)\lambda^2 + (-2x)\lambda + (x^2 + y^2),
\end{align*}

and we are solving for when $\chi_Z(\lambda) = 0$. Plugging that into the
quadratic formula gives

\begin{align*}
    \lambda
        &=
            \frac{1}{2(1)}
            \braces{
                -(-2x)
                \pm
                \brackets{
                    (-2x)^2
                    -
                    4(1)(x^2 + y^2)
                }^{1/2}
            }
            \\
        &=
            \frac{1}{2}
            \braces{
                2x
                \pm
                \brackets{
                    4x^2
                    -
                    4(x^2 + y^2)
                }^{1/2}
            }
            \\
        &=
            \frac{1}{2}
            \braces{
                2x
                \pm
                \brackets{
                    -4y^2
                }^{1/2}
            }
            \\
        &=
            \frac{1}{2}
            \braces{
                2x
                \pm
                \brackets{
                    (-1)(4)(y^2)
                }^{1/2}
            }
            \\
        &=
            \frac{1}{2}
            \braces{
                2x
                \pm
                (-1)^{1/2}
                (4)^{1/2}
                (y^2)^{1/2}
            }
            \\
        &=
            \frac{1}{2}
            \braces{
                2x \pm 2y i
            }
            \\
        &= x \pm y i \\
\end{align*}

For $z = x + yi$, the value $\bar z = x - yi$ is called the \term{complex
conjugate} of $z$. We have just shown

\begin{theorem}
    For a complex number $z = x + yi$, the eigenvalues of its asssociated
    matrix $Z = \matr{x & -y \\ y & x}$ are itself and its complex conjugate.
\end{theorem}

For any matrix, the product of its eigenvalues equals its determinant. As an
exercise, calculate the determinant of $Z$ using the $2 \times 2$ determinant
formula, and also calculate the product $z\bar{z}$.

The quantity $Q(x + yi) = x^2 + y^2$ is called the \term{quadrance} of $x +
yi$. As an exercise, draw the point $(x,y)$ on a coordinate plane, and convince
yourself that

\begin{enumerate}
\item
    The quadrance of $(x, y)$ is the area of the square with opposite corners
    $(0, 0)$, $(x, y)$.
\item
    The quadrance of $(x, y)$ is the square of the distance from $(0,0)$ to
    $(x, y)$.

    (Use the Pythagorean theorem).
\end{enumerate}

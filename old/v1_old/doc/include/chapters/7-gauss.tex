%       Every operation, calculation, and concept, no matter how arbitrarily
%       complex, reduces to adding integers together. There are no new concepts
%       in QAnal. Everything is just putting lipstick on adding integers
%       together.
%
%   -- Dr. Ajay Kumar PHD, Founder of QAnal
%=============================================================================%
\chapter{[XAnal] Motivating example: Gaussian Elimination}

\ajayquote
%=============================================================================%


The most basic problem in linear algebra is: given a matrix $A$ and a vector
$b$, solve the system $Ax = b$.  That is, we wish to find the set of all
vectors $x$ such that $Ax = b$.

If $A$ is invertible, then the solution is $x = A^{-1}b$. This isn't a good
enough answer, for the following reasons:

\begin{itemize}
    \item Not all matrices are invertible; in particular, non-square matrices
    are \emph{never} invertible.

    \item Non-square matrices are a very common case in practice

    \item Even if $A$ is invertible, how does one go about \emph{computing}
    $A^{-1}$?

\end{itemize}

Gaussian elimination \emph{cleanly} and \emph{deterministically} solves all of
these problems. Gaussian elimination is simply the algorithm you learned in
school for solving problems like this:

\begin{align*}
    - x  -6y  +9z &= -4 \\
     5x  + y  +5z &= -3 \\
    -2x  +6y  +8z &= -5 \\
\end{align*}

I encourage you to solve that by hand before reading on. Meaning, find values
of $x$, $y$, and $z$ such that all of the equations above are true.

\newpage

It's a lot easier, notationally, and for the computer, to first rewrite that as
a matrix problem

\begin{align*}
    \matr{
        -1 & -6 &  9 \\
         5 &  1 &  5 \\
        -2 &  6 &  8 \\
    }
    \matr{
        x \\ y \\ z \\
    }
    =
    \matr{
        -4 \\
        -3 \\
        -5 \\
    }
\end{align*}

First let us calculate the inverse of $A$ using a very sophisticated technique

\begin{lstlisting}
>>> import numpy as np
>>> A = np.matrix('-1 -6 9; 5 1 5; -2 6 8')
>>> A
matrix([[-1, -6,  9],
        [ 5,  1,  5],
        [-2,  6,  8]])
>>> A1 = np.linalg.inv(A)
>>> A1
matrix([[-0.03606557,  0.16721311, -0.06393443],
        [-0.08196721,  0.01639344,  0.08196721],
        [ 0.05245902,  0.0295082 ,  0.04754098]])
>>> b = np.matrix('-4 ; -3 ; -5')
>>> A1*b
matrix([[-0.03770492],
        [-0.13114754],
        [-0.53606557]])
\end{lstlisting}

We can check that this is indeed a solution to the system of equations with

\begin{lstlisting}
>>> A*A1*b
matrix([[-4.],
        [-3.],
        [-5.]])
\end{lstlisting}

%-----------------------------------------------------------------------------%
\section{So what's going on here?}
%-----------------------------------------------------------------------------%

First of all, there should be no mystery as to why $x = A^{-1}b$ is a solution
to $Ax = b$. Substituting $x=A^{-1}b$ into that equation gives us $AA^{-1}b =
b$, which is true by the definition of $A^{-1}$.

But how is $A^{-1}$ computed? That is called Gaussian elimination. The basic
idea is to compute a sequence of matrices $\Gamma_n, \dots, \Gamma_2, \Gamma_1$
such that $\Gamma_n\dots\Gamma_2\Gamma_1 A = I_3$ where $I_3$ is the $3\times
3$ identity matrix. Then by definition, $\Gamma_n\dots\Gamma_2 \Gamma_1 =
A^{-1}$. The matrices $\Gamma_i$ are called the \term{Gauss matrices}.

Remember, every matrix inverse is a two sided inverse. This is simply a
consequence of having an associative product, and is not a special feature of
matrices (most useful products are associative). Suppose $A$ has a left inverse
$L$ and a right inverse $R$. That is, $LA = AR = I_3$. Then $L = L = L(AR) =
(LA)R = R$, so $L = R$.

Let's walk through it in the special case here. And it's going to seem like I'm
pulling a lot of magic out of my ass, but trust that at the end of this it will
be obvious how it works

\begin{enumerate}
    \item First order of business is to get a $1$ in the top left entry
        \begin{displaymath}
            A =
            \matr{
                -1 & -6 &  9 \\
                 5 &  1 &  5 \\
                -2 &  6 &  8 \\
            }
        \end{displaymath}

        To do this we need to multiply the top row by $-1$. The following
        matrix does that

        \begin{displaymath}
            \Gamma_1 =
            \matr{
                -1 & 0 & 0 \\
                 0 & 1 & 0 \\
                 0 & 0 & 1 \\
            }
        \end{displaymath}

        If you're following along at home,

\begin{lstlisting}
>>> A = np.matrix('-1 -6 9; 5 1 5; -2 6 8')
>>> A
matrix([[-1, -6,  9],
        [ 5,  1,  5],
        [-2,  6,  8]])
>>> G1 = np.matrix('-1 0 0; 0 1 0; 0 0 1')
>>> G1*A
matrix([[ 1,  6, -9],
        [ 5,  1,  5],
        [-2,  6,  8]])
\end{lstlisting}

    \item Next, we need to eliminate all of the nonzero entries in the first
    column

        \begin{align*}
            \Gamma_2 &=
                \matr{
                     1 & 0 & 0 \\
                    -5 & 1 & 0 \\
                     0 & 0 & 1 \\
                } \\
            \Gamma_3 &=
                \matr{
                     1 & 0 & 0 \\
                     0 & 1 & 0 \\
                     2 & 0 & 1 \\
                } \\
        \end{align*}

\begin{lstlisting}
>>> G2 = np.matrix('1 0 0; -5 1 0; 0 0 1')
>>> G2*G1*A
matrix([[  1,   6,  -9],
        [  0, -29,  50],
        [ -2,   6,   8]])
>>> G3 = np.matrix('1 0 0; 0 1 0; 2 0 1')
>>> G3*G2*G1*A
matrix([[  1,   6,  -9],
        [  0, -29,  50],
        [  0,  18, -10]])
\end{lstlisting}

    \item Our next goal is to get a $1$ in the middle entry. And here is where
    we encounter the joggerliciousness known as floating point arithmetic, and
    also where Numpy's quote syntax ends

    \begin{align*}
        \Gamma_4 &=
            \matr{
                 1 & 0 & 0 \\
                 0 & -\frac{1}{29} & 0 \\
                 0 & 0 & 1 \\
            } \\
    \end{align*}


\begin{lstlisting}
>>> G4 = np.matrix([[1,0,0], [0,-1/29,0], [0,0,1]])
>>> G4*G3*G2*G1*A
matrix([[ 1.00000000e+00,  6.00000000e+00, -9.00000000e+00],
        [ 1.38777878e-17,  1.00000000e+00, -1.72413793e+00],
        [ 0.00000000e+00,  1.80000000e+01, -1.00000000e+01]])
\end{lstlisting}

    In human-readable format, that's saying

    \begin{align*}
        \Gamma_4
        \Gamma_3
        \Gamma_2
        \Gamma_1
        A
        &=
        \matr{
             1 & 6 & -9 \\
             0 & 1 & -\frac{50}{29} \\
             0 & 18 & -10 \\
        } \\
    \end{align*}

    \item We need to eliminate everything that isn't the diagonal from the
    second column.

    Before I show you this matrix, try to guess it yourself. Play around in
    Numpy. In fact, try to do the rest of it yourself and then come back and
    compare answers

    \newpage

    \begin{align*}
        \Gamma_5 &=
            \matr{
                 1 & -6 & 0 \\
                 0 & 1 & 0 \\
                 0 & 0 & 1 \\
            } \\
        \Gamma_6 &=
            \matr{
                 1 &   0 & 0 \\
                 0 &   1 & 0 \\
                 0 & -18 & 1 \\
            } \\
    \end{align*}

\begin{lstlisting}
>>> G5 = np.matrix([[1,-6,0], [0,1,0], [0,0,1]])
>>> G5*G4*G3*G2*G1*A
matrix([[ 1.00000000e+00,  8.32667268e-17,  1.34482759e+00],
        [ 1.38777878e-17,  1.00000000e+00, -1.72413793e+00],
        [ 0.00000000e+00,  1.80000000e+01, -1.00000000e+01]])
>>> G6 = np.matrix([[1,0,0], [0,1,0], [0,-18,1]])
>>> G6*G5*G4*G3*G2*G1*A
matrix([[ 1.00000000e+00,  8.32667268e-17,  1.34482759e+00],
        [ 1.38777878e-17,  1.00000000e+00, -1.72413793e+00],
        [ 0.00000000e+00,  0.00000000e+00,  2.10344828e+01]])
\end{lstlisting}

    In human-readable form, that is

    \begin{align*}
        \Gamma_5
        \Gamma_4
        \Gamma_3
        \Gamma_2
        \Gamma_1
        A
            &=
            \matr{
                 1 & 0 & -9 + 6\cdot\frac{50}{29}  \\
                 0 & 1 & -\frac{50}{29} \\
                 0 & 18 & -10 \\
            } \\
            &=
            \matr{
                 1 & 0 & -\frac{261}{29} + \frac{300}{29}  \\
                 0 & 1 & -\frac{50}{29} \\
                 0 & 18 & -10 \\
            } \\
            &=
            \matr{
                 1 & 0 & -\frac{39}{29}  \\
                 0 & 1 & -\frac{50}{29} \\
                 0 & 18 & -10 \\
            } \\
        \Gamma_6
        \Gamma_5
        \Gamma_4
        \Gamma_3
        \Gamma_2
        \Gamma_1
        A
            &=
            \matr{
                 1 & 0 &  \frac{39}{29}  \\
                 0 & 1 & -\frac{50}{29} \\
                 0 & 0 & -10  - (18)\parens{-\frac{50}{29}} \\
            } \\
            &=
            \matr{
                 1 & 0 &  \frac{39}{29}  \\
                 0 & 1 & -\frac{50}{29} \\
                 0 & 0 & -\frac{290}{29}  + \frac{900}{29} \\
            } \\
            &=
            \matr{
                 1 & 0 &  \frac{39}{29}  \\
                 0 & 1 & -\frac{50}{29} \\
                 0 & 0 &  \frac{610}{29} \\
            } \\
    \end{align*}

And just to verify

\begin{lstlisting}
>>> -9 + 6*50/29
1.3448275862068968
>>> -39/29
1.3448275862068968
>>> -10 + 18*50/29
21.03448275862069
>>> 610/29
21.03448275862069
\end{lstlisting}

    \item Alright we're almost done with this nightmare. Just have to convert
    the bottom right entry to a 1, and then eliminate the entries above it.

    I'm not going to show the intermediate steps because copy and pasting
    things from Python is tedious.

    Alright let's get started

    \begin{align*}
        \Gamma_7
            &=
            \matr{
                 1 & 0 & 0 \\
                 0 & 1 & 0 \\
                 0 & 0 & \frac{29}{610} \\
            } \\
        \Gamma_8
            &=
            \matr{
                 1 & 0 & 0 \\
                 0 & 1 & \frac{50}{29} \\
                 0 & 0 & 1 \\
            } \\
        \Gamma_9
            &=
            \matr{
                 1 & 0 & -\frac{39}{29} \\
                 0 & 1 & 0 \\
                 0 & 0 & 1 \\
            } \\
        \Gamma_9
        \Gamma_8
        \Gamma_7
        \Gamma_6
        \Gamma_5
        \Gamma_4
        \Gamma_3
        \Gamma_2
        \Gamma_1
        A
            &=
            \matr{
                 1 & 0 & 0 \\
                 0 & 1 & 0 \\
                 0 & 0 & 1 \\
            } \\
    \end{align*}

\begin{lstlisting}
>>> G7 = np.matrix([[1,0,0], [0,1,0], [0,0,29/610]])
>>> G7*G6*G5*G4*G3*G2*G1*A
matrix([[ 1.00000000e+00,  8.32667268e-17,  1.34482759e+00],
        [ 1.38777878e-17,  1.00000000e+00, -1.72413793e+00],
        [ 0.00000000e+00, -1.38777878e-17,  1.00000000e+00]])
>>> G8 = np.matrix([[1,0,0], [0,1,50/29], [0,0,1]])
>>> G8*G7*G6*G5*G4*G3*G2*G1*A
matrix([[ 1.00000000e+00,  8.32667268e-17,  1.34482759e+00],
        [ 0.00000000e+00,  1.00000000e+00, -2.22044605e-16],
        [ 0.00000000e+00, -1.38777878e-17,  1.00000000e+00]])
>>> G9 = np.matrix([[1,0,-39/29], [0,1,0], [0,0,1]])
>>> G9*G8*G7*G6*G5*G4*G3*G2*G1*A
matrix([[ 1.00000000e+00,  1.38777878e-16, -1.11022302e-16],
        [ 0.00000000e+00,  1.00000000e+00, -2.22044605e-16],
        [ 0.00000000e+00, -1.38777878e-17,  1.00000000e+00]])
\end{lstlisting}
\end{enumerate}

Alright. Go have a cigarette, contemplate what the hell just happened, and come
back to this.

Maybe think about this: since
$A^{-1} =
\Gamma_9
\Gamma_8
\Gamma_7
\Gamma_6
\Gamma_5
\Gamma_4
\Gamma_3
\Gamma_2
\Gamma_1
$, and $A^{-1}$ is a two-sided inverse, what do the matrices $\Gamma_9$,
$\Gamma_8$, etc do when you multiply on the right?

That is, what does the sequence of matrices 

\begin{align*}
    &A, \\
    &    \quad A\Gamma_9 \\
    &    \quad A\Gamma_9\Gamma_8, \\
    &    \quad A\Gamma_9\Gamma_8\Gamma_7, \\
    &    \quad \dots
\end{align*}

look like? What action does each $\Gamma$ have when you multiply on the right?

We know from just logic that eventually it'll get to the identity. But what
path does it take to get there?

Go have your cigarette.

And when you come back, maybe try that out.

It will be a lot more enlightening if you use a language that has exact
arithmetic, such as Erlang with QAnal:

\begin{lstlisting}[language=Erlang]
#!/usr/bin/env escript

% Shows gaussian elimination manually

main([]) ->
    A = qq:qm("-1 -6  9;"
              " 5  1  5;"
              "-2  6  8"),

    G1 = qq:qm("-1  0  0;"
               " 0  1  0;"
               " 0  0  1"),

    G2 = qq:qm(" 1  0  0;"
               "-5  1  0;"
               " 0  0  1"),

    G3 = qq:qm(" 1  0  0;"
               " 0  1  0;"
               " 2  0  1"),

    G4 = qq:qm(" 1     0  0;"
               " 0 -1/29  0;"
               " 0     0  1"),

    G5 = qq:qm(" 1 -6  0;"
               " 0  1  0;"
               " 0  0  1"),

    G6 = qq:qm(" 1   0  0;"
               " 0   1  0;"
               " 0 -18  1"),

    G7 = qq:qm(" 1  0      0;"
               " 0  1      0;"
               " 0  0 29/610"),

    G8 = qq:qm(" 1  0     0;"
               " 0  1 50/29;"
               " 0  0     1"),

    G9 = qq:qm(" 1  0 -39/29;"
               " 0  1      0;"
               " 0  0      1"),


    ok =
        qq:pp(
            qq:x(
                [
                  A
                %, G9
                %, G8
                %, G7
                %, G6
                %, G5
                %, G4
                %, G3
                %, G2
                %, G1
                %, A
                ]
            )
        ).
\end{lstlisting}

That file can be downloaded from
\url{https://gitlab.com/DoctorAjayKumar/qanal/-/raw/master/ebin/gauss.erl} .
(clickable link)

Or, alternatively Racket

\lstinputlisting[language=Lisp]{include/code/gauss_example.rkt}

That file can be downloaded from
\url{https://gitlab.com/DoctorAjayKumar/qanal/-/raw/master/doc/include/code/gauss_example.rkt} .
(clickable link)

%-----------------------------------------------------------------------------%
\section{The joggerliciousness of floats, and the philosophy of QAnal}
%-----------------------------------------------------------------------------%

QAnal is an educational/research library. However, it seems likely it might
accidentally be useful. For the purposes of education, it is not helpful at all
to have to deal with the triviality of floating point arithmetic. Therefore,
as a matter of principle, QAnal does not use floating point arithmetic at all.

\subsection{But muh eigenvalues}

There are a lot of quantities of fundamental importance that take us out of the
bounds of rational arithmetic. For instance, this matrix has irrational
eigenvalues ($\pm \sqrt{2}$):

\[ \matr{0 & 2 \\ 1 & 0} \]

\begin{lstlisting}
>>> M = np.matrix('0 2; 1 0')
>>> np.linalg.eigvals(M)
array([ 1.41421356, -1.41421356])
\end{lstlisting}

We are going to stubbornly insist on never using floats. This means: useful
quantities that in theory require floating-point arithmetic (for instance,
angles in trigonometry, or eigenvalues in linear algebra) will have to be
replaced with rational alternatives that ultimately contain the same
information.

% TODO find direct quotation with reference

The inspiration for this comes from the work of Norman Wildberger.  According
to Wildberger, it usually happens to be the case that these rational
alternatives are much cleaner than their transcendental counterparts, and
insisting on rational representations of quantities tends to point one in the
correct direction. \cite{needed}

I have not seen enough information to draw that conclusion myself. However, the
prospect of being able to perform numerical analysis without floats is so
lucrative that it is worth pursuing to its endpoint.

%-----------------------------------------------------------------------------%
\section{The operations}
%-----------------------------------------------------------------------------%

For a reference on this section, see \textcite[p. 10]{Artin}.

There are three operations in Gaussian elimination:

\begin{enumerate}
    \item Multiplying a row by a constant
    \item Adding a multiple of one row to another row
    \item Swapping rows
\end{enumerate}

It's worth pausing right now, looking back and trying to figure out what
matrices correspond to these operations, by yourself, without looking below.

The example above had the first two, but did not require swapping rows. Try to
cook up a matrix that swaps two rows.

\newpage

There are some obvious questions that now would be a good time to address: such
as ``what if we want to do column operations instead of row
operations?'',
\footnote{
    Answer: do row operations on the \emph{transpose} of $A$, and then use the
    facts that (1) $(AB)^T = B^T A^T$o where $\cdot^T$ is the transpose of $A$, and
    (2) rule (1) applies recursively.
}
or ``what if the matrix is not invertible?'',
\footnote{
    Answer: the matrix that Gaussian elimination converges to, $\Gamma_n \dots
    \Gamma_2 \Gamma_1A$ (note the $A$ on the end) allows one to easily state
    precisely \emph{how} the matrix is not invertible. To make this statement
    precise requires a further discussion of when functions in general are not
    invertible, which will come later.

    Generally, square matrices are \emph{almost always}\footnotemark\
    invertible, rectangular matrices are \emph{never} invertible, but which way
    it is rectangular (tall versus wide) usually dictates \emph{how} it is not
    invertible.

    At any rate, Gaussian elimination will precisely lay out how the matrix is
    not invertible, in a way that is precise and useful computationally.
}
\footnotetext{
    The phrase ``almost always'' has a very precise meaning.
}
which will be answered very cleanly and precisely later on.


\begin{enumerate}
\item
    To multiply a row by a constant, you take the identity matrix and change
    the entry of that row from $1$ to whatever the constant is. So for
    instance, if I'm operating on matrix with 5 rows, and I want to multiply
    the 2nd row by $7$:

    \begin{displaymath}
        \matr{
            1  &   &   &   &   \\
               & 7 &   &   &   \\
               &   & 1 &   &   \\
               &   &   & 1 &   \\
               &   &   &   & 1 \\
        }
    \end{displaymath}

    Note: by convention, blank entries are $0$.

\item
    To add $n$ times row $i$ to row $j$, you put $n$ in the $j$-th row, $i$-th
    column of $\Gamma$. So for instance above, we had

    \begin{align*}
        \Gamma_1 A
            &=
            \matr{
                 1 &  6 & -9 \\
                 5 &  1 &  5 \\
                -2 &  6 &  8 \\
            }
    \end{align*}

    At this point, we're trying to eliminate all of the nonzero entries in the
    first column. The next move is to add $-5$ times the first row to the
    second row. Thus I came up with the matrix

    \begin{align*}
        \Gamma_2 &=
            \matr{
                 1 & 0 & 0 \\
                -5 & 1 & 0 \\
                 0 & 0 & 1 \\
            } \\
    \end{align*}

    And thus we had

    \begin{align*}
        \Gamma_2
        \Gamma_1
        A
        =
        \matr{
             1 &   6 & -9 \\
             0 & -29 & 50 \\
            -2 &   6 &  8 \\
        }
    \end{align*}
\item
    To swap rows, you swap the rows in the identity matrix:

    \begin{align*}
        S
            &=
            \matr{
                0 & 1 & 0 \\
                1 & 0 & 0 \\
                0 & 0 & 1 \\
            } \\
        S
        \Gamma_2
        \Gamma_1
        A
            &=
            \matr{
                 0 & -29 & 50 \\
                 1 &   6 & -9 \\
                -2 &   6 &  8 \\
            } \\
    \end{align*}


    This is a useful stopping point to ask, what is the inverse of a
    row-swapping matrix?
\end{enumerate}

The matrices that are used to do row operations are called \term{elementary
matrices}. Every elementary matrix is invertible,
\footnote{
    Exercise (easy): find their inverses
}
and every invertible matrix is a product of elementary matrices.
\footnote{
    Exercise (easy): To see this, replay Gaussian elimination in reverse on the
    identity matrix.
}

%-----------------------------------------------------------------------------%
\section{The algorithm}
%-----------------------------------------------------------------------------%

We still have not described the algorithm in sufficient detail to explain it to
a computer.

First, we have to cleanly articulate when the algorithm is complete. The
algorithm is complete when $\Gamma_n\dots\Gamma_2\Gamma_1 A$ is in
\term{reduced row echelon form}.

This is a good point to interject with

\begin{theorem}
    A matrix is invertible if and only if its reduced row echelon form is the
    identity matrix
\end{theorem}

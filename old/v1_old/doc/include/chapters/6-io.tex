%       Every operation, calculation, and concept, no matter how arbitrarily
%       complex, reduces to adding integers together. There are no new concepts
%       in QAnal. Everything is just putting lipstick on adding integers
%       together.
%
%   -- Dr. Ajay Kumar PHD, Founder of QAnal
%=============================================================================%
\chapter{Introduction and Outline}

\ajayquote
%=============================================================================%

This started when I tried to learn how neural networks work. Tutorials online,
and textbooks were incomplete for one of two reasons

\begin{enumerate}
\item
    The tutorial would fail to adequately explain backpropagation (the part
    where the neural network learns, you know, the most important part), and
    would give no code implementation.
\item
    The tutorial would explain backpropagation in a confusing and convoluted
    manner, and then give an implementation that was confusing and overly
    complicated.

    Usually the excuse for the overly complicated implementation of backprop is
    ``performance'' reasons. In any event, the code was completely
    incomprehensible, and unsuitable for pedagogical purposes.
\end{enumerate}

In my opinion, the problem with these tutorials was that their authors did not
understand the multidimensional chain rule of differential calculus, and
therefore failed to explain backpropagation clearly (both to themselves and to
their audience). I believed I had a better understanding of the chain rule, and
set out to make a video tutorial which accomplished the following things

\begin{enumerate}
\item
    Clearly explain the chain rule, particularly the version in multiple
    dimensions.\footnote{
        The chain rule is \emph{obvious} in multiple dimensions, and in a
        single dimension seems \emph{arbitrary}. Most explanations of the
        multidimensional chain rule explained it as a more complicated case of
        the single-dimensional chain rule. No, the single-dimensional chain
        rule is properly viewed as a degenerate case of the multi-dimensional
        chain rule.
    }

\item
    Give an implementation of neural networks in a functional language, with
    code that was self-contained, clean, well-documented, and obviously correct.
\end{enumerate}

I wrote up some notes for my video outlining the mathematical exposition I was
to make. I sent the notes to people who were hard-constraint members of my
target audience. I knew these people to be extremely competent programmers, and
decided that at a minimum, these people had to be able to understand everything
in the exposition.

The feedback I got was unexpected. These people did not know very basic things
required to understand the exposition. For instance,

\begin{enumerate}
\item
    Some of these people did not know what is meant by the ``derivative'' of
    a function.
\item
    Some were unaware that matrix multiplication is \textbf{not} commutative.
\item
    Some did not know the behavior of shapes of matrices under multiplication;
    i.e. did not know that a $A \times B$ matrix times a $B \times C$ matrix
    creates a $A \times C$ matrix.
\item
    Some could not read matrix notation. I was asked for instance what is meant
    by the phrase ``$A \times B$ matrix''

\item
    Some did not understand what is meant by the symbol $\frac{d}{dx}$.

\item
    I was asked to translate into code things which \emph{cannot} be translated
    cleanly into code; for instance, I cannot write a function to compute a
    derivative that is obviously correct.
\end{enumerate}

So I was left with two options:

\begin{enumerate}
\item
    Proceed with my tutorial, knowing that people in the 99th percentile of
    background knowledge would not be able to understand it.
\item
    Explain everything from scratch.
\end{enumerate}

For a variety of reasons, I chose the second option. And that's how this
project got started.

The first item on the agenda is to explain the basics of linear algebra. This
quickly forces a hard left turn in the goals of the project. The very first
(and most fundamental) topic in linear algebra is \term{Gaussian elimination}.

Implementing Gaussian elimination requires performing a lot of division. On
computers, this usually means floating point arithmetic. Remember, the hard
constraint coming in is that all code examples must be self-contained, clean,
well-documented, and obviously correct.  There is almost nothing one can do
with floating point arithmetic that is obviously correct. Therefore,
implementing Gaussian elimination using floating-point arithmetic would violate
the hard constraint. Therefore, I made the decision to implement arithmetic
from scratch without using floats. This is not as difficult as it sounds.

%=============================================================================%
\chapter{The QAnal Fissure: how precision is the natural enemy of exactritude.}
%=============================================================================%

\begin{center}
    \ajaypic\

    \begin{quote}
        {\em
            The most important innovation in QAnal is that it makes and
            enforces a conceptual barrier between exact objects and precise
            objects. This conceptual dichotomy between exactritude and
            precision---called The QAnal Fissure---is portable and flexible,
            and extends far beyond the bounds of QAnal.

            The Founder allows you the artistic freedom to tailor The QAnal
            Fissure to your own application, based perhaps on the constraints
            imposed by the tools you are using to address your problem.

            The QAnal Fissure is a gift to you from The Founder, and it would
            be wise of you to humbly and gratefully accept it, and to keep in
            mind the love, generosity, and grace of The Founder whenever you
            use it.
        }

        -- Dr. Ajay Kumar PHD, The Founder
    \end{quote}
\end{center}

The Founder commanded, let us first make an observation, then in due time we
shall be enlightened as to the true extent of his love, generosity, and grace.

Numerical data from the real world tends to be in one of two formats:

\begin{enumerate}
\item
    Exact counts; for instance, the Boston Red Sox won 24 games in 2020.
\item
    Measurements which carry with them the notion of precision; for instance,
    the sun is ``93 million miles'' away from the earth.
\end{enumerate}

%And the Founder commanded, put it first in your mind this observation as the
%motivating observation which led him to divine The QAnal Fissure.

Objects that have precision are \term{precise}. Objects that do not have the
notion of precision are \term{exact}.

Precise and exact are synonyms in common parlance. In QAnal, they are antonyms.

Standard mathematics largely ignores The QAnal Fissure, seemingly due to not
ever having articulated the concept, or perhaps never having developed
effective nomenclature for it. However, if you entertain the concept for even a
short period of time, its utility becomes obvious, and even necessary.

QAnal thus is split into two parallel and dual fields:

\begin{enumerate}
\item XAnal: exact analysis
\item PreAnal: precise analysis
\end{enumerate}

Any concept or result in one field typically has an analog in the other. And
when one doesn't, there's a very interesting reason why not.

The scientific community has a developmentally disabled ancestor of PreAnal
called \term{significant figures} or \term{sigfigs}. Sigfigs are the
jumping off point for PreAnal.

%-----------------------------------------------------------------------------%
\section{The QAnal Fission Property}
%-----------------------------------------------------------------------------%

\begin{quote}
    \emph{
        Exact objects should typically be thought of as acting on precise
        objects, rather than being transformed into precise objects.
    }

    -- Dr. Ajay Kumar PHD, The Founder
\end{quote}

Exact objects may be transformed into precise objects. But never can a precise
object be transformed into an exact object.

This is the only hard constraint of The QAnal Fissure. Everything beyond this,
including which things are classified as exact or precise, is a matter of
artistic choice, preference, taste, or domain-specific requirement.

%-----------------------------------------------------------------------------%
\section{Which things are exact, and which things are precise?}
%-----------------------------------------------------------------------------%

Which choices you make in your application are up to you. However, here exist
some general guidelines that are worth following:

\begin{enumerate}
\item The only numbers that are exact are integers.
\item
    Symbolic expressions can be exact, such as $\pi$, $\sqrt{2}$, or
    $\frac{3}{4}$.

    However, their decimal representations are typically precise.

    That is, $\pi$ is exact, but $3.14159$ is precise.

    You should take care to admit as few exact symbols as possible to minimize
    the risk of violating the Fission Property.

\item
    Any number with a decimal point is precise. This means $\frac{1}{2}$ is
    exact but $0.5$ is precise.

\item
    Properties are exact, and so is algebra. For instance,
    $\frac{a}{b}\frac{c}{d} = \frac{ac}{bd}$ is exact.

\item
    Evaluating an exact symbol typically produces a precise number. For
    instance, evaluating the exact symbol $\frac{1}{2}$ produces the precise
    number $0.5$.

    However, $\sin(\frac{\pi}{2}) = 1$ is (concievably) exact

\item
    Limits (and generally, infinite processes) should be avoided at all costs.
    Only introduce limits with the utmost care.

    In particular, avoid the temptation to admit infinite decimals as valid
    objects. $\pi$ is the atomic symbol $\pi$. Only when $\pi$ is evaluated
    does it become the (arbitrary but finite length) decimal $3.14159...N$,

    If you must introduce a limiting process (for instance, an integral),
    generally:

    \begin{enumerate}
    \item The limit of an exact sequence is exact
    \item The limit of an precise sequence is precise
    \end{enumerate}

    Limits blur the line between exact and precise, which is why they should be
    avoided. Limits and infinite processes can be dangerous because you may
    accidentally convince yourself that a limit of a precise sequence can be
    exact. No. If one object in a calculation is precise, then every object in
    that calculation becomes precise.

    Things that normally involve limits may be able to be stated using an
    exact algebraic definition without any appeal to infinite processes. For
    instance, one can \term{define} the derivative of the exact symbol $x^n$ to
    be the exact symbol $nx^{n-1}$, without any appeal to limiting geometry.

    As you will see, much of the (exact) internal logic of PreAnal relies on
    properties of logarithms, which we can define to be exact and atomic
    without ever making any appeal to computation.

\item
    Logic is exact.

\item
    Estimates are precise.

\item
    Any quantity obtained by counting is exact.

\item
    Any quantity obtained by measurement is precise.

\item
    If a quantity has multiple competing units of measure (for instance, feet
    versus meters), then it is probably precise.
\end{enumerate}

(table)

A feature of PreAnal is including a comprehensive, simple, powerful, and clean
algebraic language of finite-precision units of measure. This is called
\term{precise measure theory}.

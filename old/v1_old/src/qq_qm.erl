% @doc
% QAnal: Rational Analysis
%
%       Every operation, calculation, and concept, no matter how arbitrarily
%       complex, reduces to adding integers together. There are no new concepts
%       in QAnal. Everything is just putting lipstick on adding integers
%       together.
%
%   -- Dr. Ajay Kumar PHD, Founder of QAnal
%
% qq_qm: rational matricees
%
% This eliminates stupid corner cases and floating point errors in e.g.
% inverting matrices
% @end

% FIXME rewrite operations that use lists to use arrays instead
%
% TODO Gaussian elimination
% TODO real pretty printer
%
% DONE function for selective replace, like selrep(QM, [{{1,1}, 3}]
% DONE zeroes
% DONE sparse
% DONE write diag/1 for creating a diagonal matrix (also sdiag)
% DONE write id_n/2 for creating an arbitrary identity matrix
-module(qq_qm).

% FIXME reorganize these to be more like the qq_c module
% FIXME add every arithmetic function/order-of-operands like qmtimesq, ztimesqm

% types
-export_type([shape/0,
              idx0/0,
              idx1/0,
              qm/0]).
% accessors
-export([shape/1,
         array/1,
         ij2i0/2,
         i02ij/2,
         ijth/2,
         nthcol/2,
         nthrow/2,
         rows/1,
         cols/1]).

% low level modification
-export([ijput/3,
         selrep/2]).

% constructors
-export([s2qm/1,
         from_rows/1,
         from_zrows/1,
         % these are not constructors, move these
             qm2s/1,
             pp/1,
         % end
         col/1,
         zcol/1,
         row/1,
         zrow/1,
         id2/0,
         id3/0,
         zeros/1,
         idn/1,
         sparse/2,
         diag/1]).

% these are accessors, move these
-export([is_row/1,
         is_col/1,
         is_qm/1,
         is_rcef/1,
         is_rref/1]).

% standard linalg operations
-export([transpose/1,
         qmplus/2,
         qmtimes/2,
         qtimesqm/2,
         ztimesqm/2,
         qmprod/1,
         s2qls/1,
         dot/2
         ]).

%% Gauss attempt 1
%-export([rcef/1,
%         rcef_steps/1,
%         rref/1,
%         rref_steps/1,
%         row_swap_matrix/1,
%         rsm/3]).

% Gauss attempt 2
-export_type(
    [ gstep/0
    ]
).
-export(
    [ gstep/1
    , gstep_debug/2
    , rcef/1
    , rref/1
    , det/1
    , is_invertible/1
    , inverse_left/1
    , inverse_right/1
    ]
).


-type z() :: qq_z:z().
-type q() :: qq_q:q().
-record(qm,
        {shape :: shape(),
         array :: array:array(q())}).

-type shape() :: {pos_integer(), pos_integer()}.
-type qm()    :: #qm{}.

-type idx0()  :: non_neg_integer().
-type idx1()  :: pos_integer().


%-----------------------------------------------------------------------------%
% ACCESSORIES
%-----------------------------------------------------------------------------%

-spec shape(qm()) -> shape().
shape(#qm{shape=S}) -> S.


-spec array(qm()) -> array:array(q()).
array(#qm{array=A}) -> A.


-spec ij2i0(shape(), {idx1(), idx1()}) -> idx0().
% @doc Given a shape, a row index, and a column index, convert into a 0-index
% for accessing the correct element in the array
%
% No bounds checks
% @end
%
% FIXME add bounds checks
% FIXME change the type of this so that dialyzer catches accidental argswap
ij2i0({_, NC}, {RIdx1, CIdx1}) ->
    RIdx0 = RIdx1 - 1,
    CIdx0 = CIdx1 - 1,
    ArrIdx0 = RIdx0*NC + CIdx0,
    ArrIdx0.


-spec i02ij(Shape, idx0()) -> {idx1(), idx1()}
    when Shape :: {pos_integer(), pos_integer()}.
% @doc Convert 0-index to ij-index
%
% No bounds checks
% @end
%
% FIXME add bounds checks
i02ij({_, NC}, ArrIdx0) ->
    % Column index 0 will be ArrIdx0 mod NumberOfColumns
    CI0 = ArrIdx0 rem NC,
    RI0 = ArrIdx0 div NC,
    CI1 = CI0 + 1,
    RI1 = RI0 + 1,
    {RI1, CI1}.


-spec ijth(qm(), {idx1(), idx1()}) -> q().
% @doc This is "safe" in that it does bounds checks, and gives a verbose error
% message if it fails.
%
% ijth_unsafe does not do bounds checks
% @end

% FIXME the arguments are backwards, it should match lists:nth
ijth(M=#qm{shape={NR,NC}}, {RIdx1, CIdx1})
            when 1 =< RIdx1, RIdx1 =< NR,
                 1 =< CIdx1, CIdx1 =< NC ->
    ijth_unsafe(M, {RIdx1, CIdx1});
ijth(#qm{shape=Shape}, {RIdx1, CIdx1}) ->
    shape_error("cannot get {~w,~w}-th entry from matrix with shape ~w",
                [RIdx1, CIdx1, Shape]).


-spec nthcol(idx1(), qm()) -> [q()].
% @doc return list of entries in column
%
% No bounds check
% @end
%
% FIXME add bounds check
nthcol(J, QM=#qm{shape={NR,_}}) ->
    IJPairs = [{I, J} || I <- lists:seq(1, NR)],
    [ijth(QM, IJ) || IJ <- IJPairs].


-spec cols(qm()) -> [[q()]].
% @doc list of cols
% @end
cols(QM = #qm{shape={_, NC}}) ->
    [nthcol(I, QM) || I <- lists:seq(1, NC)].


-spec nthrow(idx1(), qm()) -> [q()].
% @doc return list of entries in row
%
% No bounds check
% @end
%
% FIXME add bounds check
nthrow(I, QM=#qm{shape={_,NC}}) ->
    IJPairs = [{I, J} || J <- lists:seq(1, NC)],
    [ijth(QM, IJ) || IJ <- IJPairs].


-spec rows(qm()) -> [[q()]].
% @doc list of rows
% @end
rows(QM = #qm{shape={NR, _}}) ->
    [nthrow(I, QM) || I <- lists:seq(1, NR)].

%-----------------------------------------------------------------------------%
% LOW-LEVEL MUTATIONS
%-----------------------------------------------------------------------------%

-spec ijput(qm(), {idx1(), idx1()}, q()) -> qm().
% @doc ijput(QM, {I,J}, Q) puts Q into spot I,J
% @end
%
% FIXME put in a shape error for out-of-bounds
ijput(QM=#qm{shape=Shape, array=Arr}, Idx1Pair, Q) ->
    case qq_q:is_q(Q) of
        true ->
            ArrIdx0 = ij2i0(Shape, Idx1Pair),
            NewArr = array:set(ArrIdx0, Q, Arr),
            QM#qm{array = NewArr};
        false ->
            {type_error, not_rational, Q}
    end.


-spec selrep(qm(), Pairs) -> qm()
    when Pairs :: [{{idx1(), idx1()}, q()}].
% @doc selective replace: run ijput/3 on each pair in Pairs
selrep(QM, Pairs) ->
    lists:foldl(
        fun({Idx1Pair, Q}, QMAccum) ->
            NewQMAccum = ijput(QMAccum, Idx1Pair, Q),
            NewQMAccum
        end,
        QM,
        Pairs
    ).

%-----------------------------------------------------------------------------%
% CONVERSION/CONSTRUCTION
%-----------------------------------------------------------------------------%

-spec s2qm(string()) -> qm().
% @doc
% FIXME should be the left-inverse of qm2s, meaning s2qm(qm2s(QM)) = QM
%
% Meant to mimick numpy's syntax for constructing matrices from strings
%
% - integers are automatically converted to rationals
%
% - negative numbers and fractions are denoted with the obvious syntax
%
% - no spaces are allowed within a number; i.e. different tokens will map to
%   different entries
%
% - row separation is denoted using a semicolon
%
% - Infers the shape of the matrix from the total number of entries and the
%   number of entries in the first row. In particular, that means for certain
%   malformed inputs, it will parse successfully, but possibly incorrectly.
%
%   You are responsible for making sure the inputs you send to this function
%   this make sense, and/or checking that the shape of the resulting matrix is
%   what you expect it to be
%
%       1> qq_qm:s2qm("1 0 0; 0 1; 0 0 1 0").
%       {qm,{3,3},
%           {array,9,0,undefined,
%                  {{q,1,1},
%                   {q,0,1},
%                   {q,0,1},
%                   {q,0,1},
%                   {q,1,1},
%                   {q,0,1},
%                   {q,0,1},
%                   {q,1,1},
%                   {q,0,1},
%                   undefined}}}
% @end
%
% FIXME add more robust shape check
s2qm(S) ->
    from_rows(s2qls(S)).


-spec from_rows(nonempty_list(nonempty_list(q()))) -> qm().
% @doc Will not throw a shape error if you give it inconsistently sized lists.
%
% Works by flattening the list, creating array, and reshaping.
%
% Infers shape from length of list and length of first row.
% @end
%
% FIXME add more robust shape check
% FIXME rename to rows
% TODO also add columns
from_rows(Qss = [FirstRow | _]) ->
    NR = erlang:length(Qss),
    NC = erlang:length(FirstRow),
    % concat is a joggerlicious function
    Qs = lists:flatten(Qss),
    Shape = {NR, NC},
    from_list(Qs, Shape).


-spec from_zrows(nonempty_list(nonempty_list(z()))) -> qm().
% @doc Will not throw a shape error if you give it inconsistently sized lists.
%
% Works by flattening the list, creating array, and reshaping.
%
% Infers shape from length of list and length of first row.
% @end
%
% FIXME add more robust shape check
% FIXME rename to zrows
from_zrows(ZLs) ->
    ZL2QL =
        fun(ZL) ->
            lists:map(fun(Z) -> qq_q:z2q(Z) end, ZL)
        end,
    QLs = lists:map(ZL2QL, ZLs),
    from_rows(QLs).


-spec pp(qm()) -> ok.
pp(QM) ->
    io:format("~s", [qm2s(QM)]).


-spec qm2s(qm()) -> string().
% @doc pretty print matrix
% @end
%
% FIXME make qm2s which would be the right inverse of s2qm (meaning
%       s2qm(qm2s(QM)) recovers the same matrix; this means s2qm should
%       interpret newlines as new rows, and ignore [ ]. Then this should just
%       io:format whatever that function does. Also should move q2s to the
%       qq_q module, and you know do the inverse stuff
qm2s(QM) ->
    % better name needed for pp_row
    ColWidths = col_widths(QM),
    FieldWidths = [CW + 1 || CW <- ColWidths ],
    Rows = rows(QM),
    format_rows(Rows, FieldWidths).


-spec format_rows([[q()]], [pos_integer()]) -> string().
format_rows([], _) ->
    [];
format_rows([Qs | Qss], FieldWidths) ->
    format_row(Qs, FieldWidths) ++ format_rows(Qss, FieldWidths).

-spec format_row([q()], [pos_integer()]) -> string().
format_row(Qs, FieldWidths) ->
    Qs_FWs = lists:zip(Qs, FieldWidths),
    lists:flatten(
        [
            "[",
            [format_q(Q, FW) ++ " " || {Q, FW} <- Qs_FWs],
            "]\n"
        ]
    ).


-spec format_q(q(), pos_integer()) -> string().
format_q(Q, FW) ->
    QStr = qq_q:q2s(Q),
    % will always be >= 1
    NumFrontSpacesToAdd = FW - erlang:length(QStr),
    Padding = [$  || _ <- lists:seq(1, NumFrontSpacesToAdd)],
    Padding ++ QStr.


-spec col_widths(qm()) -> [pos_integer()].
col_widths(QM) ->
    Cols = cols(QM),
    [col_width(Col) || Col <- Cols].

-spec col_width([q()]) -> pos_integer().
col_width(Qs) ->
    StrLens = [erlang:length(qq_q:q2s(Q)) || Q <- Qs],
    lists:max(StrLens).


-spec col(Col) -> qm()
    when Col :: nonempty_list(q()).
col(Ns) ->
    M = #qm{shape = {1, Len}}
      = row(Ns),
    reshape(M, {Len, 1}).


-spec zcol(Col) -> qm()
    when Col :: nonempty_list(z()).
zcol(Zs) ->
    Qs = lists:map(fun qq_q:z2q/1, Zs),
    col(Qs).


-spec row(Row) -> qm()
    when Row :: nonempty_list(q()).
row(Ns) ->
    Len = erlang:length(Ns),
    InitArr = array:new(Len),
    Insert =
        fun(Elem, {Idx0, Arr}) ->
            NewArr = array:set(Idx0, Elem, Arr),
            {Idx0+1, NewArr}
        end,
    {_, Arr} = lists:foldl(Insert, {0, InitArr}, Ns),
    Matrix = #qm{shape = {1, Len},
                array = Arr},
    Matrix.


-spec zrow(Row) -> qm()
    when Row :: nonempty_list(z()).
zrow(Zs) ->
    Qs = lists:map(fun qq_q:z2q/1, Zs),
    row(Qs).


-spec id2() -> qm().
% @doc 2x2 identity matrix
id2() -> s2qm("1 0 ; 0 1").


-spec id3() -> qm().
% @doc 3x3 identity matrix
id3() -> s2qm("1 0 0 ; 0 1 0 ; 0 0 1").


-spec zeros(shape()) -> qm().
% @doc matrix of given shape with all entries equal to q0()
zeros(Shape = {NR, NC}) ->
    Arr =
        array:new(
            [
                {size, NR*NC},
                {default, qq_q:q0()}
            ]
        ),
    #qm{shape = Shape,
        array = Arr}.


-spec idn(N :: pos_integer()) -> qm().
% @doc NxN identity matrix
idn(N) ->
    ReplacePairs = [{{I,I}, qq_q:q1()} || I <- lists:seq(1,N)],
    sparse({N, N}, ReplacePairs).


-spec sparse(shape(), Pairs) -> qm()
    when Pairs :: [{{idx1(), idx1()}, q()}].
% @doc composes zeros/1 and selrep/1
sparse(Shape, Pairs) ->
    InitQM = zeros(Shape),
    selrep(InitQM, Pairs).


-spec diag([q(), ...]) -> qm().
% @doc makes square diagonal matrix with given diagonals
diag(Qs) ->
    Len = erlang:length(Qs),
    Pairs = [{{I,I}, Q} || {I,Q} <- lists:zip(lists:seq(1, Len), Qs)],
    sparse({Len, Len}, Pairs).

%-----------------------------------------------------------------------------%
% is_*
%-----------------------------------------------------------------------------%


-spec is_row(term()) -> boolean().
is_row(#qm{shape={1, _}}) -> true;
is_row(_)                -> false.


-spec is_col(term()) -> boolean().
is_col(#qm{shape={_, 1}}) -> true;
is_col(_)                -> false.


-spec is_qm(term()) -> boolean().
is_qm(#qm{}) -> true;
is_qm(_)     -> false.


-spec is_rcef(qm()) -> boolean().
% @doc checks if is in reduced column echelon form
is_rcef(QM) ->
    QMT = transpose(QM),
    is_rref(QMT).


-spec is_rref(qm()) -> boolean().
% @doc checks if is in reduced row echelon form
%
% Code seems correct but is extremely joggerlicious
% @end
%
% TODO implement unit testing/PropER
% FIXME make the subcalls less joggerlicious/use arrays instead of lists
is_rref(QM) ->
    Rows = rows(QM),
    all_rows_are_all_zero_or_start_with_1(Rows)
        andalso all_rows_that_are_all_zero_are_at_the_bottom(Rows)
        andalso the_leading_1_of_any_row_that_is_not_all_zeros_is_strictly_to_the_right_of_the_leading_1_above_it(Rows)
        andalso all_columns_with_leading_1_are_otherwise_0s(Rows).


%-----------------------------------------------------------------------------%
% Standard matrix operations
%-----------------------------------------------------------------------------%


-spec transpose(qm()) -> qm().
transpose(#qm{shape=Shape={NR,NC}, array=OldArr}) ->
    InitNewArr = array:new(NR*NC),
    NewShape = {NC, NR},
    TransposeElem =
        fun(OldIdx0, Elem, NewArr) ->
            {OldI, OldJ} = i02ij(Shape, OldIdx0),
            NewI = OldJ,
            NewJ = OldI,
            NewIdx0 = ij2i0(NewShape, {NewI, NewJ}),
            array:set(NewIdx0, Elem, NewArr)
        end,
    NewArr = array:foldl(TransposeElem, InitNewArr, OldArr),
    #qm{shape = NewShape,
       array = NewArr}.

-spec qmplus(qm(), qm()) -> qm().
% Add two matrices with same shape
qmplus(#qm{shape = SameShape, array = Arr1},
       #qm{shape = SameShape, array = Arr2}) ->
    QPlus =
        fun(Arr1_Idx0, Elem1) ->
            Elem2 = array:get(Arr1_Idx0, Arr2),
            qq_q:qplus(Elem1, Elem2)
        end,
    Arr3 = array:map(QPlus, Arr1),
    #qm{shape = SameShape,
        array = Arr3}.


-spec qmtimes(qm(), qm()) -> qm().
% @doc this could probably be made faster by not using lists
% @end
% FIXME Rewrite without using lists
qmtimes(M1 = #qm{shape = {NR1, NC1 = MustMatch}},
        M2 = #qm{shape =      {NR2 = MustMatch, NC2}}) ->
    Dot =
        fun(Xs, Ys) ->
            Prods = lists:zipwith(fun(X, Y) -> qq_q:qtimes(X,Y) end, Xs, Ys),
            Sum = qq_q:qsum(Prods),
            Sum
        end,
    M1_IthRowIJs =
        fun(I) ->
            [{I, J} || J <- lists:seq(1, NC1)]
        end,
    M1_IthRow =
        fun(I) ->
            IthRowIJs = M1_IthRowIJs(I),
            [ijth(M1, {Eye, J}) || {Eye, J} <- IthRowIJs]
        end,
    M2_JthColIJs =
        fun(J) ->
            [{I, J} || I <- lists:seq(1, NR2)]
        end,
    M2_JthCol =
        fun(J) ->
            JthColIJs = M2_JthColIJs(J),
            [ijth(M2, {I, Jay}) || {I, Jay} <- JthColIJs]
        end,
    M1M2_IJthEntry =
        fun(I, J) ->
            Dot(M1_IthRow(I), M2_JthCol(J))
        end,
    M1M2_Shape = {NR1, NC2},
    InitNewArr = array:new(NR1*NC2),
    PutValue =
        fun(Idx0, _) ->
            {I, J} = i02ij(M1M2_Shape, Idx0),
            M1M2_IJ = M1M2_IJthEntry(I, J),
            M1M2_IJ
        end,
    M1M2_Array = array:map(PutValue, InitNewArr),
    M1M2 = #qm{shape = M1M2_Shape,
               array = M1M2_Array},
    M1M2;
qmtimes(#qm{shape=Shape1}, #qm{shape=Shape2}) ->
    shape_error("Cannot mulitply matrices with shapes ~w, ~w",
                [Shape1, Shape2]).


-spec qtimesqm(q(), qm()) -> qm().
qtimesqm(Q, QM=#qm{array=Arr}) ->
    QTimes =
        fun(_Idx0, Elem) ->
            qq_q:qtimes(Q, Elem)
        end,
    NewArr = array:map(QTimes, Arr),
    QM#qm{array=NewArr}.


-spec ztimesqm(z(), qm()) -> qm().
ztimesqm(Z, QM) ->
    Q = qq_q:z2q(Z),
    qtimesqm(Q, QM).


-spec qmprod([qm(), ...]) -> qm().
% @doc Product of matrices
qmprod([QM])        -> QM;
qmprod([QM | Rest]) -> qmtimes(QM, qmprod(Rest)).


-spec dot(qm(), qm()) -> q().
% @doc
% Take dot product of a row*col vector and fish out the scalar.
% @end
dot(M1=#qm{shape={1, N}}, M2=#qm{shape={N, 1}}) ->
    M1M2t = qmtimes(M1, M2),
    ijth(M1M2t, {1, 1});
dot(#qm{shape=S1}, #qm{shape=S2}) ->
    shape_error("Cannot dot matrix of shape ~w with matrix of shape ~w",
                [S1, S2]).


%-----------------------------------------------------------------------------%
% Internal_functions
%-----------------------------------------------------------------------------%


-spec ijth_unsafe(qm(), {idx1(), idx1()}) -> q().
% @doc This is "unsafe" in that it will give a more cryptic error message if
% you try to access an element that is out of bounds.
ijth_unsafe(#qm{shape=Shape, array=Arr}, {RIdx1, CIdx1}) ->
    ArrIdx0 = ij2i0(Shape, {RIdx1, CIdx1}),
    array:get(ArrIdx0, Arr).


-spec shape_error(io:format(), [term()]) -> no_return().
shape_error(Message, Args) ->
    EMsg = lists:flatten(io_lib:format(Message, Args)),
    erlang:error({shape_error, EMsg}).


-spec reshape(qm(), NewShape) -> qm()
    when NewShape :: {pos_integer(), pos_integer()}.
% @doc Changes the shape field, makes sure the two shapes are compatible.
%
% That is, makes sure NewNR*NewNC = OldNR*OldNC
reshape(M = #qm{shape={NR, NC}}, NewShape = {NewNR, NewNC}) when NewNR*NewNC =:= NR*NC ->
    M#qm{shape=NewShape};
reshape(#qm{shape=OldShape}, NewShape) ->
    shape_error("cannot reshape ~p into ~p; require NewNR*NewNC = OldNR*OldNC",
                [OldShape, NewShape]).


-spec from_list(nonempty_list(q()), shape()) -> qm().
% TODO consider making this public
from_list(Qs, Shape) ->
    Row = row(Qs),
    reshape(Row, Shape).


-spec s2qls(string()) -> [[q()]].
s2qls(S) ->
    SRowStrs = string:tokens(S, ";"),
    TrimmedRowStrs = lists:map(fun string:trim/1, SRowStrs),
    GetEntryQs =
        fun (RowString) ->
            % :: [string()]
            EntryStrings1 = string:tokens(RowString, " "),
            % :: [string()]
            EntryStrings2 = lists:map(fun string:trim/1, EntryStrings1),
            % :: [q()]
            [qq_q:s2q(EntryString) || EntryString <- EntryStrings2]
        end,
    lists:map(GetEntryQs, TrimmedRowStrs).




% FIXME rewrite this whole function
% FIXME should not pattern match on {q,0,1} or {q,1,1}
-spec all_columns_with_leading_1_are_otherwise_0s(Rows) -> boolean()
    when Rows :: [[q()]].
all_columns_with_leading_1_are_otherwise_0s(Rows) ->
    ColIdx1WithLeading1 =
        fun(Row) ->
            RowIsAllZero = row_is_all_zero(Row),
            case RowIsAllZero of
                true ->
                    false;
                false ->
                    LeadingZeros =
                        lists:takewhile(
                            fun(Elem) ->
                                Elem =:= qq_q:q0()
                            end,
                            Row
                        ),
                    NumLeadingZeros = erlang:length(LeadingZeros),
                    % no leading 0s -> first column
                    ThisColIdx1WithLeading1 = NumLeadingZeros + 1,
                    {true, ThisColIdx1WithLeading1}
            end
        end,
    ColumnsWithLeading1 = lists:filtermap(ColIdx1WithLeading1, Rows),
    Columns = rows( transpose( from_rows( Rows))),
    ColumnIsAllZeroesExcept1 =
        fun(ColIdx1) ->
            ThisColumn = lists:nth(ColIdx1, Columns),
            HowManyOnesInThisColumn =
                lists:foldl(
                    fun
                        ({q,0,1}, no_one_yet)                                 -> no_one_yet;
                        ({q,1,1}, no_one_yet)                                 -> have_encountered_one;
                        (_Q, no_one_yet)                                      -> column_contains_something_that_isnt_zero_or_one;
                        ({q,0,1}, have_encountered_one)                       -> have_encountered_one;
                        ({q,1,1}, have_encountered_one)                       -> more_than_one_one;
                        (_Q, have_encountered_one)                            -> column_contains_something_that_isnt_zero_or_one;
                        (_Q, more_than_one_one)                               -> more_than_one_one;
                        (_Q, column_contains_something_that_isnt_zero_or_one) -> column_contains_something_that_isnt_zero_or_one
                    end,
                    no_one_yet,
                    ThisColumn
                ),
            % ok =
            %     io:format(
            %         "ThisColumn              = ~p~n"
            %         "HowManyOnesInThisColumn = ~p~n"
            %         "~n",
            %         [
            %             ThisColumn,
            %             HowManyOnesInThisColumn
            %         ]
            %     ),
            IsThisColumnAllZeroesExcept1 =
                case HowManyOnesInThisColumn of
                    have_encountered_one                             -> true;
                    no_one_yet                                       -> false;
                    more_than_one_one                                -> false;
                    column_contains_something_that_isnt_zero_or_one  -> false
                end,
            IsThisColumnAllZeroesExcept1
        end,
    lists:foldl(
        fun(ColIdx1, Accum) ->
            Accum andalso ColumnIsAllZeroesExcept1(ColIdx1)
        end,
        true,
        ColumnsWithLeading1
    ).


-spec the_leading_1_of_any_row_that_is_not_all_zeros_is_strictly_to_the_right_of_the_leading_1_above_it([[q()]]) -> boolean().
the_leading_1_of_any_row_that_is_not_all_zeros_is_strictly_to_the_right_of_the_leading_1_above_it(Rows) ->
    check_lead_1(Rows, 0).


-spec check_lead_1(Rows, LastPivotCol) -> boolean()
    when Rows         :: [[q()]],
         LastPivotCol :: non_neg_integer().
check_lead_1([], _) ->
    true;
check_lead_1([Row | Rest], PrevPivotCol) ->
    IsQZero = fun(Q) -> Q =:= qq_q:q0() end,
    IsAllZeroes =
        lists:foldl(
            fun(Elem, Accum) ->
                Accum andalso IsQZero(Elem)
            end,
            true,
            Row
        ),
    case IsAllZeroes of
        % We can short-circuit here, since we've already checked that if the
        % row is all zeroes, then all future rows are also all zero
        true ->
            true
            ;
        false ->
            LeadingZeroes = lists:takewhile(IsQZero, Row),
            % If there's 2 leading zeroes, then the pivot is in the 3rd column.
            ThisPivotCol = erlang:length(LeadingZeroes) + 1,
            TrueForThisRow = ThisPivotCol > PrevPivotCol,
            % ok =
            %     io:format(
            %         "Row          = ~p~n"
            %         "PrevPivotCol = ~p~n"
            %         "ThisPivotCol = ~p~n"
            %         "T > R        = ~p~n"
            %         "~n",
            %         [Row,
            %          PrevPivotCol,
            %          ThisPivotCol,
            %          TrueForThisRow]
            %     ),
            TrueForThisRow andalso check_lead_1(Rest, ThisPivotCol)
    end.


-spec all_rows_are_all_zero_or_start_with_1([[q()]]) -> boolean().
all_rows_are_all_zero_or_start_with_1(Rows) ->
    lists:foldl(
        fun(Row, Acc) ->
            Acc andalso row_is_all_zero_or_starts_with_1(Row)
        end,
        true,
        Rows
    ).

-spec row_is_all_zero_or_starts_with_1([q()]) -> boolean().
% FIXME should not be pattern matching on {q,0,1} or {q,1,1}
row_is_all_zero_or_starts_with_1([]) ->
    true;
row_is_all_zero_or_starts_with_1([{q,1,1} | _]) ->
    true;
row_is_all_zero_or_starts_with_1([{q,0,1} | Rest]) ->
    row_is_all_zero_or_starts_with_1(Rest);
row_is_all_zero_or_starts_with_1(_) ->
    false.


-spec all_rows_that_are_all_zero_are_at_the_bottom([[q()]]) -> boolean().
all_rows_that_are_all_zero_are_at_the_bottom([]) ->
    true;
all_rows_that_are_all_zero_are_at_the_bottom([Row | Rest]) ->
    RowIsAllZero = row_is_all_zero(Row),
    case RowIsAllZero of
        % If row is not all zero, move on
        false ->
            all_rows_that_are_all_zero_are_at_the_bottom(Rest);
        % If row is all zero, the rest better be too
        true ->
            lists:foldl(
                fun(RestRow, Acc) ->
                    Acc andalso row_is_all_zero(RestRow)
                end,
                true,
                Rest
            )
    end.


-spec row_is_all_zero([q(), ...]) -> boolean().
% FIXME should not be pattern matching on {q,0,1} and {q,1,1}
row_is_all_zero([{q,0,1}])        -> true;
row_is_all_zero([{q,0,1} | Rest]) -> row_is_all_zero(Rest);
row_is_all_zero(_)                -> false.


% FIXME better typespec needed
% FIXME should crash on out of bounds
-spec row_swap_matrix(NR :: pos_integer(), RowIdx1 :: idx1(), OtherRowIdx1 :: idx1()) -> qm().
row_swap_matrix(NR, R1, R2) ->
    IDNR = idn(NR),
    % {R1,R1}th entry should be zeroed
    M1 = ijput(IDNR, {R1, R1}, qq_q:q0()),
    % {R2,R2}th entry should be zeroed
    M2 = ijput(M1, {R2, R2}, qq_q:q0()),
    % {R1,R2}th entry should be oned
    M3 = ijput(M2, {R1, R2}, qq_q:q1()),
    % {R2,R1}th entry should be oned
    M4 = ijput(M3, {R2, R1}, qq_q:q1()),
    M4.


% FIXME better typespec needed
% FIXME should crash on out of bounds
-spec row_times_matrix(NR :: pos_integer(), RowIdx1 :: idx1(), Factor :: q()) -> qm().
row_times_matrix(NR, RowIdx1, Factor) ->
    IDNR = idn(NR),
    ijput(IDNR, {RowIdx1, RowIdx1}, Factor).


% FIXME better typespec needed
% FIXME should crash on out of bounds
-spec row_subtract_matrix(NR :: pos_integer(), Attacker :: idx1(), Victim :: idx1()) -> qm().
row_subtract_matrix(NR, Attacker, Victim) ->
    row_subtract_factor_matrix(NR, Attacker, Victim, qq_q:q1()).


% FIXME better typespec needed
% FIXME should crash on out of bounds
-spec row_subtract_factor_matrix(NR :: pos_integer(), Attacker :: idx1(), Victim :: idx1(), Factor :: q()) -> qm().
row_subtract_factor_matrix(NR, Attacker, Victim, Factor) ->
    % Attacker corresponds to the column, victim corresponds to row;
    %
    % makes sense, because the row in the operator matrix determines what the
    % row in the result matrix will be
    %
    % Anyway, put -1*factor in {Victim, Attacker} position
    MinusFactor = qq_q:qtimes(qq_q:z2q(-1), Factor),
    IDNR = idn(NR),
    ijput(IDNR, {Victim, Attacker}, MinusFactor).


%-----------------------------------------------------------------------------%
% GAUSSIAN ELIMINATION II
%-----------------------------------------------------------------------------%

% we will rewrite the is_rref function to use the rref_step instead
%
%    all_rows_are_all_zero_or_start_with_1(Rows)
%        andalso all_rows_that_are_all_zero_are_at_the_bottom(Rows)
%        andalso the_leading_1_of_any_row_that_is_not_all_zeros_is_strictly_to_the_right_of_the_leading_1_above_it(Rows)
%        andalso all_columns_with_leading_1_are_otherwise_0s(Rows).

-type gstep()
        :: is_rref

         % all_rows_are_all_zero_or_start_with_1(Rows)
         | {leader_is_not_1,
                {the_idx1_of_the_not_all_zero_row_is, idx1()},
                {the_row_starts_with, q()},
                {so_therefore_to_make_it_1_multiply_the_row_by, q()}}

         % all_rows_that_are_all_zero_are_at_the_bottom(Rows)
         | {zero_row_before_nonzero_row,
                {the_idx1_of_the_all_zero_row_is, idx1()},
                {the_idx1_of_the_not_all_zero_row_is, idx1()},
                so_therefore_swap_rows}

         % the_leading_1_of_any_row_that_is_not_all_zeros_is_strictly_to_the_right_of_the_leading_1_above_it(Rows)
         | {leading_1_left_of_prior_leading_1,
                {higher_row_idx1_is, idx1()},
                {lower_row_idx1_is, idx1()},
                so_therefore_swap_rows}
         | {leading_1_below_prior_leading_1,
                {higher_row_idx1_is, idx1()},
                {lower_row_idx1_is, idx1()},
                so_therefore_subtract_the_higher_row_from_the_lower_row}

         % all_columns_with_leading_1_are_otherwise_0s(Rows).
         %
         % We've eliminated the case of there being a 1 BELOW this pivot, so
         % therefore the leading 1 must be ABOVE this pivot
         | {eliminate_above,
                {lower_row, idx1()},
                {higher_row, idx1()},
                {eliminate_entry, q()},
                {so_therefore_subtract_rows,
                    {take_row_with_idx_1, idx1()},
                    {add_to_it_a_multiple_of_row_with_idx1, idx1()},
                    that_multiple_is_minus_the_entry_that_needs_to_be_eliminated,
                    {that_multiple_is, q()}}}

         % fuck me
         | {pc_load_letter, term()}
         .


-spec rcef(qm()) -> qm().
rcef(QM = #qm{}) ->
    QMT = transpose(QM),
    RCEFT = rref(QMT),
    RCEF = transpose(RCEFT),
    RCEF.


-spec rref(qm()) -> qm().
rref(QM = #qm{shape={NR,_}}) ->
    case gstep(QM) of
        is_rref ->
            QM;
        X = {pc_load_letter, _} ->
            erlang:error(X);
        GStep ->
            M = gstep2qm(NR, GStep),
            NewQM = qmtimes(M, QM),
            rref(NewQM)
    end.

-spec gstep_debug(qm(), NumSteps :: non_neg_integer()) -> ok.
% Print out the steps of Gaussian elimination
gstep_debug(QM, 0) ->
    % Header
    ok = dashline(),
    ok = io:format("Last step~n"),
    ok = dashline(),

    % QM
    ok = io:format("QM:~n"),
    ok = qq:pp(QM),

    % Done
    ok;
gstep_debug(QM = #qm{shape={NR,_}}, N) when N > 0 ->
    % Header
    ok = dashline(),
    ok = io:format("~p steps remaining",
                   [N]),
    ok = dashline(),

    % QM
    ok = io:format("QM:~n"),
    ok = qq:pp(QM),

    % GStep
    GStep = gstep(QM),
    ok    = io:format("~n"
                      "GStep:~n"
                      "~p~n",
                      [GStep]),

    % Row operation matrix
    StepM = gstep2qm(NR, GStep),
    ok    = io:format("~n"
                      "StepM:~n"),
    ok    = qq:pp(StepM),

    % New operation matrix
    ok    = io:format("~n"
                      "StepM * QM:~n"),
    NewQM = qmtimes(StepM, QM),
    ok    = qq:pp(NewQM),

    % footer
    ok = io:format("~n"),

    % next step
    gstep_debug(NewQM, N - 1).


-spec dashline() -> ok.
dashline() ->
    Dashes = [$- || _ <- lists:seq(1,80)],
    ok = io:format("~n" ++ Dashes ++  "~n"),
    ok.


% I believe this wins worst Erlang code ever written
-spec gstep(qm()) -> gstep().
% @doc Given a matrix, figure out the next step in Gaussian elimination
gstep(QM) ->
    case is_rref(QM) of
        true ->
            is_rref;
        false ->
            case leader_is_not_1(QM) of
                {true, AtomWaffle} -> AtomWaffle;
                false ->
                    case zero_row_before_nonzero_row(QM) of
                        {true, AtomWaffle} -> AtomWaffle;
                        false ->
                            case leading_1_left_of_prior_leading_1(QM) of
                                {true, AtomWaffle} -> AtomWaffle;
                                false ->
                                    case leading_1_below_prior_leading_1(QM) of
                                        {true, AtomWaffle} -> AtomWaffle;
                                        false ->
                                            case eliminate_above(QM) of
                                                {true, AtomWaffle} -> AtomWaffle;
                                                false ->
                                                    pc_load_letter(QM)
                                            end
                                    end
                            end
                    end
            end
    end.



-spec gstep2qm(NR :: pos_integer(), gstep()) -> qm().
% @doc row operation matrix corresponding to a step
gstep2qm(NR, {leader_is_not_1,
              {the_idx1_of_the_not_all_zero_row_is, RowIdx1},
              {the_row_starts_with, _DontCare},
              {so_therefore_to_make_it_1_multiply_the_row_by, Factor}}) ->
    row_times_matrix(NR, RowIdx1, Factor);
gstep2qm(NR, {zero_row_before_nonzero_row,
              {the_idx1_of_the_all_zero_row_is, A},
              {the_idx1_of_the_not_all_zero_row_is, B},
              so_therefore_swap_rows}) ->
    row_swap_matrix(NR, A, B);
gstep2qm(NR, {leading_1_left_of_prior_leading_1,
              {higher_row_idx1_is, A},
              {lower_row_idx1_is, B},
              so_therefore_swap_rows}) ->
    row_swap_matrix(NR, A, B);
gstep2qm(NR, {leading_1_below_prior_leading_1,
              {higher_row_idx1_is, Attacker},
              {lower_row_idx1_is, Victim},
              so_therefore_subtract_the_higher_row_from_the_lower_row}) ->
    row_subtract_matrix(NR, Attacker, Victim);
gstep2qm(NR, {eliminate_above,
              {lower_row, _},
              {higher_row, _},
              {eliminate_entry, _},
              {so_therefore_subtract_rows,
               {take_row_with_idx1, Victim},
               {add_to_it_a_multiple_of_row_with_idx1, Attacker},
               that_multiple_is_minus_the_entry_that_needs_to_be_eliminated,
               {that_multiple_is, Factor}}}) ->
    % subtracting minus factor
    MinusFactor = qq_q:qtimes(qq_q:z2q(-1), Factor),
    row_subtract_factor_matrix(NR, Attacker, Victim, MinusFactor);
gstep2qm(QM, AtomWaffle) ->
    erlang:error({pc_load_letter, [gstep2qm, QM, AtomWaffle]}).



-spec eliminate_above(QM)
            -> false
             | {true, gstep()}
     when QM :: qm().
eliminate_above(QM) ->
    Rows = rows(QM),
    Cols = cols(QM),
    leading_1_below_nonzero(Rows, Cols, 1).


-spec leading_1_below_nonzero(Rows, Cols, RowIdx1) -> MaybeGStep
    when Rows         :: [[q()]],
         Cols         :: [[q()]],
         RowIdx1      :: idx1(),
         MaybeGStep   :: false | {true, gstep()}.
% End of the rows, no counterexample, get outta here
leading_1_below_nonzero([], _, _) ->
    false;
% First row, move along
leading_1_below_nonzero([_ | RemainingRows], Cols, 1) ->
    leading_1_below_nonzero(RemainingRows, Cols, 2);
% Figure out where the leading 1 is
% Grab that column
% Make sure the first (1.. [ThisRowIdx1-1]) entries of that column are all zero
% If not, we'll return back the atomwaffle corresponding to eliminating the
% first one.
leading_1_below_nonzero([ThisRow | RemainingRows], Cols, ThisRowIdx1) ->
    Leading0s =
        lists:takewhile(
                fun(Item) ->
                    ItemIs0 = Item =:= qq_q:q0(),
                    ItemIs0
                end,
                ThisRow
            ),
    NumLeading0s = erlang:length(Leading0s),
    % If the row is all zero, then we can short-circuit exit, because all
    % subsequent rows will also be all zero
    RowIsAll0 = NumLeading0s =:= erlang:length(ThisRow),
    case RowIsAll0 of
        true ->
            false;
        false ->
            % Grab the column of the leading 1
            Leading1ColIdx1 = NumLeading0s + 1,
            ThisCol = lists:nth(Leading1ColIdx1, Cols),
            % All of the entries in this column above this row must be 0
            EntriesToCheck = [lists:nth(N, ThisCol)
                              || N <- lists:seq(1, ThisRowIdx1-1)],
            AllAboveEntriesAre0 =
                lists:all(
                        fun(Item) ->
                            Item =:= qq_q:q0()
                        end,
                        EntriesToCheck
                    ),
            case AllAboveEntriesAre0 of
                % If all above entries are 0, then move on to the next row
                true ->
                    NextRowIdx1 = ThisRowIdx1 + 1,
                    leading_1_below_nonzero(RemainingRows, Cols, NextRowIdx1);
                % If not, we have to find out
                %
                %   - the RowIdx1 of the higher entry
                %   - what the entry is
                %
                false ->
                    NumLeading0sInThisCol =
                        erlang:length(
                            lists:takewhile(
                                fun(Item) ->
                                    Item =:= qq_q:q0()
                                end,
                                ThisCol
                            )
                        ),
                    HigherRowIdx1 = NumLeading0sInThisCol + 1,
                    OffendingEntry = lists:nth(HigherRowIdx1, ThisCol),
                    {true,
                     {eliminate_above,
                      {lower_row, ThisRowIdx1},
                      {higher_row, HigherRowIdx1},
                      {eliminate_entry, OffendingEntry},
                      {so_therefore_subtract_rows,
                       {take_row_with_idx1, HigherRowIdx1},
                       {add_to_it_a_multiple_of_row_with_idx1, ThisRowIdx1},
                       that_multiple_is_minus_the_entry_that_needs_to_be_eliminated,
                       {that_multiple_is, qq_q:qtimes(qq_q:z2q(-1), OffendingEntry)}}}}
            end
    end.


-spec leading_1_below_prior_leading_1(qm()) -> false | {true, gstep()}.
leading_1_below_prior_leading_1(QM) ->
    Rows = rows(QM),
    leading_1_below_prior_leading_1(Rows, 1, first_row).


-spec leading_1_below_prior_leading_1(Rows, ThisRowIdx1, PriorLeading1ColIdx1) -> false | {true, gstep()}
    when
        Rows :: [[qm()]],
        ThisRowIdx1 :: idx1(),
        PriorLeading1ColIdx1 :: first_row | pos_integer().
% If we get to the end of the matrix and haven't found a counterexample, we're
% cool
leading_1_below_prior_leading_1([], _, _) ->
    false;
leading_1_below_prior_leading_1([ThisRow | RemainingRows], ThisRowIdx1, PriorLeading1ColIdx1) ->
    % Check if the leading 1 of this row is strictly to the left of the leading
    % 1 above it
    %
    % Eliminated the following cases in prior branches:
    %
    %   - this row doesn't start with 0 or 1
    %   - the things before the leading 1 are not 0
    %   - the prior rows are all zero
   ThisRowLeading0s =
        lists:takewhile(
            fun(Item) ->
                ItemIs0 = Item =:= qq_q:q0(),
                ItemIs0
            end,
            ThisRow
        ),
    NumLeading0s = erlang:length(ThisRowLeading0s),
    RowIsAllZero = NumLeading0s =:= erlang:length(ThisRow),
    case RowIsAllZero of
        % If row is all zero, can short circuit, because we've eliminated
        % zero rows being followed by nonzero rows
        true -> false;
        false ->
            Leading1ColIdx1 = NumLeading0s + 1,
            % Determine if the leading 1 of this row is directly below the
            % prior leading 1.
            Leading1LeftOfPriorLeading1 =
                case PriorLeading1ColIdx1 of
                    % We're on the first row so the condition is false because
                    % there are no prior leading 1s
                    first_row -> false;
                    % If we're not on the first row, check
                    _         -> Leading1ColIdx1 =:= PriorLeading1ColIdx1
                end,
            case Leading1LeftOfPriorLeading1 of
                % False, move on to next row
                false ->
                    NextRowIdx1 = ThisRowIdx1 + 1,
                    leading_1_below_prior_leading_1(
                            RemainingRows,
                            NextRowIdx1,
                            Leading1ColIdx1
                        );
                true ->
                    PreviousRowIdx1 = ThisRowIdx1 - 1,
                    {true,
                     {leading_1_below_prior_leading_1,
                      {higher_row_idx1_is, PreviousRowIdx1},
                      {lower_row_idx1_is, ThisRowIdx1},
                      so_therefore_subtract_the_higher_row_from_the_lower_row}}
            end
    end.


-spec leading_1_left_of_prior_leading_1(qm()) -> false | {true, gstep()}.
leading_1_left_of_prior_leading_1(QM) ->
    Rows = rows(QM),
    leading_1_left_of_prior_leading_1(Rows, 1, first_row).


-spec leading_1_left_of_prior_leading_1(Rows, ThisRowIdx1, PriorLeading1ColIdx1) -> false | {true, gstep()}
    when
        Rows :: [[qm()]],
        ThisRowIdx1 :: idx1(),
        PriorLeading1ColIdx1 :: first_row | pos_integer().
% If we get to the end of the matrix and haven't found a counterexample, we're
% cool
leading_1_left_of_prior_leading_1([], _, _) ->
    false;
leading_1_left_of_prior_leading_1([ThisRow | RemainingRows], ThisRowIdx1, PriorLeading1ColIdx1) ->
    % Check if the leading 1 of this row is strictly to the left of the leading
    % 1 above it
    %
    % Eliminated the following cases in prior branches:
    %
    %   - this row doesn't start with 0 or 1
    %   - the things before the leading 1 are not 0
    %   - the prior rows are all zero
   ThisRowLeading0s =
        lists:takewhile(
            fun(Item) ->
                ItemIs0 = Item =:= qq_q:q0(),
                ItemIs0
            end,
            ThisRow
        ),
    NumLeading0s = erlang:length(ThisRowLeading0s),
    RowIsAllZero = NumLeading0s =:= erlang:length(ThisRow),
    case RowIsAllZero of
        % If row is all zero, can short circuit, because we've eliminated
        % zero rows being followed by nonzero rows
        true -> false;
        false ->
            Leading1ColIdx1 = NumLeading0s + 1,
            % Determine if the leading 1 of this row is left of the prior
            % leading 1.
            Leading1LeftOfPriorLeading1 =
                case PriorLeading1ColIdx1 of
                    % We're on the first row so the condition is false because
                    % there are no prior leading 1s
                    first_row -> false;
                    % If we're not on the first row, check
                    _         -> Leading1ColIdx1 < PriorLeading1ColIdx1
                end,
            case Leading1LeftOfPriorLeading1 of
                % False, move on to next row
                false ->
                    NextRowIdx1 = ThisRowIdx1 + 1,
                    leading_1_left_of_prior_leading_1(
                            RemainingRows,
                            NextRowIdx1,
                            Leading1ColIdx1
                        );
                true ->
                    PreviousRowIdx1 = ThisRowIdx1 - 1,
                    {true,
                     {leading_1_left_of_prior_leading_1,
                      {higher_row_idx1_is, PreviousRowIdx1},
                      {lower_row_idx1_is, ThisRowIdx1},
                      so_therefore_swap_rows}}
            end
    end.


-spec zero_row_before_nonzero_row(qm()) -> false | {true, gstep()}.
% @doc
% Detects
%
%   0 0 0
%   0 3 0
%   0 0 1
zero_row_before_nonzero_row(QM) ->
    Rows = rows(QM),
    zero_row_before_nonzero_row(Rows, 1).


-spec zero_row_before_nonzero_row([[q()]], idx1()) -> false | {true, gstep()}.
% Reached the end of the list, all good, never a row that was all zero
zero_row_before_nonzero_row([], _RowIdx1) ->
    false;
zero_row_before_nonzero_row([ThisRow | Rows], ThisRowIdx1) ->
    ThisRowIsAllZero = row_is_all_zero(ThisRow),
    NextRowIdx1 = ThisRowIdx1 + 1,
    case ThisRowIsAllZero of
        % this row is not all zero, move along
        false ->
            zero_row_before_nonzero_row(Rows, NextRowIdx1);
        % this row is all zero, fold over the rest
        true ->
            IsThereARowAfterThisOneThatIsntAllZero = rest_of_rows_better_fucking_be_zero_or_else(Rows, NextRowIdx1),
            case IsThereARowAfterThisOneThatIsntAllZero of
                % this row is all zero, but so are all the remaining rows. We're cool, let's get out of here
                false ->
                    false;
                % sheeeit
                % not only is this row all zero. But a row after this has the fucking gall not to be. better swap them before
                % anyone notices
                {true, TheIdx1OfTheRowThatWereGonnaSwapThisRowWith} ->
                    {true,
                     {zero_row_before_nonzero_row,
                         {the_idx1_of_the_all_zero_row_is, ThisRowIdx1},
                         {the_idx1_of_the_not_all_zero_row_is, TheIdx1OfTheRowThatWereGonnaSwapThisRowWith},
                         so_therefore_swap_rows}}
            end
    end.


-spec rest_of_rows_better_fucking_be_zero_or_else([[q()]], idx1()) -> false | {true, idx1()}.
% Reached the end of the list, all good, there was a row that was zero, but it was at the bottom. so we're cool
rest_of_rows_better_fucking_be_zero_or_else([], _) ->
    false;
% gotta check if this row is all zero and all the others too
rest_of_rows_better_fucking_be_zero_or_else([ThisRow | Rows], ThisRowIdx1) ->
    ThisRowIsAllZero = row_is_all_zero(ThisRow),
    case ThisRowIsAllZero of
        % we're cool, move along
        true ->
            NextRowIdx1 = ThisRowIdx1 + 1,
            rest_of_rows_better_fucking_be_zero_or_else(Rows, NextRowIdx1);
        % oh shit
        % this function doesn't get called until a zero row has been encountered
        % and this row isn't zero
        % oh no
        % better report this
        false ->
            {true, ThisRowIdx1}
        % alright brain is fried need some sleep
    end.


-spec leader_is_not_1(qm()) -> false | {true, gstep()}.
% @doc
% Detects
%
%   1 0 0
%   0 3 0
%   0 0 1
leader_is_not_1(QM) ->
    Rows = rows(QM),
    ReturnFalse = all_rows_are_all_zero_or_start_with_1(Rows),
    case ReturnFalse of
        true ->
            false;
        false ->
            % Find the idx1 of the row that is not all zero but does not start with 1
            {{the_idx1_is, RowIdx1},
             {the_row_starts_with, Q}} = idx1_of_row_that_is_not_all_zero_but_does_not_start_with_1(Rows),
            QInv = qq_q:qinv(Q),
            {true,
                {leader_is_not_1,
                    {the_idx1_of_the_not_all_zero_row_is, RowIdx1},
                    {the_row_starts_with, Q},
                    {so_therefore_to_make_it_1_multiply_the_row_by, QInv}}}
    end.


-spec idx1_of_row_that_is_not_all_zero_but_does_not_start_with_1([[q()]]) -> {{the_idx1_is, idx1()}, {the_row_starts_with, q()}}.
idx1_of_row_that_is_not_all_zero_but_does_not_start_with_1(Rows) ->
    idx1_of_row_that_is_not_all_zero_but_does_not_start_with_1(Rows, 1).


% but with parity 2
-spec idx1_of_row_that_is_not_all_zero_but_does_not_start_with_1([[q()]], idx1()) -> {{the_idx1_is, idx1()}, {the_row_starts_with, q()}}.
idx1_of_row_that_is_not_all_zero_but_does_not_start_with_1([], I1) ->
    % bad input
    erlang:error({idx1_of_row_that_is_not_all_zero_but_does_not_start_with_1, [], I1});
idx1_of_row_that_is_not_all_zero_but_does_not_start_with_1([Qs | Qss], RowIdx1) ->
    ThisRowIsAllZero = row_is_all_zero(Qs),
    case ThisRowIsAllZero of
        true ->
            ok =
                io:format(
                    "RowIdx1     = ~p~n"
                    "RowIdx1 + 1 = ~p~n",
                    [RowIdx1, RowIdx1 + 1]
                ),
            idx1_of_row_that_is_not_all_zero_but_does_not_start_with_1(Qss, RowIdx1 + 1);
        false ->
            [NonZeroEntry|_] =
                    lists:dropwhile(
                        fun(Elem) ->
                            Elem =:= qq_q:q0()
                        end,
                        Qs
                    ),
            % Have to check if the nonzero entry is 1 or not
            case NonZeroEntry =:= qq_q:q1() of
                false ->
                    {{the_idx1_is, RowIdx1},
                     {the_row_starts_with, NonZeroEntry}};
                true ->
                    idx1_of_row_that_is_not_all_zero_but_does_not_start_with_1(Qss, RowIdx1 + 1)
            end
    end.


-spec pc_load_letter(term()) -> gstep().
pc_load_letter(QM) ->
    {pc_load_letter, QM}.


%-----------------------------------------------------------------------------%
% DETERMINANTS AND INVERSES
%-----------------------------------------------------------------------------%

-spec det(qm()) -> q().
% @doc Determinant of matrix. Crashes if matrix not square
% @end
det(QM = #qm{shape={NR, NC}}) when NR =/= NC ->
    shape_error("non square matrix: ~p", [QM]);
% Determinant of 1x1 matrix is its entry
det(QM = #qm{shape={1,1}}) ->
    ijth(QM, {1,1});
% det of a nxn matrix = expansion by minors
det(QM = #qm{shape={N, N}}) ->
    TopRowIJPairs = [{1, I} || I <- lists:seq(1,N)],
    Minors = [ijth_minor(IJ, QM) || IJ <- TopRowIJPairs],
    TopRow = nthrow(1, QM),
    AltSignsTopRow = alt_signs(TopRow),
    MinorDeterminants = [det(Minor) || Minor <- Minors],
    dot(row(AltSignsTopRow), col(MinorDeterminants)).


-spec alt_signs([q()]) -> [q()].
alt_signs(Qs) -> alt_signs2(Qs, plus).


-spec alt_signs2([q()], plus|minus) -> [q()].
alt_signs2([], _) ->
    [];
alt_signs2([Q | Qs], plus) ->
    [Q | alt_signs2(Qs, minus)];
alt_signs2([Q | Qs], minus) ->
    [qq_q:ztimesq(-1, Q) | alt_signs2(Qs, plus)].


-spec ijth_minor({idx1(), idx1()}, qm()) -> qm().
% @doc does not assing the sign to minor. No bounds checks
% @end
%
% FIXME put a shape error on out of bounds
%
% 1xN or Nx1 matrices have no minors
ijth_minor(_, QM = #qm{shape={1, _}}) ->
    shape_error("Row matrices do not have minors: ~p", [QM]);
ijth_minor(_, QM = #qm{shape={_, 1}}) ->
    shape_error("Column matrices do not have minors: ~p", [QM]);
ijth_minor({R, C}, QM = #qm{shape={NR, NC}}) ->
    IJs = [{I, J} || I <- lists:seq(1, NR),
                     J <- lists:seq(1, NC),
                     I =/= R,
                     J =/= C],
    NewQMEntryList = [ijth(QM, IJ) || IJ <- IJs],
    NewQM1 = row(NewQMEntryList),
    NewQM2 = reshape(NewQM1, {NR-1, NC-1}),
    NewQM2.


-spec is_invertible(qm()) -> true | false.
% @doc checks if determinant =/= 0
is_invertible(QM) ->
    det(QM) =/= qq_q:q0().


-spec inverse_left(qm()) -> qm().
% @doc inverse_left is the product of the row-operation matrices on QM. It has
% the property that
%
%       qmtimes(inverse_left(QM), QM) = rref(QM)
%
% If the matrix is invertible, then inverse_left(QM) = inverse_right(QM)
inverse_left(QM = #qm{shape={NR, _}}) ->
    case gstep(QM) of
        % If the matrix is in rref, its inverse is the identity
        is_rref ->
            idn(NR);
        X = {pc_load_letter, _} ->
            erlang:error(X);
        GStep ->
            % M = matrix corresponding to gaussian elimination step
            M = gstep2qm(NR, GStep),
            NewQM = qmtimes(M, QM),
            % Recursively multiply on the left
            qmtimes(inverse_left(NewQM), M)
    end.


-spec inverse_right(qm()) -> qm().
% @doc inverse_right is the product of the row-operation matrices on QM. It has
% the properties that
%
%       qmtimes(QM, inverse_right(QM)) = rcef(QM)
%
% If the matrix is invertible, then inverse_right(QM) = inverse_right(QM)
%
% Additionally, this matrix has the property that
%
%       inverse_right(QM) = transpose(inverse_left(transpose(QM)))
inverse_right(QM) ->
    transpose(inverse_left(transpose(QM))).

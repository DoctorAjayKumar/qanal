% @doc
% QAnal: Rational Analysis
%
%       Every operation, calculation, and concept, no matter how arbitrarily
%       complex, reduces to adding integers together. There are no new concepts
%       in QAnal. Everything is just putting lipstick on adding integers
%       together.
%
%   -- Dr. Ajay Kumar PHD, Founder of QAnal
%
% qq: main API module
%
% HOW TO USE THIS LIBRARY
%
% Most of the functions you will use are exported from this module. The
% functions in this module are intentionally polymorphic to make it easy to
% write quick programs without worrying about stupidity with types. If you need
% better Dialyzer guarantees, use one of the functions in the submodule.
%
% As a general rule, the function you need will be located in the module
% corresponding to highest-order type operand; e.g. if you are multiplying an
% integer by a rational matrix, look in the module for rational matrices.
%
% @end

% TODO: add instances for all the complex arithmetic. I need a break

-module(qq).
-vsn("0.1.0").
-author("Dr. Ajay Kumar PHD").
-copyright("Dr. Ajay Kumar PHD").
-license("MIT").


% types
-export_type([pn/0,
              z/0,
              q/0,
              qm/0]).

% constructors/operators
-export([q0/0,
         q1/0,
         c0/0,
         c1/0,
         inv/1,
         minus/2,
         plus/1,
         plus/2,
         pow/2,
         %rref_step/1,
         q/2,
         transpose/1,
         x/2,
         x/1]).

% is_*
-export([is_pn/1,
         is_z/1,
         is_q/1,
         is_qm/1,
         is_c/1,
         is_rcef/1,
         is_rref/1]).

% string parsing
-export([c/2,
         qq/1,
         q/1,
         qm/1]).

% pretty printing
-export([n2s/1,
         pp/1]).

-type pn() :: qq_z:pn().
-type z()  :: qq_z:z().
-type q()  :: qq_q:q().
-type c()  :: qq_c:c().

-type qm() :: qq_qm:qm().

% comm_num = commutative number
-type comm_num() :: pn()
                  | z()
                  | q()
                  | c().

-type num()     :: comm_num()
                 | qm().


%-----------------------------------------------------------------------------%
% CONSTRUCTORS/OPERATORS
%-----------------------------------------------------------------------------%

-spec q0() -> q().
q0() -> qq_q:q0().


-spec q1() -> q().
q1() -> qq_q:q1().


-spec c0() -> c().
c0() -> qq_c:c0().


-spec c1() -> c().
c1() -> qq_c:c1().


-spec inv(comm_num()) -> comm_num().
% @doc inv(X) -> q(1, X).
%
% Should in the future invert matrices, does not atm because I haven't
% implemented matrix multiplication
%
% Does not handle complex numbers yet
% @end
inv(X) -> q(1, X).


-spec minus(num(), num()) -> num().
% @doc
% Subtracts two things. No corresponding list operator since subtraction is not
% associative.
%
% minus(X, Y) -> plus(X, x(-1, Y)).
% @end
minus(X, Y) -> plus(X, x(-1, Y)).


-spec plus(num(), num()) -> num().
% @doc
% Adds two things that are kind of numbers. Will crash if they aren't
% compatible.
%
% If you want better dialyzer guarantees, use the specific functions exported
% @end
plus(X, Y) ->
    CPlus  = is_c  (X) andalso is_c  (Y),
    CPlusQ = is_c  (X) andalso is_q  (Y),
    CPlusZ = is_c  (X) andalso is_z  (Y),

    QPlus  = is_q  (X) andalso is_q  (Y),
    QPlusC = is_q  (X) andalso is_c  (Y),
    QPlusZ = is_q  (X) andalso is_z  (Y),

    QMPlus = is_qm (X) andalso is_qm (Y),

    ZPlus  = is_z  (X) andalso is_z  (Y),
    ZPlusC = is_z  (X) andalso is_c  (Y),
    ZPlusQ = is_z  (X) andalso is_q  (Y),

    % Should be rewritten as to dialyze better
    if
        CPlus  -> qq_c  : cplus  (X,Y);
        CPlusQ -> qq_c  : qplusc (Y,X);
        CPlusZ -> qq_c  : zplusc (Y,X);

        QPlus  -> qq_q  : qplus  (X,Y);
        QPlusC -> qq_c  : qplusc (X,Y);
        QPlusZ -> qq_q  : zplusq (Y,X);

        QMPlus -> qq_qm : qmplus (X,Y);

        ZPlus  -> X+Y;
        ZPlusC -> qq_c  : zplusc (X,Y);
        ZPlusQ -> qq_q  : zplusq (X,Y);

        true   -> erlang:error({type_error, plus, X, Y})
    end.


-spec plus([num(), ...]) -> num().
plus([N])        -> N;
plus([N | Rest]) -> plus(N, plus(Rest)).


-spec pow(num(), z()) -> num().
% FIXME should not use erlang:is_integer/1, instead should factor this into a
% case statement, and have an internal function for validated input
pow(P, N) when not erlang:is_integer(N) ->
    erlang:error({type_error, non_integer_pow, {pow, P, N}});
% 0^0 is nonsense
pow(0, 0) ->
    erlang:error("0^0 is nonsense, fuck off");
% FIXME: returns integer regardless of input type
pow(_, 0) ->
    1;
pow(X, N) when N < 0 ->
    inv(pow(X, x(-1, N)));
pow(X, N) when N > 0 ->
    x(X, pow(X, N-1)).


%-spec rref_step(qm()) -> qq_qm:gstep().
%rref_step(QM) -> qq_qm:rref_step(QM).

-spec q(comm_num(), comm_num()) -> comm_num().
% @doc General quotient function
%
% Matrices are NOT included in the typespec because of the non-commutativity of
% matrix multiplication
%
% Namely, for general matrices A, B, it's not clear whether user wants AB^{-1}
% or B^{-1}A
%
% FIXME Functions in this module should not be doing any typecasting or weird
% argument switching; it should call already-existing functions from the
% submodules for doing the arithmetic operations
q(Left, Right) ->
    CqC = is_c(Left) andalso is_c(Right),
    CqQ = is_c(Left) andalso is_q(Right),
    CqZ = is_c(Left) andalso is_z(Right),

    QqC = is_q(Left) andalso is_c(Right),
    QqQ = is_q(Left) andalso is_q(Right),
    QqZ = is_q(Left) andalso is_z(Right),

    ZqC = is_z(Left) andalso is_c(Right),
    ZqQ = is_z(Left) andalso is_q(Right),
    ZqZ = is_z(Left) andalso is_z(Right),

    if
        CqC -> qq_c : cdiv  (Left, Right);
        CqQ -> qq_c : cdivq (Left, Right);
        CqZ -> qq_c : cdivz (Left, Right);

        QqC -> qq_c : cdiv (qq_c:q2c(Left), Right);
        QqQ -> qq_q : qdiv (Left, Right);
        QqZ -> qq_q : qdiv (Left, qq_q:z2q(Right));

        ZqC -> qq_c : cdiv (qq_c:z2c(Left), Right);
        ZqQ -> qq_q : qdiv (qq_q:z2q(Left), Right);
        ZqZ -> qq_q : q    (Left          , Right);

        true ->
            erlang:error({type_error, {qq, q, [Left, Right]}})
    end.


-spec transpose(qm()) -> qm().
% @doc alias for qq_qm:transpose/1
transpose(QM) -> qq_qm:transpose(QM).


% TODO: write out overloaded typespecs (maybe once Dialyzer supports them)
%
%-spec x
%    (pn(), pn()) -> pn();
%    (z(), z())   -> z();
%    (q(), q())   -> q().
%    (q(), q())   -> q().

-spec x(num(), num()) -> num().
% @doc
% Multiplies two things that are kind of numbers. Will crash if they aren't
% compatible.
%
% If you want better dialyzer guarantees, use the specific multiplication
% functions exported from the submodules; e.g. to multiply a integer times a
% rational matrix, use qq_qm:ztimesqm/2
% @end
x(Left, Right) ->
    CTimes      = is_c  (Left) andalso is_c  (Right),
    CTimesQ     = is_c  (Left) andalso is_q  (Right),
    CTimesZ     = is_c  (Left) andalso is_z  (Right),

    QTimes      = is_q  (Left) andalso is_q  (Right),
    QTimesC     = is_q  (Left) andalso is_c  (Right),
    QTimesZ     = is_q  (Left) andalso is_z  (Right),
    QTimesQM    = is_q  (Left) andalso is_qm (Right),

    QMTimes     = is_qm (Left) andalso is_qm (Right),
    QMTimesZ    = is_qm (Left) andalso is_z  (Right),
    QMTimesQ    = is_qm (Left) andalso is_q  (Right),

    ZTimes      = is_z  (Left) andalso is_z  (Right),
    ZTimesC     = is_z  (Left) andalso is_c  (Right),
    ZTimesQ     = is_z  (Left) andalso is_q  (Right),
    ZTimesQM    = is_z  (Left) andalso is_qm (Right),

    % Should be rewritten as to dialyze better
    if

        CTimes      -> qq_c  : ctimes   (Left  , Right);
        CTimesQ     -> qq_c  : qtimesc  (Left  , Right);
        CTimesZ     -> qq_c  : ztimesc  (Right , Left);

        QTimes      -> qq_q  : qtimes   (Left  , Right);
        QTimesC     -> qq_c  : qtimesc  (Left  , Right);
        QTimesZ     -> qq_q  : ztimesq  (Right , Left);
        QTimesQM    -> qq_qm : qtimesqm (Left  , Right);

        QMTimes     -> qq_qm : qmtimes  (Left  , Right);
        QMTimesZ    -> qq_qm : ztimesqm (Right , Left);
        QMTimesQ    -> qq_qm : qtimesqm (Right , Left);

        ZTimes      -> Left*Right;
        ZTimesC     -> qq_c  : ztimesc  (Left  , Right);
        ZTimesQ     -> qq_q  : ztimesq  (Left  , Right);
        ZTimesQM    -> qq_qm : ztimesqm (Left  , Right);

        true ->
            erlang:error({type_error, {qq, times, [Left, Right]}})
    end.


-spec x([num(), ...]) -> num().
x([N])        -> N;
x([N | Rest]) -> x(N, x(Rest)).



%-----------------------------------------------------------------------------%
% is_*
%-----------------------------------------------------------------------------%

-spec is_pn(term()) -> boolean().
is_pn(Z) -> qq_z:is_pn(Z).


-spec is_z(term()) -> boolean().
is_z(Z) -> qq_z:is_z(Z).


-spec is_q(term()) -> boolean().
is_q(Z) -> qq_q:is_q(Z).


-spec is_qm(term()) -> boolean().
is_qm(Z) -> qq_qm:is_qm(Z).


-spec is_c(term()) -> boolean().
is_c(Z) -> qq_c:is_c(Z).


-spec is_rcef(qm()) -> boolean().
% @doc alias for qq_qm:is_rcef/1
is_rcef(QM) -> qq_qm:is_rcef(QM).


-spec is_rref(qm()) -> boolean().
% @doc alias for qq_qm:is_rref/1
is_rref(QM) -> qq_qm:is_rref(QM).


%-----------------------------------------------------------------------------%
% String parsers
%-----------------------------------------------------------------------------%

-spec c(string(), string()) -> num().
c(R, I) ->
    qq_c:c(q(R), q(I)).


-spec qq(string()) -> num().
% @doc NYI: should be a parser for arbitrary QAnal terms, and a left inverse to
% n2s
qq(_) ->
    erlang:error(nyi).


-spec q(string()) -> q().
% @doc alias for qq_q:s2q/1
q(S) -> qq_q:s2q(S).


-spec qm(string()) -> qq_qm:qm().
% @doc alias for qq_qm:s2qm/1
%
% Polymorphic for ease of usage. If you want better dialyzer guarantees, use
% one of the functions in the submodule
%
% You should not send this a list of numbers, as it will attempt to parse that
% as a string. If you want to convert a list of numbers (or list of list of
% numbers) to a matrix, use qq_qm:from_rows/1
% @end
%
% FIXME Should be polymorphic rather than just accepting string
qm(S) -> qq_qm:s2qm(S).


%-----------------------------------------------------------------------------%
% Pretty printing
%-----------------------------------------------------------------------------%


-spec n2s(num()) -> string().
n2s(N) ->
    IsZ  = is_z  (N),
    IsQ  = is_q  (N),
    IsQM = is_qm (N),
    if
        IsZ  -> qq_z  : z2s  (N);
        IsQ  -> qq_q  : q2s  (N);
        IsQM -> qq_qm : qm2s (N)
    end.


-spec pp(num()) -> ok.
pp(N) ->
    N2S = string:chomp(n2s(N)),
    io:format("~s~n", [N2S]).

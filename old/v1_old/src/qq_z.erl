% @doc
% QAnal: Rational Analysis
%
%       Every operation, calculation, and concept, no matter how arbitrarily
%       complex, reduces to adding integers together. There are no new concepts
%       in QAnal. Everything is just putting lipstick on adding integers
%       together.
%
%   -- Dr. Ajay Kumar PHD, Founder of QAnal
%
% qq_z: integer arithmetic
% @end
-module(qq_z).

-export_type([pn/0, z/0]).
-export([is_pn/1,
         is_z/1,
         gcd/2,
         z2s/1]).

-type pn() :: pos_integer().
-type z()  :: integer().

-spec is_pn(term()) -> boolean().
is_pn(N) ->
    is_z(N) andalso N > 0.


-spec is_z(term()) -> boolean().
is_z(Z) -> erlang:is_integer(Z).


-spec gcd(z(), z()) -> z().
% @doc
% Euclid's algorithm to compute greatest common divisor.
% @end
% Every number divides 0, and the greatest common divisor of N with itself is
% |N|
gcd(N, 0) -> abs(N);
gcd(0, N) -> abs(N);
gcd(N, K) when N < 0 ->
    gcd(-1*N, K);
gcd(N, K) when K < 0 ->
    gcd(N, -1*K);
gcd(1, _) -> 1;
gcd(_, 1) -> 1;
gcd(N, N) -> N;
% Assume N > K
gcd(N, K) when N < K ->
    gcd(K, N);
% If K divides N (ie N mod K = 0), then gcd(N, K) = K.
gcd(N, K) when N > K, N rem K == 0 ->
    K;
% Here K does not divide N
gcd(N, K) when N > K ->
    % N = Q*K + R
    % Q = N div K,
    R = N rem K,
    % Facts:
    %
    % - 1 =< R < K < N
    % - We have eliminated in a prior head the case where R == 0
    % - Any common divisor of N and K will also divide (N - Q*K) = R.
    % - Therefore, the greatest common divisor of N and K is also the greatest
    %   common divisor of K and R
    gcd(K, R).


-spec z2s(z()) -> string().
% @doc integer pretty printer
z2s(Z) -> erlang:integer_to_list(Z).

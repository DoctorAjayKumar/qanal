% @doc
% QAnal: Rational Analysis
%
%       Every operation, calculation, and concept, no matter how arbitrarily
%       complex, reduces to adding integers together. There are no new concepts
%       in QAnal. Everything is just putting lipstick on adding integers
%       together.
%
%   -- Dr. Ajay Kumar PHD, Founder of QAnal
%
%
% qq_c: complex arithmetic
% @end
-module(qq_c).

-export_type([c/0]).

% accessors
-export([is_c/1,
         re/1,
         im/1,
         conjugate/1,
         quadrance/1,
         c2qm/1]).

% constructors
-export([c/2,
         c0/0,
         c1/0,
         z2c/1,
         q2c/1]).

% arithmetic: complex
-export([cplus/2,
         cminus/2,
         ctimes/2,
         cdiv/2,
         cinv/1]).

% arithmetic: integers/complex mixed arithmetic
-export([zplusc/2,
         zminusc/2,
         cminusz/2,
         ztimesc/2,
         zdivc/2,
         cdivz/2]).

% arithmetic: quotients/complex mixed arithmetic
-export([qplusc/2,
         qminusc/2,
         cminusq/2,
         qtimesc/2,
         qdivc/2,
         cdivq/2]).

-type z() :: qq_z:z().
-type q() :: qq_q:q().
-type qm() :: qq_qm:qm().

-record(c,
        {re :: q(),
         im :: q()}).
-type c() :: #c{}.


%-----------------------------------------------------------------------------%
% ACCESSORIES
%-----------------------------------------------------------------------------%

-spec is_c(term()) -> boolean().
is_c(#c{}) -> true;
is_c(_)    -> false.


-spec re(c()) -> q().
re(#c{re=R}) -> R.


-spec im(c()) -> q().
im(#c{im=I}) -> I.


-spec conjugate(c()) -> c().
% @doc Conj(x + iy) = x - iy
conjugate(#c{re=R, im=I}) ->
    c(
        R,
        ztimesq(-1, I)
    ).


-spec quadrance(c()) -> q().
% @doc Q(x + iy) = x^2 + y^2
quadrance(#c{re=R, im=I}) ->
    qq_q:qplus(
        qq_q:qpow(R, 2),
        qq_q:qpow(I, 2)
    ).


-spec c2qm(c()) -> qm().
c2qm(#c{re=R, im=I}) ->
    qq_qm:from_rows(
        [[R, ztimesq(-1, I)],
         [I,              R]]
    ).


%-----------------------------------------------------------------------------%
% CONSTRUCTORS
%-----------------------------------------------------------------------------%

-spec c(q(), q()) -> c().
c(Re, Im) -> #c{re=Re, im=Im}.


-spec c0() -> c().
c0() -> c(q0(), q0()).


-spec c1() -> c().
c1() -> c(q1(), q0()).


-spec z2c(z()) -> c().
z2c(Z) ->
    ZQ = z2q(Z),
    c(ZQ, q0()).


-spec q2c(q()) -> c().
q2c(Q) ->
    c(Q, q0()).


%-----------------------------------------------------------------------------%
% Purely complex arithmetic
%-----------------------------------------------------------------------------%

-spec cplus(c(), c()) -> c().
cplus(#c{re=R1, im=I1},
      #c{re=R2, im=I2}) ->
    c(
        qplus(R1, R2),
        qplus(I1, I2)
    ).


-spec cminus(c(), c()) -> c().
cminus(#c{re=R1, im=I1},
       #c{re=R2, im=I2}) ->
    c(
        qminus(R1, R2),
        qminus(I1, I2)
    ).


-spec ctimes(c(), c()) -> c().
ctimes(#c{re=R1, im=I1},
       #c{re=R2, im=I2}) ->
    c(
        qminus(
            qtimes(R1, R2),
            qtimes(I1, I2)
        ),
        qplus(
            qtimes(R1, I2),
            qtimes(R2, I1)
        )
    ).


-spec cdiv(c(), c()) -> c().
% C1 / C2
%
%   - multiply top and bottom by complex conjugate of C2, to obtain
%   - C1/C2 = (C1*conjugate(C2))/quadrance(C2)
cdiv(C1, C2) ->
    Quad2    = quadrance(C2),
    InvQuad2 = qq_q:qinv(Quad2),
    C1C2Bar  = ctimes(C1, conjugate(C2)),
    qtimesc(InvQuad2, C1C2Bar).


-spec cinv(c()) -> c().
% @doc cinv(c) = 1/C
cinv(C) ->
    cdiv(c1(), C).


%-----------------------------------------------------------------------------%
% mixed integer/complex arithmetic
%-----------------------------------------------------------------------------%

-spec zplusc(z(), c()) -> c().
zplusc(Q, C) ->
    cplus(
        z2c(Q),
        C
    ).


-spec zminusc(z(), c()) -> c().
zminusc(Q, C) ->
    cminus(
        z2c(Q),
        C
    ).


-spec cminusz(c(), z()) -> c().
cminusz(C, Q) ->
    cminus(
        C,
        z2c(Q)
    ).


-spec ztimesc(z(), c()) -> c().
ztimesc(Q, C) ->
    ctimes(
        z2c(Q),
        C
    ).


-spec cdivz(c(), z()) -> c().
cdivz(C, Q) ->
    ctimes(
        C,
        cinv(z2c(Q))
    ).


-spec zdivc(z(), c()) -> c().
zdivc(Q, C) ->
    ctimes(
        z2c(Q),
        cinv(C)
    ).



%-----------------------------------------------------------------------------%
% mixed rational/complex arithmetic
%-----------------------------------------------------------------------------%

-spec qplusc(q(), c()) -> c().
qplusc(Q, C) ->
    cplus(
        q2c(Q),
        C
    ).


-spec qminusc(q(), c()) -> c().
qminusc(Q, C) ->
    cminus(
        q2c(Q),
        C
    ).


-spec cminusq(c(), q()) -> c().
cminusq(C, Q) ->
    cminus(
        C,
        q2c(Q)
    ).


-spec qtimesc(q(), c()) -> c().
qtimesc(Q, C) ->
    ctimes(
        q2c(Q),
        C
    ).


-spec cdivq(c(), q()) -> c().
cdivq(C, Q) ->
    ctimes(
        C,
        cinv(q2c(Q))
    ).


-spec qdivc(q(), c()) -> c().
qdivc(Q, C) ->
    ctimes(
        q2c(Q),
        cinv(C)
    ).

%-----------------------------------------------------------------------------%
% internal
%-----------------------------------------------------------------------------%

-spec q0() -> q().
q0() -> qq_q:q0().


-spec q1() -> q().
q1() -> qq_q:q1().


-spec qminus(q(), q()) -> q().
qminus(Q1, Q2) -> qq_q:qminus(Q1, Q2).


-spec qplus(q(), q()) -> q().
qplus(Q1, Q2) -> qq_q:qplus(Q1, Q2).


-spec qtimes(q(), q()) -> q().
qtimes(Q1, Q2) -> qq_q:qtimes(Q1, Q2).


-spec z2q(z()) -> q().
z2q(Z) -> qq_q:z2q(Z).


-spec ztimesq(z(), q()) -> q().
ztimesq(Z, Q) -> qq_q:ztimesq(Z, Q).

% QAnal: Rational Analysis
%
%       Every operation, calculation, and concept, no matter how arbitrarily
%       complex, reduces to adding integers together. There are no new concepts
%       in QAnal. Everything is just putting lipstick on adding integers
%       together.
%
%   -- Dr. Ajay Kumar PHD, Founder of QAnal
%
% eunit_qq_qm: Unit tests for qq_qm

-module(eunit_qq_qm).

-include_lib("eunit/include/eunit.hrl").

%-----------------------------------------------------------------------------%
% EUnit
%-----------------------------------------------------------------------------%

% No tests yet

%-----------------------------------------------------------------------------%
% Internal
%-----------------------------------------------------------------------------%

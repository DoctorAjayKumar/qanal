% QAnal: Rational Analysis
%
%       Every operation, calculation, and concept, no matter how arbitrarily
%       complex, reduces to adding integers together. There are no new concepts
%       in QAnal. Everything is just putting lipstick on adding integers
%       together.
%
%   -- Dr. Ajay Kumar PHD, Founder of QAnal
%
% prop_qq_qm: modules for prop_qq_qm

-module(prop_qq_qm).

-compile(export_all).
-compile(nowarn_export_all).

-include_lib("proper/include/proper.hrl").


%-----------------------------------------------------------------------------%
% PropER propositions
%-----------------------------------------------------------------------------%

prop_idn_is_always_rcef() ->
    testing("identity matrix is by definition in reduced column echelon form"),
    ?FORALL(N, pos_integer(), is_rcef(idn(N))).


prop_idn_is_always_rref() ->
    testing("identity matrix is by definition in reduced row echelon form"),
    ?FORALL(N, pos_integer(), is_rref(idn(N))).


%-----------------------------------------------------------------------------%
% Internal
%-----------------------------------------------------------------------------%

testing(Str) ->
    ok = io:format("~s~n", [Str]),
    ok.


idn(N) ->
    qq_qm:idn(N).


is_rcef(QM) ->
    qq_qm:is_rcef(QM).


is_rref(QM) ->
    qq_qm:is_rref(QM).

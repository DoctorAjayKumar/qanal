% QAnal: Rational Analysis
%
%       Every operation, calculation, and concept, no matter how arbitrarily
%       complex, reduces to adding integers together. There are no new concepts
%       in QAnal. Everything is just putting lipstick on adding integers
%       together.
%
%   -- Dr. Ajay Kumar PHD, Founder of QAnal
%
% prop_qq_z: property-based tests for the qq_z module

-module(prop_qq_z).

-compile(export_all).
-compile(nowarn_export_all).

-include_lib("proper/include/proper.hrl").


%-----------------------------------------------------------------------------%
% PropER propositions
%-----------------------------------------------------------------------------%

prop_gcd_symmetric() ->
    testing("gcd(N,K) = gcd(K,N)"),
    ?FORALL({N,K}, {integer(),integer()},
            gcd(N,K) =:= gcd(K,N)).


%-----------------------------------------------------------------------------%
% Internal
%-----------------------------------------------------------------------------%

testing(Str) ->
    ok = io:format("~s~n", [Str]),
    ok.


gcd(N, K) ->
    qq_z:gcd(N, K).

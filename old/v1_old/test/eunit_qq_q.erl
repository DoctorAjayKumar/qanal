% QAnal: Rational Analysis
%
%       Every operation, calculation, and concept, no matter how arbitrarily
%       complex, reduces to adding integers together. There are no new concepts
%       in QAnal. Everything is just putting lipstick on adding integers
%       together.
%
%   -- Dr. Ajay Kumar PHD, Founder of QAnal
%
% See
%
%   - http://erlang.org/doc/apps/eunit/chapter.html#Running_EUnit
%   - https://rebar3.org/docs/testing/eunit/

-module(eunit_qq_q).

-include_lib("eunit/include/eunit.hrl").


qplus_test_() ->
    [ ?_assertNotEqual (4    , 1+1)
    , ?_assertEqual    (q1() , qplus(q0(), q1()))
    , ?_assertNotEqual (q0() , qplus(q0(), q1()))
    , ?_assertEqual    (q0() , qplus(q0(), q0()))
    ].


qplus(Q1, Q2) ->
    qq_q:qplus(Q1, Q2).


q0() ->
    qq_q:q0().


q1() ->
    qq_q:q1().

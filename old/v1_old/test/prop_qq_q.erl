% QAnal: Rational Analysis
%
%       Every operation, calculation, and concept, no matter how arbitrarily
%       complex, reduces to adding integers together. There are no new concepts
%       in QAnal. Everything is just putting lipstick on adding integers
%       together.
%
%   -- Dr. Ajay Kumar PHD, Founder of QAnal
%
% See
%
%   - http://erlang.org/doc/apps/eunit/chapter.html#Running_EUnit
%   - https://rebar3.org/docs/testing/eunit/

-module(prop_qq_q).

-compile(export_all).
-compile(nowarn_export_all).

-include_lib("proper/include/proper.hrl").


%-----------------------------------------------------------------------------%
% PropER propositions
%-----------------------------------------------------------------------------%

% TODO: Test commutativity, associativity, etc

prop_zplusq0() ->
    testing("an integer qplus q0 equals itself"),
    ?FORALL(Z, integer(), zplusq0(Z) =:= z2q(Z)).


prop_zplusz() ->
    testing("z2q(Z1 + Z2) = qplus(z2q(Z1), z2q(Z2))"),
    ?FORALL({Z1, Z2},
            {integer(), integer()},
            z2q(Z1 + Z2) =:= qplus(z2q(Z1), z2q(Z2))).


prop_ztimesz() ->
    testing("z2q(Z1 * Z2) = qtimes(z2q(Z1), z2q(Z2))"),
    ?FORALL({Z1, Z2},
            {integer(), integer()},
            z2q(Z1 * Z2) =:= qtimes(z2q(Z1), z2q(Z2))).


prop_ztimesq0() ->
    testing("an integer qtimes q0 equals q0"),
    ?FORALL(Z, integer(), ztimesq0(Z) =:= q0()).


prop_ztimesq1() ->
    testing("an integer qtimes 1 equals itself"),
    ?FORALL(Z, integer(), ztimesq1(Z) =:= z2q(Z)).


%-----------------------------------------------------------------------------%
% Internal
%-----------------------------------------------------------------------------%

testing(Str) ->
    ok = io:format("~s~n", [Str]),
    ok.


q0() ->
    qq_q:q0().


q1() ->
    qq_q:q1().


qplus(Q1, Q2) ->
    qq_q:qplus(Q1, Q2).


qtimes(Q1, Q2) ->
    qq_q:qtimes(Q1, Q2).


zplusq0(Z) ->
    qq_q:zplusq(Z, q0()).


ztimesq0(Z) ->
    qq_q:ztimesq(Z, q0()).


ztimesq1(Z) ->
    qq_q:ztimesq(Z, q1()).


z2q(Z) ->
    qq_q:z2q(Z).

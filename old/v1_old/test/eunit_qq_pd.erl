
% QAnal: Rational Analysis
%
%       Every operation, calculation, and concept, no matter how arbitrarily
%       complex, reduces to adding integers together. There are no new concepts
%       in QAnal. Everything is just putting lipstick on adding integers
%       together.
%
%   -- Dr. Ajay Kumar PHD, Founder of QAnal
-module(eunit_qq_pd).

-include_lib("eunit/include/eunit.hrl").

%-----------------------------------------------------------------------------%
% Tests
%-----------------------------------------------------------------------------%

%qplus_test_() ->
%    [ ?_assertNotEqual (4    , 1+1)
%    , ?_assertEqual    (q1() , qplus(q0(), q1()))
%    , ?_assertNotEqual (q0() , qplus(q0(), q1()))
%    , ?_assertEqual    (q0() , qplus(q0(), q0()))
%    ].

input_test_() ->
    [ test_6point()
    , test_6point0()
    , test_6point17()
    , test_6point_plus_6point()
    , test_point17()
    ].


% These should have atomic errors. Instead of the error being some tuple
% cancer, it should be an atom like 'badparse'
%
% test_006() ->
%     DoubleOSix  = s2sf("006"),
%     Map         = sf2map(DoubleOSix),
%     MapShouldBe =
%         #{sig  => 6,
%           figs => infinity,
%           expt => 0},
%     ?_assertEqual(Map, MapShouldBe).
%
%
% test_6() ->
%     Six         = s2sf("6"),
%     Map         = sf2map(Six),
%     MapShouldBe =
%         #{sig  => 6,
%           figs => infinity,
%           expt => 0},
%     ?_assertEqual(Map, MapShouldBe).
%
%
% test_600() ->
%     SixHundred  = s2sf("600"),
%     Map         = sf2map(SixHundred),
%     MapShouldBe =
%         #{sig  => 600,
%           figs => infinity,
%           expt => 0},
%     ?_assertEqual(Map, MapShouldBe).


test_6point() ->
    Six         = s2sf("6."),
    Map         = sf2map(Six),
    MapShouldBe =
        #{sig  => 6,
          figs => 1,
          expt => 0},
    ?_assertEqual(Map, MapShouldBe).


test_6point0() ->
    Six         = s2sf("6.0"),
    Map         = sf2map(Six),
    MapShouldBe =
        #{sig  => 60,
          figs => 2,
          expt => -1},
    ?_assertEqual(Map, MapShouldBe).


test_6point17() ->
    Six         = s2sf("6."),
    Point17     = s2sf(".17"),
    SixPoint17  = sfplus(Six, Point17),
    Map         = sf2map(SixPoint17),
    MapShouldBe =
        #{sig  => 6,
          figs => 1,
          expt => 0},
    ?_assertEqual(Map, MapShouldBe).


test_6point_plus_6point() ->
    Six         = s2sf("6."),
    Twelve      = sfplus(Six, Six),
    Map         = sf2map(Twelve),
    MapShouldBe =
        #{sig  => 12,
          figs =>  2,
          expt =>  0},
    ?_assertEqual(Map, MapShouldBe).


test_point17() ->
    Point17 = s2sf(".17"),
    Map = sf2map(Point17),
    MapShouldBe =
        #{sig  => 17,
          figs => 2,
          expt => -2},
    ?_assertEqual(Map, MapShouldBe).


%-----------------------------------------------------------------------------%
% UTIL FNS
%-----------------------------------------------------------------------------%

%-----------------------------------------------------------------------------%
% ALIASES
%-----------------------------------------------------------------------------%

s2sf(S) ->
    qq_sf:s2sf(S).


sf2map(SF) ->
    qq_sf:sf2map(SF).


sfplus(SF1, SF2) ->
    qq_sf:sfplus(SF1, SF2).

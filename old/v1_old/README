===============================================================================
QAnal: Rational Analysis
===============================================================================

        Every operation, calculation, and concept, no matter how arbitrarily
        complex, reduces to adding integers together. There are no new concepts
        in QAnal. Everything is just putting lipstick on adding integers
        together.

    -- Dr. Ajay Kumar PHD, The Founder


-------------------------------------------------------------------------------
Immediate future plans
-------------------------------------------------------------------------------

Turns out precise arithmetic is really really hard. Exact arithmetic and exact
mathematics, easy. Precise? Really really hard.

- currently working on revelation for homology theory

    - the theory of units is very similar to homology theory.
    - really to meaningfully address the precise analysis, I need to study up
      on both algebra and information theory.
    - I still don't know what the correct data structure is.
    - I will probably move the homology theory stuff into this repository

- Clean up the code
- Move the LaTeX documentation into revelations

    - I am not going to do a revelation on how Gaussian elimination works,
      because there's already a ton of tutorials out there. I will talk about
      how the qq_qm Gaussian elimination works, and perhaps contrast it with
      the Gaussian elimination for homology (which only uses integers).

-------------------------------------------------------------------------------
How to get started
-------------------------------------------------------------------------------

Examples of how to use this library can be found in the ebin/ directory.

You should read the sections of this document on

    - the directory structure of QAnal
    - how to install the necessary software to run QAnal

To learn about the library, read these files in the following order

    1. src/qq.erl       (API)
    2. src/qq_z.erl     (integer arithmetic)
    3. src/qq_q.erl     (rational arithmetic)
    4. src/qq_qm.erl    (rational matrix arithmetic)
    5. doc/frame.pdf    (textbook/package documentation)

        This has since been removed from the git repository because it was
        blowing up the size of the git repository.

        Therefore, you have to do one of the following

            1. Just read the TeX source code found in

                    doc/include/chapters/

            2. Build the PDF file yourself

                $ sudo apt-get install texlive-full
                $ cd doc
                $ make
                $ evince frame.pdf

        Git cannot track the changes between binary files. So every time I
        would make a change to the textbook, git would have to store a new copy
        of the PDF, which as it stands is about 500KB.


To run everything (erl -make, dialyzer, tests, etc), run

    $ make

To compile the source code, run

    $ make build

To run all tests

    $ make tests

To check Dialyzer, run

    $ make dialyzer

To run the EUnit tests suite, run

    $ make eunit

To run the PropER tests suite, run

    $ make proper

To build the PDF documentation, run

    $ cd doc
    $ make

A formal test suite needs to be added, which can double as examples of how to
use the library.


-------------------------------------------------------------------------------
What is this?
-------------------------------------------------------------------------------

It's arithmetic for dealing with fractions exactly. So for instance

    1> 0.1 + 0.2.
    0.30000000000000004
    2> qq:plus(qq:q(1,10), qq:q(2,10)).
    {q,3,10}

QAnal stores a fraction in reduced format as two integers: numerator over
denominator. So there is no opportunity for floating point errors.

The only opportunity for machine error is integer underflow and overflow. My
hunch is that can usually be avoided by not being a moron. In any event, it's
much easier to avoid underflow/overflow issues than floating point errors.

The difference in philosophy between QAnal and every other number crunching
library is this:

    - If you ask numpy a stupid question, numpy will give you a stupid answer

        >>> import numpy as np
        >>> np.sqrt(2)
        1.4142135623730951

    - If you ask QAnal a stupid question, it will crash your program

        2> qq:pow(2, qq:q(1,2)).
        ** exception error: {type_error,non_integer_pow,{pow,2,{q,1,2}}}
             in function  qq:pow/2 (src/qq.erl, line 118)

The goal of QAnal is to have a codebase full of well-commented, well-documented
code that is obviously correct implementing common algorithms. Additionally, it
is a research project about rational arithmetic.

There is almost nothing one can do with floats that is obviously correct.
Therefore, floats are banned.

Attempting to perform an operation that takes one outside the boundaries of
rational numbers, from the standpoint of QAnal, is as severe an offense as
attempting to divide by zero. So QAnal will crash your program if you try, just
as it will if you try to divide by zero.


-------------------------------------------------------------------------------
Prerequisite software for Erlang
-------------------------------------------------------------------------------

For building the PDF documentation, see the appropriate section in this
document.

UBUNTU 18.04

    Consult this blog post regarding installing Erlang on Ubuntu 18.04:

        https://zxq9.com/archives/1797

    If by the time you are reading this, that link is so out of date as to not
    be useful, please open a bug, send a patch, or email the maintainer of this
    package.


OTHER *NIX

    If you are on another *NIX system, read the instructions for Ubuntu 18.04,
    and attempt to translate them to your operating system.

    If it is not obvious how to translate the Ubuntu instructions to your
    system, please open a bug report, send a patch, or email the maintainer of
    this package with your issue.


OTHER OS (e.g. Windows)

    Please figure out how to install and run QAnal yourself, then send a patch
    updating this document with instructions for your OS.


-------------------------------------------------------------------------------
Prerequisite software for documentation
-------------------------------------------------------------------------------

UBUNTU 18.04

    The best bet is to install the texlive-full package

        $ sudo apt-get update
        $ sudo apt-get install texlive-full

    You may be able to get away with installing a more minimal subset of TeX
    packages, although I have not bothered to check. See the following blog
    post for more details:

        https://linuxconfig.org/how-to-install-latex-on-ubuntu-18-04-bionic-beaver-linux


OTHER *NIX

    TeXLive is very popular, so your operating system likely contains
    documentation on how to install it. Consult that documentation first.

    If you are on another *NIX system, read the instructions for Ubuntu 18.04,
    and attempt to translate them to your operating system.

    If it is not obvious how to translate the Ubuntu instructions to your
    system, please open a bug report, send a patch, or email the maintainer of
    this package with your issue.


OTHER OS (e.g. Windows)

    Please figure out how to build the QAnal documentation yourself, then send
    a patch updating this document with instructions for your OS.



-------------------------------------------------------------------------------
Directory structure
-------------------------------------------------------------------------------

Top level

- doc/              Textbook/package documentation, written in LaTeX

    - doc/.projectile       Dummy file for emacs users who use projectile
    - doc/frame.pdf         PDF documentation
    - doc/frame.tex         macros, imports, etc; compiles into frame.pdf

    - doc/Makefile          Compiles PDF documentation

        - make [all]            runs targets setup, build, clean
        - make setup            creates sandbox, copies all required files in
        - make build            builds PDF file
        - make clean            deletes sandbox

    - doc/include/          things that frame.tex needs to compile

        - doc/include/bible.bib     bibliography data
        - doc/include/chapters/     where source text for chapters is
        - doc/include/code/         where code listings go (if not inline)
        - doc/include/images/       for images

- ebin/             Standard OTP directory; also contains escript examples
- src/              Library source code


-------------------------------------------------------------------------------
Copyright
-------------------------------------------------------------------------------

Copyright 2020-2021 Dr. Ajay Kumar PHD

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

===============================================================================
Notes to self
===============================================================================

-------------------------------------------------------------------------------
Joggerlicious dialyzer error
-------------------------------------------------------------------------------

Dialyzer frequently will give the completely useless error message

      Checking whether the PLT /home/doctorajaykumar/.dialyzer_plt is up-to-date... yes
      Proceeding with analysis...
    qq_qm.erl:1117: The pattern
              {'true', AtomWaffle} can never match the type
              'false'
     done in 0m0.39s

I believe this means that somewhere way down in the call chain you are
returning something that breaks the typespec. So for instance, in this case,
the culprit was returning this term

    {true,
     {leading_1_left_of_prior_leading_1,
      {the_idx1_of_the_geographically_higher_row_is, PreviousRowIdx1},
      {the_idx1_of_the_geographically_lower_row_is, ThisRowIdx1}}}

When the proper return type was

    :: false
     | {true,
        {leading_1_left_of_prior_leading_1,
             {the_idx1_of_the_geographically_higher_row_is, idx1()},
             {the_idx1_of_the_geographically_lower_row_is, idx1()},
             so_therefore_swap_rows}}

I forgot the last atom, 'so_therefore_swap_rows'

Dialyzer did detect that I was sending back a malformed term, but did
not give me an error message that made any amount of sense.

-------------------------------------------------------------------------------
Scratch notes
-------------------------------------------------------------------------------

- Write out action of integer addition on the precise decimals (easy)
- Write out action of integer multiplication on the precise decimals (easy)
- Write out action of dividing a precise decimal by an integer (probably some
  subtlety)
- Work out how to do precise division (I think I have an idea)

- Figure out multiplicative inversion in precise arithmetic. There's actually
  no single unit in the set of precise symbols.

- Adding the exact integer 0 has the null action, as does

Next we need to do some algebraic checks, using PropER first to see if they're
true, and then maybe prove them

    - Check associative property of precise addition

        (=?
            (pplus (pplus P1 P2) P3)
            (pplus P1 (pplus P2 P3))
        )

        Expectation: TRUE

    - Check commutative property of precise addition

        (=?
            (pplus P1 P2)
            (pplus P2 P1)
        )

        Expectation: TRUE


    - Check associative property of precise multiplication

        (=?
            (ptimes (ptimes P1 P2) P3)
            (ptimes P1 (ptimes P2 P3))
        )

        Expectation: FALSE

    - Check commutative property of precise multiplication

        (=?
            (ptimes P1 P2)
            (ptimes P2 P1)
        )

        Expectation: TRUE

    - Check distributive property of precise multiplication

        (=?
            (ptimes P1 (pplus P2 P3))
            (pplus (ptimes P1 P2)
                   (ptimes P1 P3))
        )

        Expectation: FALSE

Some things have become very clear

    - the mathematical framework created---in particular, the QAnal Fissure and
      precise arithmetic---are incredibly useful

    - the code so far needs to be restructured and mostly rewritten

Structure for the next generation of code, as I see it now (listed in order of
order) (this is likely to change):

    The basenum modules are:

        exact basenums:

            - bna_xz (basenum arithmetic, exact integers)

              implements type xz() and all integer arithmetic

            - bna_xq (basenum arithmetic, exact quotients)

              implements type xq(), rational arithmetic, built out of integer
              arithmetic

            - bna_x (basenum arithmetic, exact)

              implements type x(), which is one of xz() or xq()

              general interface for dealing with these from the outside


        precise basenums:

            - bna_p (basenum arithmetic, precise) , which is a better version
              of sigfigs, built out of exact integers xz())


        basenum interface module:

            - bna (basenum arithmetic), which higher-order structures call to
              do their basenum arithmetic

    Beyond that we have


        - una (united arithmetic),


    General rules:

        - low-order things ACT on high-order things rather than being typecast
          into them

          therefore, code for mixed-type operands (for instance, multiplying an
          integer by a rational), goes in the module for the highest-order
          operand.

        - in particular, x* things can act on p* things, but not the other way
          around

        - in this manner, "typecasting" only goes up the river of order

        - antiorder flows downhill


    Issues with this structure:

        - Before I commit to this structure, I need to figure out how division
          works in precise arithmetic. Because it's not clear at this point how
          rationals act on precise numbers.

        - I need to figure out how to nicely let the higher-order types
          enforce constraints in the lower-order types.

        - A usage case that some people will have is modular arithmetic

          Actually, no. The normal tools that ship with every programming
          language are perfectly satisfactory for modular arithmetic

        - Focus on the things that we can do that other programming languages
          can't, and focus on the usage cases I will actually use

            - so for instance, precise numbers only for decimals
            - mention in the book how to do it for any base, and in fact how it
              applies to arbitrary tree structures, not just numbers. But the
              parser is complicated enough, leave well enough alone.

              Most float data in the real world is in decimal notation. If we
              encounter a usage case, then add it

        - Add things (and possibly restructure) when the usage cases dictate

-------------------------------------------------------------------------------
Longer term future plans
-------------------------------------------------------------------------------

This is an educational thing but it seems likely to be accidentally useful.

- numeric linear algebra (Gaussian elimination, determinants, common
  factorizations)

  Need to figure out how to cleanly write the library to separate exact and
  precise.

- complex matrices (think precisely about implementation)
- quaternions (including a tutorial on 3D rotation math)
- maybe tensors if I have a compelling usage case
- neural network implementation

- maybe implement the neural network concept in Python first
- dataframes maybe
- I think for the neural network, dataframes are a must, just so we can have a
  train_on_df function
- automatic differentiation of polynomials using dual numbers


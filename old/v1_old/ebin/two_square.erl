#!/usr/bin/env escript

% Test RREF

-mode(compile).

main([]) ->
    foo(
        "-1  1  0  0  0  0  0;"
        " 0 -1  1  0  1  0  0;"
        " 0  0 -1  1  0  0  0;"
        " 0  0  0 -1  0  1  0;"
        " 0  0  0  0 -1 -1  1;"
        " 1  0  0  0  0  0 -1"
    ).

foo(S) ->
    ok = dashline(),

    QM = qq:qm(S),
    ok = io:format("QM:~n"),
    ok = qq:pp(QM),

    %Cycle =
    %    qq:qm(
    %        "1 ; 1 ; 0 ; 0 ; 
    %    )

    RREF = qq_qm:rref(QM),
    ok = io:format("~nRREF:~n"),
    ok = qq:pp(RREF),
    ok.

dashline() ->
    Dashline = [$- || _ <- lists:seq(1, 80)],
    ok = io:format("~n" ++ Dashline ++ "~n").
%
%pp_rref(QM) ->
%    RREF = qq:rref(QM),
%    ok = qq:pp(RREF),
%    ok = io:format("~n", []),
%    ok.
%
%prq(S) ->
%    ok = pp_rref(qq:qm(S)),
%    ok.

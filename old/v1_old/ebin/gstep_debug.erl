#!/usr/bin/env escript

% Test RREF

-mode(compile).

main([]) ->
    ok
    , foo(
            "-1 -6  9;"
            " 5  1  5;"
            "-2  6  8;"
        )
    .

foo(S) ->
    NSteps = 25,
    QM = qq:qm(S),
    ok = qq_qm:gstep_debug(QM, NSteps),
    ok.

%
%pp_rref(QM) ->
%    RREF = qq:rref(QM),
%    ok = qq:pp(RREF),
%    ok = io:format("~n", []),
%    ok.
%
%prq(S) ->
%    ok = pp_rref(qq:qm(S)),
%    ok.

#!/usr/bin/env escript

% Test RREF

-mode(compile).

main([]) ->
    ok
    , foo("1 0 0; 0 1 0; 0 0 1")
    , foo("1 0 0; 0 3 0; 0 0 1")
    , foo("0 0 0; 0 1 0; 0 0 1")
    , foo("0 1 0; 1 0 0; 0 0 1")
    , foo("0 0 1; 0 1 0; 1 0 0")
    , foo("0 0 1 0; 0 1 0 0; 1 0 0 0")
    , foo("1 2 3; 5 89 2; 87  3 16; 22 1 -99/7")
    .

foo(S) ->
    ok = dashline(),

    QM = qq:qm(S),
    ok = io:format("QM:~n"),
    ok = qq:pp(QM),

    RREF = qq_qm:rref(QM),
    ok = io:format("~nRREF:~n"),
    ok = qq:pp(RREF),
    ok.

dashline() ->
    Dashline = [$- || _ <- lists:seq(1, 80)],
    ok = io:format("~n" ++ Dashline ++ "~n").
%
%pp_rref(QM) ->
%    RREF = qq:rref(QM),
%    ok = qq:pp(RREF),
%    ok = io:format("~n", []),
%    ok.
%
%prq(S) ->
%    ok = pp_rref(qq:qm(S)),
%    ok.

#!/usr/bin/env escript

% Test RREF

-mode(compile).

main([]) ->
    M = qq:qm("-1  0  0  0  0  1  0;"
              " 0  0  0  0  1 -1  1;"
              " 0  0  0  1 -1  0  0;"
              " 0  0  1 -1  0  0  0;"
              " 0  1 -1  0  0  0 -1;"
              " 1 -1  0  0  0  0  0;"),
    RREF = qq_qm:rref(M),
    ok = qq:pp(RREF).

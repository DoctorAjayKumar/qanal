#!/usr/bin/env escript

% Test RREF

-mode(compile).

main([]) ->
    A = qq:qm("-4  0  3;"
              " 4 -2  0;"
              " 1  1  1;"),
    B = qq:qm("-12; 5; 0"),
    AInv = qq_qm:inverse_left(A),
    Result = qq:x(AInv, B),
    ok = qq:pp(Result).

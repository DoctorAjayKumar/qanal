% WildFuck polynomials (Algebra of Boole reduction).
%
% shows the basic logical connectives
%
% want it to
%
%   - be able to reduce WildFuck polynomials
%   - given a truth table, produce the wildfuck polynomial
%   - given a wildfuck polynomial, produce its truth table
%   - use it to solve the labyrinth doors problem
%   - show the steps in how it derives/reduces the polynomial
%
% You remember how the Founder said that everything in QAnal reduces to adding
% integers together? Well logic does too.
-module(qw).

-behavior(qanal_ring).

-type wf() :: zero
            | one
            | {plus, qw(), qw()}
            | {times, qw(), qw()}
            | {atom, atom()}
            .

% reduced wildfuck polynomial always reduces to a sum of products
%-type wfr() ::


%%%
%%% standard logic gates
%%%

land(0, 0) -> 0;
land(0, 1) -> 0;
land(1, 0) -> 0;
land(1, 1) -> 1.


% wildfucky things

% % logic gate AND = mod2 product
% land(0, 0) -> 0;
% land(0, 1) -> 0;
% land(1, 0) -> 0;
% land(1, 1) -> 1;
wand(A, B) ->
    times(A, B).

% wildfuck inclusive or
% lior(0, 0) -> 0; % (1 + A)(1 + B)
% lior(0, 1) -> 1; % (1 + A)(B)
% lior(1, 0) -> 1; % (A)(1 + B)
% lior(1, 1) -> 1; % AB
%
%   lior(A, B) = xor(
wior(A, B) ->
    sum([one(), A, B, times(A, B)).


wxor(A, B) ->
    plus(A, B).


%%%
%%% ring callbacks
%%%

additive_inverse(X) ->
    X.

one() ->
    one.

zero() ->
    zero.

%% qxh.erl
-export([
    bp/2,
    hp/2,
    hpspline/1,
    hqspline/1,
    hspline/1,
    iq/2,

-spec bp(h(), h()) -> h().
% @doc bp = botch product; one half the conjugate of the first
% times the second plus the first times the conjugate of the second;
% almost the same as the inner product
%
% don't know what this does, but it's interesting
%
% ap(X, Y) -> (1/2) * conj(X) * Y
% bp(X, Y) -> (1/2) * [ conj(X)*Y + X*conj(Y) ]
% ip(X, Y) -> (1/2) * [ conj(X)*Y + conj(Y)*X ]
% iq(X, Y) -> (1/4) * [ conj(X)*Y - conj(Y)*X ]

bp(X, Y) ->
    OneHalf_q = qq(1, 2),
    OneHalf_h = from_q(OneHalf_q),
    times([OneHalf_h,
           plus(times(conj(X), Y),
                times(X, conj(Y)))]).



-spec hp(h(), h()) -> h().
% @doc "holy product"
%
% ap(X, Y) -> (1/2) * bar(X) * Y
% bp(X, Y) -> (1/2) * (bar(X)*Y + X*bar(Y))
% hp(X, Y) -> (1/4) * [-conj(X)*Y + conj(Y)*X ]
%   - incorrect handedness in 2 space
%   -   correct handedness in 3 space
% hp(X, Y) -> (1/2) * X*Y
%   - sometimes correct, usually just wrong
% hp(X, Y) -> (1/2) * conj(X)*Y
%   - wrong handedness in 3 space
% hp(X, Y) -> (1/2) * X*conj(Y)
%   - wrong handedness in both spaces
% hp(X, Y) -> (1/2) * conj(Y)*X
%   - wrong handedness in 2-space
%   - right handedness in 3-space
%   - weird scalar
% hp(X, Y) -> (1/4) * [ conj(Y)*X - conj(X)*Y ]
%   - wrong handedness in 2-space
%   - right handedness in 3-space
%   - no weird scalar
% hp(X, Y) -> (1/2) * Y * conj(X)
%   - right handedness in 2-space
%   - wrong handedness in 3-space
%   - weird scalar (but maybe that's good)
% hp(X, Y) -> (1/2) - (1/2) * conj(Y) * X
%   - correct handedness in both spaces
%   - weird scalar (but maybe that's good)
% hp(X, Y) -> (1/2) * [ 1 - H2*conj(H1) ]
%   - wrong handedness in both spaces
%   - wierd scalar
% hp(X, Y) -> (1/2) * [ 1 - conj(H2)*H1 ]
%   - right handedness in 2-space
%   - wrong handedness in 3-space
%   - wierd scalar
% ip(X, Y) -> (1/2) * (bar(X)*Y + bar(Y)*X)
% iq(X, Y) -> (1/4) * [ conj(X)*Y - conj(Y)*X ]
%   -   correct handedness in 2 space
%   - incorrect handedness in 3 space

hp(H1, H2) ->
    times([from_q(qq(1, 2)), H2, conj(H1)]).



-spec hpspline([h()]) -> h().
% @doc
% like hspline but uses hp/2 rather than ap/2

hpspline(List) ->
    hpspline_accum(List, zero()).



-spec hqspline([h()]) -> h().
% @doc
% like hspline but uses the inner quotient instead of the asymmetric
% product

hqspline(List) ->
    hqspline_accum(List, zero()).



-spec hspline([h()]) -> h().
% @doc
% takes the asymmetric products of consecutive pairs of items in the
% lists, and then the sum
%
% asymmetric product = one half conjugate of the first times the
% second
%
% i have no idea what this does, but it seems more morally
% correct than cspline/1
%
% all it does is add up the pairwise inner products of the list;
% the list is "assumed to be bracketed on either side by 0"

hspline(List) ->
    hspline_accum(List, zero()).



-spec iq(h(), h()) -> h().
% @doc "inner quotient"
%
% ap(X, Y) -> (1/2) * conj(X) * Y
% bp(X, Y) -> (1/2) * [ conj(X)*Y + X*conj(Y) ]
% ip(X, Y) -> (1/2) * [ conj(X)*Y + conj(Y)*X ]
% iq(X, Y) -> (1/4) * [ conj(X)*Y - conj(Y)*X ]

iq(H1, H2) ->
    OneHalf_q = qq(1, 4),
    OneHalf_h = from_q(OneHalf_q),
    Minus1    = from_z(-1),
    times(OneHalf_h,
          plus(times(conj(H1), H2),
               times([Minus1, conj(H2), H1]))).


-spec hspline_accum(Pts :: [h()], Accum :: h()) -> WTF :: h().
% @private
% fold over ap, and take the sum

hspline_accum([Z1, Z2 | Rest], AccumSum) ->
    hspline_accum([Z2 | Rest], plus(ap(Z1, Z2), AccumSum));
hspline_accum(_, Accum) ->
    Accum.



-spec hpspline_accum(Pts :: [h()], Accum :: h()) -> WTF :: h().
% @private
% fold over hp, take the sum

hpspline_accum([Z1, Z2 | Rest], AccumSum) ->
    hpspline_accum([Z2 | Rest], plus(hp(Z1, Z2), AccumSum));
hpspline_accum(_, Accum) ->
    Accum.



-spec hqspline_accum(Pts :: [h()], Accum :: h()) -> WTF :: h().
% @private
% fold over iq, take the sum

hqspline_accum([Z1, Z2 | Rest], AccumSum) ->
    hqspline_accum([Z2 | Rest], plus(iq(Z1, Z2), AccumSum));
hqspline_accum(_, Accum) ->
    Accum.

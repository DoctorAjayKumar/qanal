% qanal.erl
-export([
    bp/2,
    hpspline/1,
    hqspline/1,
    hspline/1,
    hp/2,
    iq/2,
]).

-spec bp(dumbnum(), dumbnum()) -> h().
% @doc
% "botch product"
%
% ap(X, Y) -> (1/2) * conj(X) * Y
% bp(X, Y) -> (1/2) * [ conj(X)*Y + X*conj(Y) ]
% ip(X, Y) -> (1/2) * [ conj(X)*Y + conj(Y)*X ]
% iq(X, Y) -> (1/4) * [ conj(X)*Y - conj(Y)*X ]

bp(X, Y) ->
    qxh:bp(whiten(X), whiten(Y)).



-spec hp(dumbnum(), dumbnum()) -> h().
% @doc
% "holy product"

hp(A, B) ->
    qxh:hp(whiten(A), whiten(B)).



-spec hpspline([dumbnum()]) -> h().
% @doc like hspline/1 but folds over hp/2 rather than ap/2

hpspline(Blacklist) ->
    Whitelist = lists:map(fun whiten/1, Blacklist),
    qxh:hpspline(Whitelist).



-spec hqspline([dumbnum()]) -> h().
% @doc like hspline/1 but folds over iq/2 rather than ap/2
%
% This seems to be the thing with the behavior we want
%
%   90> HQSpline(CubeFront).
%   {h,{q,0,1},{q,-1,1},{q,0,1},{q,0,1}}
%   91> HQSpline(CubeBack).
%   {h,{q,0,1},{q,1,1},{q,0,1},{q,0,1}}
%   92> HQSpline(CubeFront ++ CubeBack).
%   {h,{q,0,1},{q,0,1},{q,0,1},{q,0,1}}
%   93> HQSpline(CubeFront ++ CubeBack ++ CubeL ++ CubeR).
%   {h,{q,0,1},{q,0,1},{q,0,1},{q,0,1}}
%   94> HQSpline(CubeFront ++ CubeBack ++ CubeL ++ CubeR ++ CubeTop).
%   {h,{q,0,1},{q,0,1},{q,0,1},{q,-1,1}}
%   95> HQSpline([3, C(3, 4)]).
%   {h,{q,0,1},{q,6,1},{q,0,1},{q,0,1}}
%   96> HQSpline([1, I]).
%   {h,{q,0,1},{q,1,2},{q,0,1},{q,0,1}}
%   97> HQSpline([1, C(Q(1, 2), Q(1, 2)), I]).
%   {h,{q,0,1},{q,1,2},{q,0,1},{q,0,1}}
%
% TODO: Instead of iq, this should be called the "white product", or
% wp/2
%
% dammit it's left handed

hqspline(Blacklist) ->
    Whitelist = lists:map(fun whiten/1, Blacklist),
    qxh:hqspline(Whitelist).



-spec hspline([dumbnum()]) -> h().
% @doc no idea what the significance of this is, but it's definitely
% interesting; takes the pairwise asymmetric products of the
% successive numbers, and then takes the sum
%
% I'm not 100% sure what this does.
%
% Experimentally, on a cube, the imaginary components seem to encode
% "signed area normal to this direction"
%
% No idea what's goin on in the real component
%
% Given a cube oriented such that
%
%   - bottom/top = IJ plane (normal to K)
%   - left/right = KI plane (normal to J)
%   - front/back = JK plane (normal to I)
%
%   66> HSpline(CubeBot).
%   {h,{q,1,1},{q,0,1},{q,0,1},{q,1,1}}
%   67> HSpline(CubeTop).
%   {h,{q,3,1},{q,0,1},{q,0,1},{q,-1,1}}
%   68> HSpline(CubeL).
%   {h,{q,1,1},{q,0,1},{q,1,1},{q,0,1}}
%   69> HSpline(CubeR).
%   {h,{q,3,1},{q,0,1},{q,-1,1},{q,0,1}}
%   70> HSpline(CubeFront).
%   {h,{q,3,1},{q,-1,1},{q,0,1},{q,0,1}}
%   71> HSpline(CubeBack).
%   {h,{q,1,1},{q,1,1},{q,0,1},{q,0,1}}
%
% Each of the Cube* variables are a list of vertices that traverse
% the edges of that side; the orientation is chosen such that the
% right-hand-rule gives a normal vector pointing out of the cube
%
% For instance:
%
%   73> CubeFront.
%   [0,
%    {h,{q,0,1},{q,1,1},{q,0,1},{q,0,1}},
%    {h,{q,0,1},{q,1,1},{q,1,1},{q,0,1}},
%    {h,{q,0,1},{q,1,1},{q,1,1},{q,1,1}},
%    {h,{q,0,1},{q,1,1},{q,0,1},{q,1,1}},
%    {h,{q,0,1},{q,1,1},{q,0,1},{q,0,1}},
%    0]
%
% (see etc/unit_cube_bak.png for a diagram)

hspline(Blacklist) ->
    Whitelist = lists:map(fun whiten/1, Blacklist),
    qxh:hspline(Whitelist).


-spec iq(dumbnum(), dumbnum()) -> h().
% @doc
% "inner quoteint"
%
% ap(X, Y) -> (1/2) * conj(X) * Y
% bp(X, Y) -> (1/2) * [ conj(X)*Y + X*conj(Y) ]
% ip(X, Y) -> (1/2) * [ conj(X)*Y + conj(Y)*X ]
% iq(X, Y) -> (1/4) * [ conj(X)*Y - conj(Y)*X ]

iq(A, B) ->
    qxh:iq(whiten(A), whiten(B)).



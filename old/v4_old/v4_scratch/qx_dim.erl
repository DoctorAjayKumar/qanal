% @doc
% dimensions: can be multiplied and divided, that's it
-module(qx_dim).

-export_type([
    dim/0,
    dimension/0,
    unit/0
]).
-export([
]).

% @doc
-type dim()       :: dimension().
-type dimension() :: {dim, sms(unit())}.
-type unit()      :: atom() | {epsilon, atom()}.
-type sms(X)      :: qx_sms:sms(X).




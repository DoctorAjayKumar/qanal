(defmodule qanal
    (export
        [c 2]
        [cspline 1]
        [h 4]
        [hspline 1]
        [i 0]
        [j 0]
        [k 0]
        [q 2]
        [r 1]
        [plus 1]
        [plus 2]
        [times 1]
        [times 2]
        [v 3]
        [whiten 1]
        [zero 0]
        [zgcd 1]
        [zgcd 2]
        [zmodulo 2]
    )
    (type
        ([dumbnum] (UNION [z] [q] [h]))
        ([h]       [qxh:h])
        ;([hc]      [qxh:hc])
        ([hr]      [qxh:hr])
        ([q]       [qxq:q])
        ([z]       [qxz:z])
        ([znn]     [qxz:znn])
        ;([zp]      [qxz:zp])
    )
    (spec
        ([cspline 1] ([(list (dumbnum))]    (hr)))
        ([q 2]       ([(dumbnum) (dumbnum)] (h)))
        ([whiten 1]  ([(dumbnum)]           (h)))
        ([zgcd 1]    ([(list (z))]          (znn)))
        ([zgcd 2]    ([(z) (z)]             (znn)))
        ([zmodulo 2] ([(z) (z)]             (znn)))
    )
)

(defun c (rq iq)
    (h rq iq (zero) (zero)))


(defun cspline (args)
    "signed area enclosed by list of complex numbers"
    (let ((whiteargs (lists:map (lambda (x) (whiten x)) args)))
        (qxh:cspline whiteargs)))


(defun h (rq iq jq kq)
    "h(R,I,J,K) -> R + I*i + J*j + K*k; whitens arguments"
    (plus (list (whiten rq)
                (times iq (i))
                (times jq (j))
                (times kq (k)))))


(defun hspline (args)
    "god only knows what this does, but it looks interesting"
    (let ((whiteargs (lists:map (lambda (x) (whiten x)) args)))
        (qxh:hspline whiteargs)))


(defun i () (qxh:i))
(defun j () (qxh:j))
(defun k () (qxh:k))


(defun plus (blacklist)
    (let ((whitelist (lists:map (lambda (x) (whiten x)) blacklist)))
        (qxh:plus whitelist)))


(defun plus (a b)
    (qxh:plus (whiten a) (whiten b)))


(defun q (a b)
    "divide two dumbnums, produce a quaternion"
    (qxh:q (whiten a) (whiten b)))


(defun r (x)
    "alias for whiten; r for \"real\", meant for loading real scalars"
    (whiten x))


(defun times (blacklist)
    "multiply a list of dark numbers to produce a white number"
    (let ((whitelist (lists:map (lambda (x) (whiten x)) blacklist)))
        (qxh:times whitelist)))


(defun times (a b)
    "mulitply two dark numbers, produce a white number"
    (qxh:times (whiten a) (whiten b)))


(defun v (ci cj ck)
    "make vector X*i + Y*j + Z*k"
    (h (zero) (whiten ci) (whiten cj) (whiten ck)))


(defun whiten
    "typecast number up into a quaternion"
    ([z] (when (is_integer z))
        (qxh:from_z z))
    ([(tuple 'q x y)]
        (qxh:from_q `#(q ,x ,y)))
    ([(tuple 'h cr ci cj ck)]
        (tuple 'h cr ci cj ck)))


(defun zero ()
    (qxh:zero))


(defun zgcd (list)
    "greatest common divisor of a list of integers"
    (qxz:gcd list))


(defun zgcd (a b)
    "greatest common divisor of two integers"
    (qxz:gcd a b))


(defun zmodulo (a b)
    "(modulo a b) returns r = `a beaten down mod b', s.t. 0 =< r < b"
    (qxz:modulo a b))

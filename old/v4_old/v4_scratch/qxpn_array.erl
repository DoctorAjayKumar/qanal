% @doc
% Qanal Xanal PolyNumbers
%
% (polynomials)
%
% single variable for now
%
% represented as variable-size arrays
%
% arrays don't seem quite right for this
%
% I was hoping that when I converted an array back to a list it would
% skip the trailing zeros, but it doesn't, and that's quite annoying
%
% list is better because it's more transparent and I can just write
% code to delete trailing zeros
-module(qxpn).

-export_type([
    pn/0
]).
-export([
    from_list/1,
    pf/1,
    pp/1,
    scalar/1,
    to_list/1
]).

-record(pn,
        {array :: array:array(hq())}).

-type idx0() :: qxh:hq().
-type hq()   :: qxh:hq().
-type pn()   :: #pn{}.


%%%
%%% API
%%%

-spec from_list([hq(), ...]) -> pn().
% @doc polynumber with coefficients

from_list(List) when 1 =< length(List) ->
    Default = hzero(),
    Array   = array:from_list(List, Default),
    {pn, Array}.



-spec pf(pn()) -> iolist().
% @doc pretty format
%
% (pn coeff1 coeff2 coeff3 etc)

pf(PN) ->
    Coeffs = to_list(PN),
    ok = io:format("~p~n", [Coeffs]),
    ["(pn", [[" ", qxh:pf(Coeff)] || Coeff <- Coeffs], ")"].



-spec pp(pn()) -> ok.
% @doc pretty print
%
% (pn coeff1 coeff2 coeff3 etc)

pp(PN) ->
    ok = io:format("~ts~n", [pf(PN)]),
    ok.



-spec scalar(hq()) -> pn().
% @doc
% "degree"-0 polynumber

scalar(HQ) ->
    from_list([HQ]).



-spec to_list(pn()) -> [hq()].
% @doc
% get list of coefficients

to_list({pn, Array}) ->
    array:to_list(Array).



%%%
%%% INTERNALS
%%%

hzero() ->
    qxh:zero().


delete_trailing_zeros(List) ->
    dtz_rev(lists:reverse(List)).


dtz_rev([_Zero = {h, _Re = {q, 0, 0}, _Im = {q, 0, 0}, _Jm = {q, 0, 0}, _Km = {q, 0, 0}}
         | Rest
        ]) ->
    dtz_rev(Rest);
dtz_rev(DoesntStartWithZero) ->
    lists:reverse(DoesntStartWithZero).

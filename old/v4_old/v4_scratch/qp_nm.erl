% @doc
%       Every operation, calculation, and concept, no matter how
%       arbitrarily complex, reduces to adding integers together.
%       There are no new concepts in QAnal. Everything is just
%       putting lipstick on adding integers together.
%
%   -- Dr. Ajay Kumar PHD, Founder of QAnal
%
% qp_nm: [Q]Anal [P]reAnal [N]ewton's [M]ethod
-module(qp_nm).

-export([newton_step/2, newton_steps/3,
         sqrt/2, sqrt/1]).


-type qanal() :: qanal:qanal().

% @doc
% write functions against the simple dual primitives
% apply to scalar argument
%
% Newton's method: new-arg = (- old-arg (/ (f old-arg)
%                                          (f-prime old-arg)))
-spec newton_step(Fun, Arg) -> NewArg
            when Fun    :: fun((qanal()) -> qanal()),
                 Arg    :: qanal(),
                 NewArg :: qanal().
newton_step(Fun, Arg) ->
    {se, F, FPrime} = qx_sd:fmap(Fun, [Arg]),
    NewArg = qanal:minus(Arg, qanal:divide(F, FPrime)),
    NewArg.

newton_steps(_Fun, EndPoint, 0) ->
    EndPoint;
newton_steps(Fun, StartPoint, N) when is_integer(N) and 1 =< N ->
    NewStartPoint = newton_step(Fun, StartPoint),
    newton_steps(Fun, NewStartPoint, N - 1).


% solving for (= 0
%                (- (sq X) A))
-spec sqrt(qx_q:q(), pos_integer()) -> qx_q:q().
sqrt(A, NSteps) ->
    Polynomial =
        fun(X) ->
            qx_sd:minus(qx_sd:sq(X), A)
        end,
    One = qanal:am_one(A),
    newton_steps(Polynomial, One, NSteps).


sqrt(X) ->
    sqrt(X, 5).

% QAnal matrices
-module(qm).


-export_type([qm/0]).

% accessors
-export([shape/1,
         arith_mod/1,
         tpl/1]).

-export([get/2,
         set/3]).

-export([getcol/2,
         getrow/2]).

-export([transpose/1]).

% constructors
-export([idn/2,
         zeros/2]).

% conversion
-export([from_lists/1,
         to_lists/1]).

% arrayish
-export([map/2,
         foldl/3]).


% types
-record(qm,
        {shape     :: {pos_integer(), pos_integer()},
         arith_mod :: atom(),
         tpl       :: tuple()}).
-type qm() :: #qm{}.

-type shape()     :: {pos_integer(), pos_integer()}.
-type arith_mod() :: atom().
-type val()       :: qanal:qanal().
-type ijpair()    :: {idx1(), idx1()}.
-type idx1()      :: pos_integer().

% accessors
-spec shape(qm())     -> shape().
-spec arith_mod(qm()) -> arith_mod().
-spec tpl(qm())       -> tuple().

-spec get(ijpair(), qm())        -> val().
-spec set(ijpair(), val(), qm()) -> qm().

-spec getcol(idx1(), qm()) -> qv:qv().
-spec getrow(idx1(), qm()) -> qv:qv().

% constructions
-spec zeros(shape(), arith_mod()) -> qm().

% conversion
-spec from_lists([[val(), ...], ...]) -> qm().
-spec to_lists(qm())                  -> [[val(), ...], ...].

% arrayish
-spec map(Fun, qm()) -> qm()
        when Fun      :: fun((IJPair, Val, ArithMod) -> NewVal),
             IJPair   :: ijpair(),
             Val      :: val(),
             ArithMod :: arith_mod(),
             NewVal   :: val().


-spec foldl(Fun, InitAcc, qm()) -> Acc
        when Fun      :: fun((IJPair, Val, ArithMod, Acc) -> NewAcc),
             IJPair   :: ijpair(),
             Val      :: val(),
             ArithMod :: arith_mod(),
             InitAcc  :: Acc,
             Acc      :: any(),
             NewAcc   :: Acc.


% -spec map(Fun, qm()) -> qm()
%     when Fun :: fun((IJPair, Val, ArithMod) -> NewVal),

% -spec foldl(Fun, InitAcc, qm()) -> Acc
%     when Fun :: fun((IJPair, Val, ArithMod, Acc) -> NewAcc),

%%%
%%% accessors
%%%
shape(#qm{shape = Shape})      -> Shape.
arith_mod(#qm{arith_mod = AM}) -> AM.
tpl(#qm{tpl = Tpl})            -> Tpl.


get({I, J}, #qm{shape = {NR, NC}, tpl = Tpl})
        when ( (1 =< I) andalso (I =< NR) ) andalso
             ( (1 =< J) andalso (J =< NC) ) ->
    RowI   = erlang:element(I, Tpl),
    ElemIJ = erlang:element(J, RowI),
    ElemIJ.


set({I, J}, Val, QM = #qm{shape = {NR, NC}, tpl = Tpl})
        when ( (1 =< I) andalso (I =< NR) ) andalso
             ( (1 =< J) andalso (J =< NC) ) ->
    RowI    = erlang:element(I, Tpl),
    NewRowI = erlang:setelement(J, RowI, Val),
    NewTpl  = erlang:setelement(I, Tpl, NewRowI),
    QM#qm{tpl = NewTpl}.


getrow(I, #qm{shape = {NR, _NC}, tpl = Tpl})
        when (1 =< I) andalso (I =< NR) ->
    RowTpl = erlang:element(I, Tpl),
    qv:from_tuple(RowTpl).


getcol(J, QM) ->
    getrow(J, transpose(QM)).


transpose(QM = #qm{shape = {NR, NC}, arith_mod = AM}) ->
    NewShape = {NC, NR},
    % -spec map(Fun, qm()) -> qm()
    %     when Fun :: fun((IJPair, Val, ArithMod) -> NewVal),
    Zeros  = zeros(NewShape, AM),
    Mapper =
        fun({I, J}, _Val, _ArithMod) ->
            get({J, I}, QM)
        end,
    map(Mapper, Zeros).


%%%
%%% constructors
%%%

% idn = n x n identity matrix
idn(NR = NC, ArithMod) ->
    Shape = {NR, NC},
    Zeros = zeros(Shape, ArithMod),
    One   = ArithMod:one(),
    MyFun =
        % -spec map(Fun, qm()) -> qm()
        %     when Fun :: fun((IJPair, Val, ArithMod) -> NewVal),
        fun
            ({N, N}, _Val, _ArithMod) ->
                One;
            (_OffDiagonal, Zero, _ArithMod) ->
                Zero
        end,
    map(MyFun, Zeros).



zeros(Shape = {NR, NC}, ArithMod) ->
    Zero = ArithMod:zero(),
    Row  = erlang:make_tuple(NC, Zero),
    Tpl  = erlang:make_tuple(NR, Row),
    #qm{shape     = Shape,
        arith_mod = ArithMod,
        tpl       = Tpl}.


%%%
%%% conversion
%%%
from_lists(Rows = [FirstRow = [FirstElem | _] | _]) ->
    NRows = erlang:length(Rows),
    NCols = erlang:length(FirstRow),
    Shape = {NRows, NCols},
    ArithMod = qanal:arith_mod(FirstElem),
    MyFun =
        fun(RowList) ->
            NCols = erlang:length(RowList),
            erlang:list_to_tuple(RowList)
        end,
    RowTupleList = lists:map(MyFun, Rows),
    Tpl = erlang:list_to_tuple(RowTupleList),
    #qm{shape     = Shape,
        arith_mod = ArithMod,
        tpl       = Tpl}.


to_lists(#qm{tpl = Tpl}) ->
    RowTupleList = erlang:tuple_to_list(Tpl),
    RowListList  = [erlang:tuple_to_list(Row) || Row <- RowTupleList],
    RowListList.


%%%
%%% arrayish
%%%
map(UsrFun, QM = #qm{shape = {NR, NC}, arith_mod = AM}) ->
    % -spec map(Fun, qm()) -> qm()
    %     when Fun :: fun((IJPair, Val, ArithMod) -> NewVal),
    %
    IJPairs = [{I, J} || I <- lists:seq(1, NR),
                         J <- lists:seq(1, NC)],
    ListFoldel =
        fun(IJ, AccQM) ->
            IJPair   = IJ,
            Val      = get(IJ, QM),
            ArithMod = AM,
            NewVal   = UsrFun(IJPair, Val, ArithMod),
            NewAccQM = set(IJ, NewVal, AccQM),
            NewAccQM
        end,
    InitAcc = QM,
    lists:foldl(ListFoldel, InitAcc, IJPairs).



foldl(UsrFun, InitAcc, QM = #qm{shape = {NR, NC}, arith_mod = ArithMod}) ->
    % -spec foldl(Fun, InitAcc, qm()) -> Acc
    %     when Fun :: fun((IJPair, Val, ArithMod, Acc) -> NewAcc),
    IJPairs = [{I, J} || I <- lists:seq(1, NR),
                         J <- lists:seq(1, NC)],
    ArithMod = QM#qm.arith_mod,
    ListFoldel =
        fun(IJ, Acc) ->
            IJPair   = IJ,
            Val      = get(IJ, QM),
            ArithMod = ArithMod,
            NewAcc   = UsrFun(IJPair, Val, ArithMod, Acc),
            NewAcc
        end,
    InitAcc = InitAcc,
    lists:foldl(ListFoldel, InitAcc, IJPairs).

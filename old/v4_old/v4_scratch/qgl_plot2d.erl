-module(qgl_plot2d).

-export_type([
    plotopts/0
]).
-export([
    plot_cspline/2,
    plotopts_def/0,
    plotopts_def/1
]).

-type hq()       :: qxh:hq().
-type plotopts() :: #{xmin := hq(),
                      xmax := hq(),
                      ymin := hq(),
                      ymax := hq(),
                      size := {wh, pos_integer(), pos_integer()}}.

%%%%%%%%%%%
%%% API %%%
%%%%%%%%%%%


-spec plot_cspline([hc()], plotopts()) -> no_return().
% @doc plot a list of complex numbers in a polygonal spline
%
% this is the common way to represent curves


-spec plotopts_def() -> plotopts().
% @doc default plot options

plotopts_def() ->
    #{xmin => whiten(-10),
      xmax => whiten( 10),
      ymin => whiten(-10),
      ymax => whiten( 10),
      size => {wh, 500, 500}}.



-spec plotopts_def(map()) -> plotopts().
% @doc merge the given map over plotopts_def/0

plotopts_def(Undef) ->
    maps:merge(plotopts_def(), Undef).



%%%%%%%%%%%%%%%%%
%%% internals %%%
%%%%%%%%%%%%%%%%%

whiten(X) ->
    qanal:whiten(X).

% @doc
%       Every operation, calculation, and concept, no matter how
%       arbitrarily complex, reduces to adding integers together.
%       There are no new concepts in QAnal. Everything is just
%       putting lipstick on adding integers together.
%
%   -- Dr. Ajay Kumar PHD, Founder of QAnal
%
% qx_sd: [Q]Anal [X]Anal [S]imple [D]ual numbers.
%
% This is the data structure we use for simple differentiation
% (Newton's method).
-module(qx_sd).

-compile(export_all).
% -export_type([sd/0, components/0]).
% -export([scalar/1, epsilon/1,
%          scalar_plus_epsilon/1, spe/1,
%          cs/1, component_scalar/1,
%          ce/1, component_epsilon/1,
%          components/1,
%          fmap/2,
%          arith_mod/1, one_of_sd/1, zero_of_sd/1,
%          additive_inverse/1, conj/1, one/1, plus/2, times/2, zero/1,
%          ai/1, minus/2, pow/2, prod/1, sq/1, sum/1]).

-type arith_mod() :: atom().
-type m()  :: qx_m:m().
-type sd() :: m().
-type qanal() :: qanal:qanal().

% se = scalar, epsilon
-type components() :: {se, scalar(), epsilon()}.
-type scalar()  :: qanal().
-type epsilon() :: qanal().

-spec scalar(scalar()) -> sd().
-spec epsilon(arith_mod()) -> sd().
-spec scalar_plus_epsilon(scalar()) -> sd().
-spec spe(scalar()) -> sd().

%% constructors

scalar(X) ->
    AM = qanal:arith_mod(X),
    Zero = AM:zero(),
    qx_m:from_row_lists([[   X, Zero],
                         [Zero,    X]]).

epsilon(ArithMod) ->
    Zero = ArithMod:zero(),
    One  = ArithMod:one(),
    qx_m:from_row_lists([[Zero, Zero],
                         [ One, Zero]]).


scalar_plus_epsilon(X) ->
    AM  = qanal:arith_mod(X),
    S   = scalar(X),
    E   = epsilon(AM),
    SPE = plus(S, E),
    SPE.

spe(X) ->
    scalar_plus_epsilon(X).

%% accessors
cs(X) ->
    component_scalar(X).

component_scalar(SD) ->
    qx_m:rcth({rc, 1, 1}, SD).

ce(X) ->
    component_epsilon(X).

component_epsilon(SD) ->
    qx_m:rcth({rc, 2, 1}, SD).

-spec components(sd()) -> components().
components(SD) ->
    {se, component_scalar(SD), component_epsilon(SD)}.


%% arithmetic

-spec plus(sd(), sd()) -> sd().
plus(X, Y) ->
    qx_m:plus(X, Y).

% 
% % this also works for matrices because block multiplication
% -spec fmap(Fun, Args) -> components()
%             when Fun  :: fun(),
%                  Args :: [qanal()].
% % @doc
% % write the function using the primitives in this module
% % apply it to scalar arguments
% % will return {se, Result, Derivative}
% fmap(Fun, Args) ->
%     SDArgs = [scalar_plus_epsilon(Arg) || Arg <- Args],
%     SDResult = apply(Fun, SDArgs),
%     components(SDResult).
% 
% helpers
-spec arith_mod(sd()) -> arith_mod().
arith_mod(SD) ->
    qx_m:arith_mod(SD).

-spec one_of_sd(sd()) -> qanal().
one_of_sd(SD) ->
    AM = qanal:arith_mod(SD),
    AM:one().

-spec zero_of_sd(sd()) -> qanal().
zero_of_sd(SD) ->
    AM = qanal:arith_mod(SD),
    AM:zero().
% 
% % ring
% % adapt manually
% % -include("qanal_ring_derive.hrl").
% 
% -spec additive_inverse(sd()) -> sd().
% additive_inverse(SDMatrix) ->
%     %-spec qx_m:map(Fun, m()) -> m()
%     %        when Fun      :: fun((RC, Val, ArithMod) -> NewVal),
%     %             RC       :: rc(),
%     %             Val      :: term(),
%     %             ArithMod :: arith_mod(),
%     %             NewVal   :: term().
%     Mapper =
%         % take additive inverse of each element
%         fun(_RC, Val, ArithMod) ->
%             ArithMod:ai(Val)
%         end,
%     Result = qx_m:map(Mapper, SDMatrix),
%     Result.
% 
% 
% -spec conj(sd()) -> sd().
% conj(SDMatrix) ->
%     Mapper = fun(_RC, Val, ArithMod) -> ArithMod:conj(Val) end,
%     qx_m:map(Mapper, SDMatrix).
% 
% 
% one(ArithMod) ->
%     scalar(ArithMod:one()).
% 
% 
% plus(L, R) ->
%     qx_m:plus(L, R).
% 
% -spec times(sd(), sd()) -> sd().
% times(L, R) ->
%     qx_m:times(L, R).
% 
% zero(ArithMod) ->
%     scalar(ArithMod:zero()).
% 
% ai(X) ->
%     additive_inverse(X).
% 
% minus(L, R) ->
%     qx_m:minus(L, R).
% 
% % @doc
% -spec pow(sd(), non_neg_integer()) -> sd().
% pow(SD, N) ->
%     One = one_of_sd(SD),
%     pow(SD, N, scalar(One)).
% 
% 
% -spec pow(qx_m:m(), non_neg_integer(), qx_m:m()) -> sd().
% pow(_SD, 0, Accum) ->
%     Accum;
% pow(SD, N, Accum) when is_integer(N), 1 =< N ->
%     NewAccum = times(SD, Accum),
%     pow(SD, N-1, NewAccum).
% 
% prod(List = [SDFirst | _]) ->
%     One = one_of_sd(SDFirst),
%     SD_mult_identity = scalar(One),
%     prod(List, SD_mult_identity).
% 
% prod([], Accum) ->
%     Accum;
% prod([X | Rest], Accum) ->
%     % order maybe matters here because matrices
%     NewAccum = times(Accum, X),
%     prod(Rest, NewAccum).
% 
% sq(X) ->
%     times(X, X).
% 
% sum(List = [SDFirst | _]) ->
%     Zero = zero_of_sd(SDFirst),
%     SD_add_identity = scalar(Zero),
%     sum(List, SD_add_identity).
% 
% sum([], Accum) ->
%     Accum;
% sum([X | Rest], Accum) ->
%     NewAccum = plus(Accum, X),
%     sum(Rest, NewAccum).

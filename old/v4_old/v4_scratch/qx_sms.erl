% @doc
% signed multisets; these are the data structure underlying
% dimensions.
-module(qx_sms).

-export_type([
    sms/0,
    sms/1
]).
-export([
    collapse/1,
    from_list/1,
    minus/1,
    minus/2,
    plus/1,
    plus/2,
    singleton/1,
    times/2,
    zero/0
]).

-type sms()  :: [{any(), integer()}].
-type sms(T) :: [{T, integer()}].


%%% API

-spec collapse([{Key, Mult}]) -> sms(Key)
        when Mult :: integer().
% @doc
% collapse a list of key, mult into a single sms

collapse(KMPairs) ->
    collapse(KMPairs, #{}).



-spec from_list([X]) -> sms(X).
% @doc form a signed multiset from a list, counting each element the
% number of times it occurs in the list

from_list(List) ->
    Mult1 = fun(Item) -> {Item, 1} end,
    collapse(lists:map(Mult1, List)).



-spec minus(sms(X)) -> sms(X).
% @doc additive inverse of a signed multiset (flip the signs)

minus(SMS) ->
    SignFlip =
        fun({Key, Mult}) ->
            {Key, -1*Mult}
        end,
    lists:map(SignFlip, SMS).



-spec minus(sms(X), sms(X)) -> sms(X).
% @doc
% subtract the second multiset from the first

minus(X, Y) ->
    plus(X, minus(Y)).



-spec plus([sms(X)]) -> sms(X).
% @doc
% add a list of signed multisets

plus(SMSs) ->
    collapse(lists:flatten(SMSs)).



-spec plus(sms(X), sms(X)) -> sms(X).
% @doc
% add two signed multisets

plus(X, Y) ->
    plus([X, Y]).



-spec singleton(X) -> sms(X).
% @doc
% signed multiset with X => 1

singleton(X) ->
    from_list([X]).


-spec times(sms(X), sms(x)) -> sms(X).
% @doc
% product of two multisets

times(L, R) ->
    MapL = maps:from_list(L),
    MapR = maps:from_list(R),
    % Fold over the items in the left multiset, take the product of
    % its occurance on the left with its occurance on the right. Any
    % element that does not occur in the left will also not occur in
    % the product.
    %
    % Stuff all these into an accumulator. Then pass the result
    % through collapse/1, which will sanitize the result (taking care
    % of 0-occurances, and sorting).
    Fold =
        fun(ThisElem, OccuranceL, AccumSMSList) ->
            OccuranceR = maps:get(ThisElem, MapR, 0),
            ThisOccurance = OccuranceL * OccuranceR,
            ThisPair = {ThisElem, ThisOccurance},
            NewAccumSMSList = [ThisPair | AccumSMSList],
            NewAccumSMSList
        end,
    SMSList = maps:fold(Fold, [], MapL),
    SMSResult = collapse(SMSList),
    SMSResult.



-spec zero() -> sms().
% @doc the null multiset

zero() ->
    [].



%%% INTERNALS

-spec collapse(SMSParts, Accum) -> Result
        when
            SMSParts     :: [{Key, Multiplicity}],
            Key          :: any(),
            Multiplicity :: integer(),
            Accum        :: #{Key := Multiplicity},
            Result       :: sms().
% @doc
% loop through the list of {Key, Multiplicity}, keep a running total
% of each key and its multiplicity (deleting from the map if the
% multiplicity is 0), in a map
%
% at the end of the list, convert the map back to a proplist (signed
% multiset), and sort it

collapse([], Map) ->
    lists:sort(maps:to_list(Map));
collapse([{Key, OldMultiplicity} | Rest], Map) ->
    MapMultiplicity    = maps:get(Key, Map, 0),
    NewMapMultiplicity = OldMultiplicity + MapMultiplicity,
    NewMap =
        case NewMapMultiplicity of
            0        -> maps:remove(Key, Map);
            _NotZero -> maps:put(Key, NewMapMultiplicity, Map)
        end,
    collapse(Rest, NewMap).

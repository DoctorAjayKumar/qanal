% @doc
% Matrices represented as a record, includes the shape, and a map
% mapping row-column pairs to scalars. Matrices are assumed to be
% sparse, so only nonzero entries are stored.
%
% This module only assumes that the scalars form a [QAnal ring].
% Specifically, this means that the scalar arithmetic module obeys
% the qanal_gen_ring behavior.
%
% This module may subtly assume that scalar multiplication is
% commutative. I have not checked the logic carefully.
%
% Gaussian elimination (different module) assumes the scalar
% arithmetic is a [field], which differs from a commutative ring in
% that fields have division.
%
% Note that qx_m does NOT follow the qanal_gen_ring behavior, due to
% the shape ambiguity in defining sum and product of empty lists.
% @end
-module(qx_m).

-export_type([
    idx1/0,
    m/0,
    %m/1,
    rcqmap/0,
    rc/0,
    shape/0
]).
-export([
    cth/2,
    dot/2,
    fold/3,
    fold_sparse/3,
    hc/1,
    idn/1,
    idn/2,
    map/2,
    map_sparse/2,
    minus/1,
    minus/2,
    plus/2,
    plus/1,
    rcput/3,
    rcqmap/1,
    rcth/2,
    rth/2,
    scalar_mod/1,
    shape/1,
    slice/3,
    smush_h/1,
    smush_h/2,
    smush_v/1,
    smush_v/2,
    times/1,
    times/2,
    trace/1,
    transpose/1,
    zeros/1,
    zeros/2
]).

-record(m,
        {shape             :: shape(),
         rcqmap     = #{}  :: rcqmap(),
         scalar_mod = qx_q :: scalar_mod()}).

-type idx1()       :: pos_integer().
-type m()          :: #m{}.
%-type m(X)         :: #m{
-type scalar()     :: any().
-type scalar_mod() :: atom().
-type rcqmap()     :: #{rc() := scalar()}.
-type rc()         :: {rc, idx1(), idx1()}.
-type shape()      :: {shape, pos_integer(), pos_integer()}.


%%% api

-spec cth(idx1(), m()) -> m().
% @doc
% Grab a column out of a matrix
%
% Arithmetic neutral
% @end

cth(C, #m{shape = {shape, NR, NC}, rcqmap = RCQMap}) when 1 =< C, C =< NC ->
    NewShape = {shape, NR, 1},
    IsColC =
        fun
            ({rc, _Row, Col}, _EntryQ) when Col =:= C -> true;
            (_RC, _Entry)                             -> false
        end,
    NewRCQMap = maps:filter(IsColC, RCQMap),
    #m{shape  = NewShape,
       rcqmap = NewRCQMap}.



-spec default_scalar_mod() -> qx_q.
% @doc the default scalar module is the rationals

default_scalar_mod() ->
    qx_q.



-spec dot(Row :: m(), Col :: m()) -> scalar().
% @doc
% take the standard inner product (dot product) of a row matrix with
% a column matrix
%
% Uses scalar functions:
%   - plus/2
%   - times/2
%   - zero/0
% @end

% an advantage of the sparse representation is that zero entries in
% the row vector are naturally skipped when taking the inner product
dot(_Row = #m{shape = {shape, 1, Matches}, rcqmap = RowRCQMap, scalar_mod = ScalarMod},
    Col  = #m{shape = {shape,    Matches, 1}}) ->
    Fold =
        fun(_RCL = {rc, 1, Idx1}, EntryL_Q, Accum_Q) ->
            RCR        = {rc, Idx1, 1},
            EntryR_Q   = rcth(RCR, Col),
            Summand    = ScalarMod:times(EntryL_Q, EntryR_Q),
            NewAccum_Q = ScalarMod:plus(Accum_Q, Summand),
            NewAccum_Q
        end,
    maps:fold(Fold, ScalarMod:zero(), RowRCQMap).



-spec fold(Fun, InitAcc, Matrix) -> FinalAccum
    when Fun        :: fun((rc(), scalar(), scalar_mod(), Acc) -> NewAcc),
         InitAcc    :: any(),
         Acc        :: any(),
         NewAcc     :: any(),
         Matrix     :: m(),
         FinalAccum :: any().
% @doc
% Non-sparse fold.
%
% Folds left-to-right, top-to-bottom, over all entries, including
% zero entries.
% @end

fold(UsrFun, InitAcc, Matrix) ->
    do_fold({rc, 1, 1}, UsrFun, InitAcc, Matrix).

% At the end of the last row
do_fold(ThisRC = {rc, NR, NC}, UsrFun, Accum,
        Matrix = #m{shape = {shape, NR, NC}}) ->
    FinalAccum = UsrFun(ThisRC, rcth(ThisRC, Matrix), scalar_mod(Matrix), Accum),
    FinalAccum;
% At the end of the row
do_fold(ThisRC = {rc, R, NC}, UsrFun, Accum,
        Matrix = #m{shape = {shape, _NR, NC}}) ->
    NewAccum = UsrFun(ThisRC, rcth(ThisRC, Matrix), scalar_mod(Matrix), Accum),
    % go to beginning of next row
    do_fold({rc, R + 1, 1}, UsrFun, NewAccum, Matrix);
% General case
do_fold(ThisRC = {rc, R, C}, UsrFun, Accum, Matrix) ->
    NewAccum = UsrFun(ThisRC, rcth(ThisRC, Matrix), scalar_mod(Matrix), Accum),
    % go to next column, same row
    do_fold({rc, R, C + 1}, UsrFun, NewAccum, Matrix).



-spec fold_sparse(Fun, InitAcc, Matrix) -> FinalAccum
    when Fun        :: fun((rc(), scalar(), scalar_mod(), Acc) -> NewAcc),
         InitAcc    :: any(),
         Acc        :: any(),
         NewAcc     :: any(),
         Matrix     :: m(),
         FinalAccum :: any().
% @doc
% Like fold/4, except this uses maps:fold/3 under the hood, so it's
% faster. There is no guarantee what order the elements will appear
% in. If you need that guarantee, use fold/4 or write your own
% function.
% @end

fold_sparse(UsrFun, UsrInitAcc, Matrix) ->
    % From 'man maps':
    %
    %   fold(Fun, Init, Map) -> Acc
    %
    %          Types:
    %
    %             Fun = fun((K, V, AccIn) -> AccOut)
    %             Init = Acc = AccIn = AccOut = term()
    %             Map = map()
    %             K = V = term()
    RCQMap = rcqmap(Matrix),
    ScalarMod = scalar_mod(Matrix),
    OurFun =
        fun(RC, Scalar, UsrAcc) ->
            NewUsrAcc = UsrFun(RC, Scalar, ScalarMod, UsrAcc),
            NewUsrAcc
        end,
    maps:fold(OurFun, UsrInitAcc, RCQMap).



-spec hc(m()) -> m().
% @doc
% Hermitian Conjugate (conjugate transpose)
% @end

hc(M) ->
    Mapper =
        fun(_RC, Scalar, ScalarMod) ->
            ScalarMod:conj(Scalar)
        end,
    map_sparse(Mapper, M).



-spec idn(NumRows :: pos_integer()) -> Matrix :: m().
% @doc
% Creates NxN identity matrix with scalar arithmetic
% default_scalar_mod().
% @end

idn(NumRows) ->
    idn(NumRows, default_scalar_mod()).



-spec idn(NumRows :: pos_integer(), ScalarMod :: scalar_mod()) -> Matrix :: m().
% @doc
% Creates NxN identity matrix with given scalar arithmetic module
% @end

idn(NumRows, Ring) when 1 =< NumRows ->
    mkidn(1, NumRows, Ring:one(), zeros({shape, NumRows, NumRows}, Ring)).


mkidn(N, N, One, Accum) ->
    rcput({rc, N, N}, One, Accum);
mkidn(R, NR, One, Accum) ->
    mkidn(R+1, NR, One, rcput({rc, R, R}, One, Accum)).


-spec map(Fun, Matrix) -> Matrix
    when Fun    :: fun((rc(), scalar(), scalar_mod()) -> scalar()),
         Matrix :: m().
% @doc
% Non-sparse map. Goes left-to-right, top-to-bottom.
% @end


map(UsrFun, InitMatrix) ->
    OurFun =
        fun(RC, Scalar, ScalarMod, Accum_M) ->
            NewScalar = UsrFun(RC, Scalar, ScalarMod),
            NewAccum_M = rcput(RC, NewScalar, Accum_M),
            NewAccum_M
        end,
    fold(OurFun, InitMatrix, InitMatrix).



-spec map_sparse(Fun, Matrix) -> Matrix
    when Fun    :: fun((rc(), scalar(), scalar_mod()) -> scalar()),
         Matrix :: m().
% @doc
% Sparse map. No guarantee of order
% @end

map_sparse(UsrFun, InitMatrix) ->
    OurFun =
        fun(RC, Scalar, ScalarMod, Accum_M) ->
            NewScalar = UsrFun(RC, Scalar, ScalarMod),
            NewAccum_M = rcput(RC, NewScalar, Accum_M),
            NewAccum_M
        end,
    fold_sparse(OurFun, InitMatrix, InitMatrix).



-spec minus(m()) -> m().
% @doc
% Additive inverse of matrix
%
% Uses scalar functions:
%   - minus/1
% @end

minus(M = #m{rcqmap = RCQMap, scalar_mod = ScalarMod}) ->
    Minus =
        fun(_RC, Entry_Q) ->
            NewEntry_Q = ScalarMod:minus(Entry_Q),
            NewEntry_Q
        end,
    NewRCQMap = maps:map(Minus, RCQMap),
    M#m{rcqmap = NewRCQMap}.



-spec minus(m(), m()) -> m().
% @doc
% Subtract two matrices
%
% Uses scalar functions:
%   - minus/1
% @end

minus(X, Y) ->
    plus(X, minus(Y)).



-spec plus([m(), ...]) -> m().
% @doc
% Add a nonempty list of matrices (nonempty to disambiguate
% shape)
%
% Uses scalar functions:
%   - plus/2
% @end

plus([X]) ->
    X;
plus([X, Y | Rest]) ->
    plus([plus(X, Y) | Rest]).



-spec plus(m(), m()) -> m().
% @doc
% Add two matrices with the same shape.
%
% Uses scalar functions:
%   - plus/2
% @end

plus(_ML = #m{shape = ShapeMatches, rcqmap = RCQMapL, scalar_mod = ScalarMod},
     MR  = #m{shape = ShapeMatches}) ->
    Fold =
        fun(RC, EntryL_Q, AccumR_M) ->
            EntryR_Q = rcth(RC, AccumR_M),
            NewEntryR_Q = ScalarMod:plus(EntryL_Q, EntryR_Q),
            NewAccumR_M = rcput(RC, NewEntryR_Q, AccumR_M),
            NewAccumR_M
        end,
    maps:fold(Fold, MR, RCQMapL).



-spec rcput(rc(), scalar(), m()) -> m().
% @doc
% rcput(RC, Q, M): put the value Q at RC in M
%
% No scalar functions
% @end

% on the case where we're putting zero, we're deleting the entry
% associated with the key
rcput(RC  = {rc, R, C}, _Q0 = {q, 0, 1}, M = #m{shape = {shape, NR, NC}, rcqmap = RCQMap})
        when 1 =< R, R =< NR,
             1 =< C, C =< NC
        ->
    NewRCQMap = maps:remove(RC, RCQMap),
    NewM      = M#m{rcqmap = NewRCQMap},
    NewM;
% on the case where we're putting something other zero, we're
% setting the entry
rcput(RC = {rc, R, C}, Q, M  = #m{shape = {shape, NR, NC}, rcqmap = RCQMap})
        when 1 =< R, R =< NR,
             1 =< C, C =< NC
        ->
    NewRCQMap = RCQMap#{RC => Q},
    NewM      = M#m{rcqmap = NewRCQMap},
    NewM.



-spec rcqmap(m()) -> rcqmap().
% @doc
% Returns the map that maps rc pairs to scalars.
%
% Named rcqmap because originally this was just matrices over
% rationals, until I figured out the right way to generalize the
% arithmetic.
% @end

rcqmap(#m{rcqmap = X}) ->
    X.



-spec rcth(rc(), m()) -> scalar().
% @doc
% Get the entry at {rc, Row, Column}
%
% Uses scalar functions:
%   - zero/0
% @end

rcth(RC = {rc, R, C}, #m{shape = {shape, NR, NC}, rcqmap = RCQMap, scalar_mod = ScalarMod})
            when 1 =< R, R =< NR,
                 1 =< C, C =< NC
            ->
    maps:get(RC, RCQMap, ScalarMod:zero()).



-spec rth(idx1(), m()) -> m().
% @doc
% Grab a row out of a matrix
%
% No scalar functions
% @end

rth(R, #m{shape = {shape, NR, NC}, rcqmap = RCQMap}) when 1 =< R, R =< NR ->
    NewShape = {shape, 1, NC},
    IsRowR =
        fun
            ({rc, Row, _Col}, _Entry_Q) when Row =:= R -> true;
            (_, _)                                     -> false
        end,
    NewRCQMap = maps:filter(IsRowR, RCQMap),
    #m{shape  = NewShape,
       rcqmap = NewRCQMap}.



-spec scalar_mod(m()) -> scalar_mod().
% @doc
% Returns the module for scalar arithmetic
% @end

scalar_mod(#m{scalar_mod = X}) ->
    X.



-spec shape(m()) -> shape().
% @doc
% Returns the shape of the matrix
% @end

shape(#m{shape = X}) ->
    X.



-spec slice(Matrix, {rows, StartRow_I1, EndRow_I1}, {cols, StartCol_I1, EndCol_I1}) -> NewMatrix
    when Matrix      :: m(),
         StartRow_I1 :: idx1(),
         EndRow_I1   :: idx1(),
         StartCol_I1 :: idx1(),
         EndCol_I1   :: idx1(),
         NewMatrix   :: m().
% @doc
% Take a slice out of a matrix, and redo the row-column indexing.
% @end

slice(M = #m{shape = {shape, NR, NC}, scalar_mod = Ring}, {rows, StartRow_I1, EndRow_I1}, {cols, StartCol_I1, EndCol_I1})
        when 1 =< StartRow_I1, StartRow_I1 =< EndRow_I1, EndRow_I1 =< NR,
             1 =< StartCol_I1, StartCol_I1 =< EndCol_I1, EndCol_I1 =< NC ->
    % The width will be at least 1, plus the difference between the
    % widths; ditto for height
    NewNR = 1 + (EndRow_I1 - StartRow_I1),
    NewNC = 1 + (EndCol_I1 - StartCol_I1),
    Zeros = zeros({shape, NewNR, NewNC}, Ring),
    % Differences in index
    RowDiff_I1 = StartRow_I1 - 1,
    ColDiff_I1 = StartCol_I1 - 1,
    % Map over the new matrix (zeros), grabbing the appropriate RC
    % pair out of the old matrix
    Mapper =
        fun({rc, R_New_I1, C_New_I1}, _Zero, _ScalarMod) ->
            R_Old_I1 = R_New_I1 + RowDiff_I1,
            C_Old_I1 = C_New_I1 + ColDiff_I1,
            rcth({rc, R_Old_I1, C_Old_I1}, M)
        end,
    map(Mapper, Zeros).



-spec smush_h([m(), ...]) -> m().
% @doc
% Smush a nonempty list of matrices together horizontally. Nonempty
% to disambiguate shape.
% @end

smush_h([M]) ->
    M;
smush_h([M1, M2 | Rest]) ->
    smush_h([smush_h(M1, M2) | Rest]).



-spec smush_h(m(), m()) -> m().
% @doc
% Smush two matrices together horizontally. Two matrices must have
% the same number of rows, and the same scalar module.
% @end

smush_h(ML = #m{shape = {shape, NRMatches, NC_L}, scalar_mod = SameScalarMod},
        MR = #m{shape = {shape, NRMatches, NC_R}, scalar_mod = SameScalarMod}) ->
    NewShape = {shape, NRMatches, NC_L + NC_R},
    % This is a hack, it takes the left matrix, and changes its
    % shape. Then does a map which replaces anything with a column
    % index greater than NC_L with the correct value on the right
    NewM_Init = ML#m{shape = NewShape},
    % (NC_L + 1) - ColDiff should equal 1
    ColDiff = NC_L,
    Mapper =
        fun
            % Leave values from the left alone
            ({rc, _R, C}, Val_L, _ScalarMod) when 1 =< C, C =< NC_L ->
                Val_L;
            % Grab values from the right
            ({rc, R, C}, _Zero, _ScalarMod) when NC_L < C ->
                % The index of the value we want from the right
                % matrix
                RC_R = {rc, R, C - ColDiff},
                % Get it
                Val_R = rcth(RC_R, MR),
                % return it
                Val_R
        end,
    map(Mapper, NewM_Init).



-spec smush_v([m(), ...]) -> m().
% @doc
% Smush a nonempty list of matrices together horizontally. Nonempty
% to disambiguate shape.
% @end

smush_v([M]) ->
    M;
smush_v([M1, M2 | Rest]) ->
    smush_v([smush_v(M1, M2) | Rest]).



-spec smush_v(m(), m()) -> m().
% @doc
% Smush two matrices together vertically. Two matrices must have
% the same number of columns, and the same scalar module.
% @end

smush_v(M_Top, M_Bot) ->
    % Just take the transpose of the two, smush them horizontally,
    % then take the transpose of that
    %
    % Example:
    %
    %   (smv [1 2]
    %        [3 4])
    % = (transpose (smh [1;
    %                    2]
    %                   [3;
    %                    4]))
    % = (transpose [1 3;
    %               2 4])
    % = [1 2;
    %    3 4]
    transpose(smush_h(transpose(M_Top), transpose(M_Bot))).



-spec times([m(), ...]) -> m().
% @doc
% Multiply a nonempty list of matrices (nonempty to disambiguate
% shape)
%
% Uses scalar functions:
%   - plus/2,
%   - times/2,
%   - zero/0
% @end

times([Head | Rest]) ->
    Foldel =
        fun(Matrix, Accum_M) ->
            % Important to multiply incoming matrix by the
            % accumulator ON THE RIGHT, as matrix multiplication is
            % not commutative
            NewAccum_M = times(Accum_M, Matrix),
            NewAccum_M
        end,
    lists:foldl(Foldel, Head, Rest).



-spec times(m(), m()) -> m().
% @doc
% Multiply two matrices
%
% Uses scalar functions:
%   - plus/2,
%   - times/2,
%   - zero/0
% @end

times(ML = #m{shape = {shape, NR, Matches}},
      MR = #m{shape = {shape,     Matches, NC}}) ->
    NewShape = {shape, NR, NC},
    Zeros    = zeros(NewShape),
    times_iter(ML, MR, {rc, 1, 1}, {shape, NR, NC}, Zeros).



-spec trace(m()) -> scalar().
% @doc
% Trace of a matrix = sum of its diagonal entries
%
% Uses scalar functions:
%   - plus/2
%   - zero/0
% @end

trace(#m{rcqmap = RCQMap, scalar_mod = ScalarMod}) ->
    Fold =
        fun
            (_DiagRC = {rc, Same, Same}, Entry_Q, Accum_Q) ->
                NewAccum_Q = ScalarMod:plus(Entry_Q, Accum_Q),
                NewAccum_Q;
            (_OffDiagRC, _Entry_Q, Accum_Q) ->
                Accum_Q
        end,
    maps:fold(Fold, ScalarMod:zero(), RCQMap).



-spec transpose(m()) -> m().
% @doc
% Transpose of a matrix
%
% No scalar functions.
% @end

transpose(#m{shape  = {shape, NR, NC}, rcqmap = RCQMap}) ->
    NewShape = {shape, NC, NR},
    % works by folding over the RCQMap, forming a new one with
    % row/column swapped
    Fold =
        fun({rc, R, C}, Entry_Q, Accum_RCQMap) ->
            % Swap row/column
            NewRC = {rc, C, R},
            % construct new map by putting in entry in accumulator
            NewAccum_RCQMap = maps:put(NewRC, Entry_Q, Accum_RCQMap),
            NewAccum_RCQMap
        end,
    NewRCQMap = maps:fold(Fold, maps:new(), RCQMap),
    #m{shape = NewShape, rcqmap = NewRCQMap}.



-spec zeros(Shape :: shape()) -> m().
% @doc
% Make matrix of zeros of a given shape, assuming scalar arithmetic
% module is default_scalar_mod().
%
% No scalar functions.
% @end

zeros(Shape) ->
    zeros(Shape, default_scalar_mod()).



-spec zeros(Shape :: shape(), ScalarMod :: atom()) -> m().
% @doc
% Make matrix of zeros of a given shape, with given scalar arithmetic
% module.
%
% No scalar functions.
% @end

zeros(Shape, ScalarMod) ->
    #m{shape = Shape, scalar_mod = ScalarMod}.


%%% internals

-spec times_iter(ML, MR, RC, Shape, Accum) -> Result
            when ML :: m(),
                 MR :: m(),
                 RC :: rc(),
                 Shape :: shape(),
                 Accum :: m(),
                 Result :: Accum.
% @private
% the iterator that matrix multiplication uses
% @end

% loop left-to-right, top-to-bottom
% end of rows and end of column
times_iter(MatrixL_M, MatrixR_M, RC = {rc, NR_Z, NC_Z}, {shape, NR_Z, NC_Z}, Accum_M) ->
    % Rth-row on the left, dot the C-th column on the right
    RowL_M       = rth(NR_Z, MatrixL_M),
    ColR_M       = cth(NC_Z, MatrixR_M),
    Entry_Q      = dot(RowL_M, ColR_M),
    FinalAccum_M = rcput(RC, Entry_Q, Accum_M),
    FinalAccum_M;
% end of column
times_iter(MatrixL_M, MatrixR_M, RC = {rc, R_Z, NC_Z}, Shape = {shape, _NR_Z, NC_Z}, Accum_M) ->
    RowL_M     = rth(R_Z, MatrixL_M),
    ColR_M     = cth(NC_Z, MatrixR_M),
    NewEntry_Q = dot(RowL_M, ColR_M),
    NewRC      = {rc, R_Z + 1, 1},
    NewAccum_M = rcput(RC, NewEntry_Q, Accum_M),
    times_iter(MatrixL_M, MatrixR_M, NewRC, Shape, NewAccum_M);
% not at the end of the column, continue
times_iter(MatrixL_M, MatrixR_M, RC = {rc, R_Z, C_Z}, Shape, Accum_M) ->
    RowL_M     = rth(R_Z, MatrixL_M),
    ColR_M     = cth(C_Z, MatrixR_M),
    Entry_Q    = dot(RowL_M, ColR_M),
    NewRC      = {rc, R_Z, C_Z + 1},
    NewAccum_M = rcput(RC, Entry_Q, Accum_M),
    times_iter(MatrixL_M, MatrixR_M, NewRC, Shape, NewAccum_M).

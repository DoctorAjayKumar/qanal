% @doc
% Gaussian Elimination logic
%
% This is in a different module because it assumes the scalar
% arithmetic is a [field], i.e. it has division.
%
% This implements the "pivot" approach, which has a lot of moving
% parts, but is fairly straightforward.
%
%
% The rules are as follows:
%
%   % determine the next step, and call recursively
%   ge_steps(PivotRow_I1, PivotCol_I1, Matrix = #m{shape = {shape, NR, NC}, scalar_mod = Field}, GSteps_Acc)
%           when 1 =< PivotRow_I1, PivotRow_I1 =< NR,
%                1 =< PivotCol_I1, PivotCol_I1 =< NC ->
%       Pivot_F = qx_m:rcth({rc, PivotRow, PivotCol}, Matrix),
%       PivotCol_M = qx_m:cth(PivotCol, Matrix),
%       Cases:
%       (1) if the pivot column is all zero, then move to the next
%           column, but don't increment the pivot row
%
%               NewPivotRow_I1 = PivotRow_I1,
%               NewPivotCol_I1 = PivotCol_I1 + 1,
%               NewMatrix      = Matrix,
%               NewGSteps_Acc  = GSteps_Acc,
%
%       (2) if the pivot column is not all zero, the pivot is zero, and every entry in the pivot
%       column below the pivot is zero
%
%               NewPivotRow_I1 = PivotRow_I1,
%               NewPivotCol_I1 = PivotCol_I1 + 1,
%               NewMatrix      = Matrix,
%               NewGSteps_Acc  = GSteps_Acc,
%
%       Cases (1) and case (2) can be combined into "is every entry equal to zero provided it is in
%       the pivot column and at or below the pivot row?"
%
%       (3) If the pivot is zero, but there is a nonzero entry in the pivot column below the pivot,
%           do not touch the pivot index, but do swap the two rows
%
%               NewPivotRow_I1 = PivotRow_I1,
%               NewPivotCol_I1 = PivotCol_I1,
%               NewGStep_M     = ge_swapper(NumRows, PivotRow, TargetRow),
%               NewMatrix      = qx_m:times(NewGStep, NewMatrix),
%               NewGSteps_Acc  = [NewGStep | GSteps_Acc]
%
%       (4) If the pivot is not zero, and is not equal to one, do not touch th pivot index, but
%           rescale the row so that the pivot is equal to one
%
%               NewPivotRow_I1 = PivotRow_I1,
%               NewPivotCol_I1 = PivotCol_I1,
%               NewGStep_M     = ge_rescale_row(NumRows, PivotRow_I1, Field:oneover(Pivot)),
%               NewMatrix      = qx_m:times(NewGStep_M, NewMatrix),
%               NewGSteps_Acc  = [NewGStep | GSteps_Acc]
%
%       (5) If the pivot is not zero, and is equal to one, then
%
%               - increment both the pivot row and pivot column
%               - eliminate above and below the pivots
%
%               NewPivotRow_I1 = PivotRow + 1,
%               NewPivotCol_I1 = PivotCol + 1,
%               NewGStep_M     = ge_eliminator(NumRows, PivotRow_I1, PivotCol_M),
%               NewMatrix      = qx_m:times(NewGStep_M, NewMatrix),
%               NewGSteps_Acc  = [NewGStep | GSteps_Acc]
%
%       ge_steps(NewPivotRow, NewPivotCol, NewMatrix, NewGSteps_Acc)
%   % if we're out of rows or columns, we're done.
%   %   - the matrix argument is the reduced-row-echelon form of the original matrix
%   %   - the product of the steps is the matrix that transforms the original matrix into its
%   %     reduced row echelon form
%   %   - in particular, a square matrix is invertible precisely when its RREF is the identity
%   %     matrix, and thus the product of the steps is the inverse matrix
%   ge_steps(PivotRow, PivotCol, Matrix = #m{shape = {shape, NR, NC}}, GSteps_Acc)
%           when (PivotRow > NR) orelse (PivotCol > NC) ->
%       {{rref, Matrix}, {steps, GSteps_Acc}};
% @end
-module(qx_m_ge).

-export_type([
]).
-export([
    ge_steps/1,
    inverse_left/1,
    inverse_right/1,
    rcef/1,
    rref/1
]).

-type m()    :: qx_m:m().
-type idx1() :: pos_integer().

% First step: write a slice function
% DONE

%%% API

-spec ge_step(PivotRow_I1, PivotCol_I1, m()) -> Result
    when PivotRow_I1 :: idx1(),
         PivotCol_I1 :: idx1(),
         Result      :: {{new_pivot_row_idx1, idx1()},
                         {new_pivot_col_idx1, idx1()},
                         {ge_step_m, m()}}.
% @doc
% Figures out the next step in Gaussian elimination, as well as
% correctly updating the pivot indices
% @end

ge_step(PivotRow_I1, PivotCol_I1, M) ->
    {shape, NR, _NC} = qx_m:shape(M),
    Field = qx_m:scalar_mod(M),
    % Let's first check for cases (1)/(2) above; in this case, we
    % just increase the pivot column, and leave the pivot row alone.
    %
    % First step is to slice the matrix along the column from the
    % pivot row down, and see if it's all zeros. If so, we just move
    % on to the next column
    ColSlice = qx_m:slice(M, {rows, PivotRow_I1, NR}, {cols, PivotCol_I1, PivotCol_I1}),
    % Height should be 1 in the case that PivotRowI1 = NR, so
    ZerosNR = NR - PivotRow_I1 + 1,
    ZerosNC = 1,
    Zeros = qx_m:zeros({shape, ZerosNR, ZerosNC}, Field),
    Case12 = ColSlice =:= Zeros,
    % Case 3: case (1 | 2) is false but the pivot is zero
    % In this case we swap with the first nonzero pivot below
    Zero = Field:zero(),
    PivotVal = qx_m:rcth({rc, PivotRow_I1, PivotCol_I1}, M),
    Case3 = PivotVal =:= Zero,
    % Case 4: the pivot is nonzero but isn't one
    % In which case we just rescale
    One = Field:zero(),
    Case4 = PivotVal =/= One,
    % Case 5: eliminate above and below
    Case5 = PivotVal =:= One,
    if
        % Case 1 or 2: increment the column, leave alone
        % This is when we are zero, and everything directly below us
        % is zero. There is no more work to be done on this
        % column, and we can't eliminate. So we move to the next
        % column.
        Case12 ->
            Idn = qx_m:idn(NR, Field),
            {{new_pivot_row_idx1, PivotRow_I1},
             {new_pivot_col_idx1, PivotCol_I1 + 1},
             {ge_step_m, Idn}};
        % Case 3: don't touch the pivot index, generate a swapper
        %
        % Note that our combinators to generate the gauss steps take
        % the same input data as this function, and they figure out
        % what to do.
        %
        % This is when there's a nonzero entry below us, but we are
        % zero.
        Case3 ->
            Swapper = swapper(PivotRow_I1, PivotCol_I1, M),
            {{new_pivot_row_idx1, PivotRow_I1},
             {new_pivot_col_idx1, PivotCol_I1},
             {ge_step_m, Swapper}};
        % Case 4: We have a nonzero pivot, but it's not one. So
        % rescale, don't touch the pivot index.
        Case4 ->
            Rescaler = rescaler(PivotRow_I1, PivotCol_I1, M),
            {{new_pivot_row_idx1, PivotRow_I1},
             {new_pivot_col_idx1, PivotCol_I1},
             {ge_step_m, Rescaler}};
        % Case 5: We have a nonzero pivot, and it's one. Time to
        % eliminate, then move on down a row and down a column
        Case5 ->
            Eliminator = eliminator(PivotRow_I1, PivotCol_I1, M),
            {{new_pivot_row_idx1, PivotRow_I1 + 1},
             {new_pivot_col_idx1, PivotCol_I1 + 1},
             {ge_step_m, Eliminator}}
    % and we're done; exactly one of those cases will be true
    end.


swapper(_, _, _) -> error(nyi).
rescaler(_, _, _) -> error(nyi).
eliminator(_, _, _) -> error(nyi).



-spec ge_steps(m()) -> {{rref, m()}, {steps, [m()]}}.
% @doc
% Return the Row-reduced echelon form of the matrix, as well as the
% steps to obtain it
% @end

ge_steps(M) ->
    ge_steps(1, 1, qx_m:shape(M), M, []).



-spec inverse_left(m()) -> m().
% @doc
% Return the left inverse of the matrix (product of the GE steps). In
% other words, this is defined as the matrix X where given the input
% matrix M, X*M = rref(M).
%
% For an invertible square matrix,
%
%       - its row reduced echelon form is the identity matrix
%       - its column reduced echelon form is the identity matrix
%       - its left inverse is equal to its right inverse
% @end

inverse_left(M) ->
    {{rref, _DontCare}, {steps, GSteps}} = ge_steps(M),
    qx_m:times(GSteps).



-spec inverse_right(m()) -> m().
% @doc
% Return the right inverse of the matrix. In other words, this is
% defined as the matrix X that given the input matrix M, has the
% property that M*X = rcef(M)
%
% This uses the transpose symmetry of linear algebra:
%
%       (transpose (m* X Y)) = (m* (transpose Y) (transpose X))
%
% Note the flip in order.
%
% Thus a sequence of matrices [G1, G2, ..., GN] such that
%
%   (m* M G1 G2 ... GN) = (rcef M)
%
% will also have the property that
%
%     (m* (transpose GN) ... (transpose G2) (transpose G1)
%         (transpose M))
%   = (rref (transpose M))
%
% Thus, by definition, the product (m* (transpose GN) ... (transpose
% G2) (transpose G1)) is equal to (inverse_left (transpose M))
%
% Thus (transpose (inverse_left (transpose M))), by the transpose
% symmetry, will equal (m* G1 G2 ... GN), which is our desired right
% inverse.
%
% So our strategy is
%
%   (inverse_right M) -> (transpose (inverse_left (transpose M)))
% @end

inverse_right(M) ->
    qx_m:transpose(inverse_left(qx_m:transpose(M))).



-spec rcef(m()) -> m().
% @doc
% See the comment on inverse_right for why this works.
%
% (rcef M) -> (transpose (rref (transpose M)))
% @end

rcef(M) ->
    qx_m:transpose(rref(qx_m:transpose(M))).



-spec rref(m()) -> m().
% @doc
% Row reduced echelon form, just pulls the argument out of ge_steps.
% @end

rref(M) ->
    {{rref, RREF}, {steps, _DontCare}} = ge_steps(M),
    RREF.



%%% PRIVATE

-spec ge_steps(PivotRow_I1, PivotCol_I1, {shape, NR, NC}, Matrix, Steps_Acc) -> Result
    when PivotRow_I1 :: idx1(),
         PivotCol_I1 :: idx1(),
         NR          :: pos_integer(),
         NC          :: pos_integer(),
         Matrix      :: m(),
         Steps_Acc   :: [m()],
         Result      :: {{rref, m()}, {steps, [m()]}}.
% @private
% The ge_steps function described in the comment
% @end

% Done case: we're out of rows or columns
ge_steps(PR_I1, PC_I1, {shape, NR, NC}, Matrix, GSteps) when (NR < PR_I1) orelse (NC < PC_I1) ->
    {{rref, Matrix}, {steps, GSteps}};
% Progress case
ge_steps(PR_I1, PC_I1, Shape, M, GSteps_Acc) ->
    % get the step from a separate function, for debugging
    {{new_pivot_row_idx1, NewPR_I1},
     {new_pivot_col_idx1, NewPC_I1},
     {ge_step_m, GEStep_M}} = ge_step(PR_I1, PC_I1, M),
    NewM = qx_m:times(GEStep_M, M),
    NewGSteps_Acc = [GEStep_M | GSteps_Acc],
    ge_steps(NewPR_I1, NewPC_I1, Shape, NewM, NewGSteps_Acc).

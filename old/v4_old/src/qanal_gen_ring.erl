% @doc
% Behavior definition for a [QAnal ring]. The motivation is that
% matrix arithmetic can be generalized over more than one type of
% scalar arithmetic. Scalar arithmetic modules in matrix arithmetic
% are expected to export these functions.
%
% [QAnal ring]s differ from the traditional mathematical definition
% of [ring] in a couple of small ways:
%
%   - QAnal ring elements must have "conjugation"; this is so things
%     like "Hermitian transpose" can be defined.
%
%   - Our language is towards "types" (rather "shapes") instead of
%     "sets". Set theory is joggerlicious.
%
% When you think "ring", think "addition and multiplication". A
% [commutative ring] is distinct from an ordinary [ring] in that an
% ordinary [ring] does not require multiplication to be commutative.
%
% Examples of [commutative ring]s:
%
%   - integers
%   - Z-modules
%   - rationals
%   - complex numbers
%
% Examples of [ring]s that are not commutative:
%
%   - quaternions
%   - square matrices of a fixed size
%
%     Note that the qx_m module does NOT follow this behavior.
%     The reason is that the qanal_gen_ring behavior requires the
%     following two things to be defined:
%
%       - the sum of an empty list (should be zero())
%       - the product of an empty list (should be one())
%
%     The problem with defining those generic matrices is that the
%     shape of the matrix is ambiguous.
%
% Specifically, a commutative ring is a type T with the following
% properties:
%
%   - there is a function plus(T, T) -> T with the following
%     properties:
%
%       - plus is associative; i.e.
%               plus(X, plus(Y, Z)) = plus(plus(X, Y), Z).
%       - plus is commutative; i.e.
%               plus(X, Y) = plus(Y, X)
%
%   - there is a function times(T, T) -> T with the following
%     properties
%
%       - times is associative; i.e.
%               times(X, times(Y, Z)) = times(times(X, Y), Z).
%       - times is commutative; i.e.
%               times(X, Y) = times(Y, X)
%       - times distributes over plus; i.e.
%               times(A, plus(X, Y)) = plus(times(A, X), times(A, Y))
%               times(plus(X, Y), A) = plus(times(X, A), times(Y, A))
%
%   - there is a unique element in the type called zero() with the
%     following properties:
%
%       - zero() behaves as the additive identity:
%               plus(X, zero()) = plus(zero(), X) = X
%       - zero() annihilates multiplication:
%               times(X, zero()) = times(zero(), X) = zero()
%
%     Note the structure of the constraint. The following
%     facts are true:
%
%       - zero() has the two properties listed above
%       - if an element is the additive identity, then it is zero()
%         (this is a consequence of there existing additive inverses)
%       - it is NOT necessarily true that zero is the only element
%         that divides zero. For instance, in clock arithmetic (Z mod
%         12), 3*4 = 0.
%       - if a ring has no zerodivisors, it is called an [integral
%         domain]
%       - all finite integral domains are [field]s. An example of an
%         infinite integral domain that is not a field is the
%         integers.
%
%   - all elements in the type have additive inverses; i.e. there is
%     a function minus(T) -> T, which has the property that
%           plus(X, minus(X)) = zero()
%
%   - there is a unique element in the type called one() which
%     has the following properties:
%
%       - one() behaves as the multiplicative identity:
%               times(X, one()) = times(one(), X) = X
% @end
-module(qanal_gen_ring).


-type r() :: any().



-callback conj(r()) -> r().
% @doc
% Conjugate. Only meaningful for complex numbers and
% quaternions. Should be the identity function otherwise.
% @end



-callback is_valid(any()) -> true | false.
% @doc
% Should return true precisely when the input argument is a valid
% canonical element. For instance, qx_q:is_valid({q, 2, 4}) should be
% false, because {q, 2, 4} is not a reduced fraction.
% @end



-callback minus(r()) -> r().
% @doc
% Should return the additive inverse of the element.
% @end



-callback minus(r(), r()) -> r().
% @doc
% Should be the sum of the first argument and the additive inverse of
% the second.
% @end



-callback one() -> r().
% @doc
% Multiplicative identity
% @end



-callback plus([r()]) -> r().
% @doc
% Sum of a list of elements; empty sum should be zero().
% @end



-callback plus(r(), r()) -> r().
% @doc
% Sum of two elements.
% @end



-callback square(r()) -> r().
% @doc
% Multiply an element by itself.
% @end



-callback times([r()]) -> r().
% @doc
% Product of a list of elements; empty product should be one().
% @end



-callback times(r(), r()) -> r().
% @doc
% Multiply two elements
% @end



-callback zero() -> r().
% @doc
% Additive identity.
% @end




% @doc
% Defines the behavior for [QAnal division algebra]s. These are
% [QAnal ring]s that also have division. If the multiplication
% operation is commutative, then it's called a [field].
%
% qanal_gen_divalg is a sub-behavior of qanal_gen_ring, so any module
% implementing the former should also implement the latter.
%
% The only instance that might come up in practice of a
% non-commutative division algebra is the quaternions
% @end
-module(qanal_gen_divalg).

-type f() :: any().


-callback divide(f(), f()) -> f().
% @doc
% divide(X, Y) should equal times(X, oneover(Y)).
% @end



-callback oneover(f()) -> f().
% @doc
% multiplicative inverse
% @end

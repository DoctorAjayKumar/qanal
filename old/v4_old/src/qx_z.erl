% @doc qx_z = Qanal Xanal Zahlen (= integers)
-module(qx_z).

-behavior(qanal_gen_ring).
-export_type([
    z/0,
    znn/0,
    zp/0
]).
-export([
    conj/1,
    gcd/1,
    gcd/2,
    is_valid/1,
    minus/1,
    minus/2,
    modulo/2,
    one/0,
    plus/1,
    plus/2,
    square/1,
    times/1,
    times/2,
    zero/0
]).

-type z()   :: integer().
-type znn() :: non_neg_integer().
-type zp()  :: pos_integer().

%%% API

-spec conj(z()) -> z().
% @doc
% Here for qanal_gen_ring behavior; is the identity function
% @end

conj(X) ->
    X.



-spec gcd(Ints :: [z()]) -> GCD :: znn().
% @doc gcd of a list of integers

gcd(List) ->
    gcd_accum(List, 0).



-spec gcd(z(), z()) -> znn().
% @doc
% Greatest common divisor of two integers
% @end

% make sure both arguments are non-negative
gcd(X, Y) ->
    gcdabs(abs(X), abs(Y)).



-spec is_valid(any()) -> true | false.
% @doc makes sure its an integer

is_valid(Z) ->
    is_integer(Z).



-spec minus(z()) -> z().
% @doc
% Additive inverse
% @end

minus(Z) ->
    -1 * Z.



-spec minus(z(), z()) -> z().
% @doc subtraction

minus(X, Y) ->
    X - Y.



-spec modulo(A :: z(), B :: zp()) -> AModB :: znn().
% @doc Like the rem operator, except this always returns a result
% within 0..B
%
% for simplicity/obviously-correct-behavior, B must be strictly
% positive

modulo(A, B) when is_integer(A),
                  is_integer(B),
                  0 < B ->
    ARemB = A rem B,
    % one of two cases
    %
    %   -B < ARemB < 0 ->
    %       % this will happen when A < 0
    %       return ARemB + B
    %   0 =< ARemB < B ->
    %       % this will happen when 0 =< A
    %       return ARemB
    NegRem = ((-1 * B) < ARemB) andalso (ARemB < 0),
    PosRem =       (0 =< ARemB) andalso (ARemB < B),
    if NegRem ->
           ARemB + B;
       PosRem ->
           ARemB
    end.



-spec one() -> z().
% @doc the integer 1

one() ->
    1.



-spec plus([z()]) -> z().
% @doc add a list of integers

plus(Zs) ->
    lists:foldl(fun plus/2, zero(), Zs).



-spec plus(z(), z()) -> z().
% @doc add two integers

plus(X, Y) ->
    X + Y.



-spec square(z()) -> z().
% @doc square an integer

square(X) ->
    X * X.



-spec times([z()]) -> z().
% @doc multiply a list of integers

times(Zs) ->
    lists:foldl(fun times/2, one(), Zs).



-spec times(z(), z()) -> z().
% @doc multiply two integers

times(X, Y) ->
    X * Y.



-spec zero() -> z().
% @doc the integer 0

zero() ->
    0.



%%% INTERNALS

-spec gcd_accum(Ints :: [z()], AccumGCD :: znn()) -> GCD :: znn().
% @private
% gcd of a list of integers, with an accumulator. Should set
% accumulator to 0 on initial call, because that's the "identity
% gcd", so to speak:
%
%       gcd(X, 0) -> X;
%       gcd(0, X) -> X;
% @end

% once the accumulator is 1, shortcut
gcd_accum(_List, _GCDAccum = 1) ->
    1;
% end of the list, return the accumulated GCD
gcd_accum([], GCDAccum) ->
    GCDAccum;
% new item in the list, gcd it against the accum, move along
gcd_accum([X | Xs], GCDAccum) ->
    NewGCDAccum = gcd(X, GCDAccum),
    gcd_accum(Xs, NewGCDAccum).



-spec gcdabs(znn(), znn()) -> znn().
% @private gcd where the arguments can be assumed to be non-negative,
% but not necessarily in the correct order (the argument on the left
% should be bigger than or equal to the argument on the right).

gcdabs(X, Y) when X < Y ->
    gcdplus(Y, X);
gcdabs(X, Y) when X >= Y->
    gcdplus(X, Y).



-spec gcdplus(znn(), znn()) -> znn().
% @private gcd where the arguments can be assumed to be non-negative,
% and in the correct order

gcdplus(X, 0) ->
    X;
% X might be equal to Y here, in which case, modulo(X, Y) = 0, and
% the next call will degenerate into the base case
gcdplus(X, Y) when X >= Y ->
    % X = qY + R
    % R = X - qY
    % 0 =< R < Y
    % therefore any common divisor of X and Y (such as the gcd) also
    % divides R
    % therefore
    % gcd(X, Y) ->
    %   gcd(Y, R)
    gcdplus(Y, modulo(X, Y)).

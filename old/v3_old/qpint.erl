% @doc
% qpint: [q]anal [p]reanal [int]egration
%
% integration using polygon splines
%
% will overestimate convex functions and underestimate concave
% functions, but in a predictable manner
%
% just uses qanal:cspline/1 to compute the area. if you feed this
% weird shit you might get weird shit back
-module(qpint).

-export([
    int/3,
    intnpoints/4,
    intpoints/2
]).

-type hq() :: qxh:hq().


-spec int(Fun, Min, Max) -> hq()
        when Fun :: fun((hq()) -> hq()),
             Min :: hq(),
             Max :: hq().

int(Fun, Min, Max) ->
    intnpoints(Fun, Min, Max, 200).



-spec intnpoints(Fun, Min, Max, NPoints) -> hq()
        when Fun     :: fun((hq()) -> hq()),
             Min     :: hq(),
             Max     :: hq(),
             NPoints :: pos_integer().
% @doc NPoints must be at least 2

intnpoints(Fun, Min, Max, NPoints) when 2 =< NPoints ->
    % the idea here is the path
    %
    %   gamma(T, Src, Dest) when 0 =< T, T =< 1 ->
    %       Src + T*(Dest - Src).
    %
    % gives the path from Src to Dest parameterized by T
    %
    % so if there are N points, we want our t points to be Result
    %
    %   2 -> [0,            1];
    %   3 -> [0,    1/2,    1];
    %   4 -> [0, 1/3, 2/3,  1]
    %
    % So if there are N points, then the time step size is (1 / N-1)
    %
    % And the "step" in the call to qxh:seq is... let's just do the
    % calculus
    %
    %   gamma(T, Src, Dest) when 0 =< T, T =< 1 ->
    %       Src + T*(Dest - Src).
    %   dG/dT = (Dest - Src)
    %   dG    = dT * (Dest - Src)
    %
    % so the step size is
    %
    % Step = TimeStep * (Max - Min)
    Src       = Min,
    Dest      = Max,
    TimeStepH = qanal:q(1, NPoints - 1),
    StepH     = qxh:times(TimeStepH, qxh:minus(Dest, Src)),
    {ok, DomPoints} = qxh:seq_safe(Src, Dest, StepH, 1000),
    %ok = io:format("Src       = ~ts~n"
    %               "Dest      = ~ts~n"
    %               "TimeStepH = ~ts~n"
    %               "StepH     = ~ts~n"
    %               "DomPoints = ~n",
    %               [qxh:pf(Src),
    %                qxh:pf(Dest),
    %                qxh:pf(TimeStepH),
    %                qxh:pf(StepH)]),
    %lists:foreach(fun qxh:pp/1, DomPoints),
    %ok.
    intpoints(Fun, DomPoints).



-spec intpoints(Fun, DomainPoints) -> hq()
        when Fun          :: fun((hq()) -> hq()),
             DomainPoints :: [hq()].
% @doc integrate a function using a spline based at domain points
%
% Just a wrapper around qxh:cspline/1

intpoints(Fun, DomainPoints = [FirstSplinePoint | _]) when 2 =< length(DomainPoints) ->
    MkCPoint =
        fun(DomainPoint) ->
            RangePoint = Fun(DomainPoint),
            qanal:c(DomainPoint, RangePoint)
        end,
    FirstSplinePoint   = FirstSplinePoint,
    SecondSplinePoint  = lists:last(DomainPoints),
    % going backwards over the points on the curve, as to get
    % positive orientation
    MiddleSplinePoints = lists:map(MkCPoint, lists:reverse(DomainPoints)),
    LastSplinePoint    = FirstSplinePoint,
    SplinePoints       = [FirstSplinePoint, SecondSplinePoint] ++
                            MiddleSplinePoints ++
                            [LastSplinePoint],
    qxh:cspline(SplinePoints).

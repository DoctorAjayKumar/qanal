% @doc
% multisets
%
% represented as sorted lists to be amenable to pattern matching
-module(qx_ms).

-export_type([
    ms/0,
    ms/1,
    multiset/0,
    multiset/1
]).
-export([
    delete/2,
    elems/1,
    empty/0,
    from_list/1,
    insert/2,
    minus/2,
    pf/2,
    plus/2,
    pp/2,
    singleton/1,
    size/1
]).

-type ms()        :: multiset().
-type ms(X)       :: multiset(X).
-type multiset()  :: [any()].
-type multiset(X) :: [X].


-spec delete(Item :: any(), multiset()) -> multiset().
% @doc
% crashes if try to delete a nonexistent element

delete(Item, MS) ->
    true = lists:member(Item, MS),
    lists:delete(Item, MS).



-spec elems(ms(X)) -> [X].
% @doc
% ordered list of every element that appears in the list

elems(Multiset) ->
    lists:sort(sets:to_list(sets:from_list(Multiset))).



-spec empty() -> ms().
% @doc empty multiset

empty() ->
    [].



-spec from_list([X]) -> ms(X).
% @doc just sorts the list

from_list(List) ->
    lists:sort(List).



-spec insert(any(), ms()) -> ms().
% @doc
% insert an element into the multiset

insert(X, MS) ->
    lists:sort([X | MS]).



-spec minus(ms(), ms()) -> ms().
% @doc
% subtract the second argument from the first

minus(Accum1, [Elem2 | Rest2]) ->
    minus(delete(Elem2, Accum1), Rest2);
minus(Accum1, []) ->
    Accum1.



-spec pf(MS, ItemPF) -> iolist()
        when MS     :: multiset(X),
             ItemPF :: fun((X) -> iolist()).
% @doc pretty format

pf(MS, ItemPF) ->
    ["(ms", [[" ", ItemPF(Item)] || Item <- MS], ")"].



-spec plus(ms(), ms()) -> ms().
% @doc addition of multisets

plus(X, Y) ->
    lists:sort(X ++ Y).



-spec pp(MS, ItemPF) -> ok
        when MS     :: multiset(X),
             ItemPF :: fun((X) -> iolist()).
% @doc pretty print

pp(MS, ItemPF) ->
    ok = io:format("~ts~n", [pf(MS, ItemPF)]),
    ok.



-spec singleton(X) -> ms(X).
% @doc singleton multiset

singleton(X) ->
    [X].



-spec size(ms()) -> non_neg_integer().
% @doc size of the multiset, counting multiplicity

size(MS) ->
    length(MS).

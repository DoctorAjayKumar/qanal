% @doc
% Qanal Xanal PolyNumbers
%
% (polynomials)
%
% single variable for now
%
% represented as variable-size arrays
%
% arrays don't seem quite right for this
%
% I was hoping that when I converted an array back to a list it would
% skip the trailing zeros, but it doesn't, and that's quite annoying
%
% list is better because it's more transparent and I can just write
% code to delete trailing zeros
%
% this idea seems like it works really really well as long as
% epsilon has a concrete interpretation as a one-dimensional error
%
% not sure about with complex coefficients or some such. let's stay
% in the easy world of rationals for now
%
% can very easily add dimensions can't we. hmm
%
% something like
%
% -record(pn,
%         {list        :: [hq()],
%          dim  = []   :: [{atom(), integer()}],
%          name = none :: none | {var, atom()}}).
%
% hmm
%
% anyway, this idea of just following what happens to (x + epsilon)
% is I think the correct approach for PreAnal
%
% exact                 = precise of order 0 = epsilon = 0
% linearly precise      = precise of order 1 = epsilon^2 = 0
% quadratically precise = precise of order 2 = epsilon^3 = 0
% ...
% etc
-module(qxpn).

-export_type([
    pn/0
]).
-export([
    exact/1,
    from_list/1,
    pf/1,
    precise/1,
    pp/1,
    to_list/1
]).

-record(pn,
        {list :: [hq()]}).

-type hq()   :: qxh:hq().
-type pn()   :: #pn{}.


%%%
%%% API
%%%

-spec exact(hq()) -> pn().
% @doc
% "degree"-0 polynumber

exact(HQ) ->
    from_list([HQ]).



-spec from_list([hq()]) -> pn().
% @doc polynumber with coefficients

from_list(List) ->
    DTZList = dtz(List),
    {pn, DTZList}.



-spec pf(pn()) -> iolist().
% @doc pretty format
%
% (pn coeff1 coeff2 coeff3 etc)

pf({pn, Coeffs}) ->
    ["(pn", [[" ", qxh:pf(Coeff)] || Coeff <- Coeffs], ")"].



-spec precise(hq()) -> pn().
% @doc
% "degree"-0 polynumber

precise(HQ) ->
    from_list([HQ, qxh:one()]).



-spec pp(pn()) -> ok.
% @doc pretty print
%
% (pn coeff1 coeff2 coeff3 etc)

pp(PN) ->
    ok = io:format("~ts~n", [pf(PN)]),
    ok.



-spec to_list(pn()) -> [hq()].
% @doc
% get list of coefficients

to_list({pn, List}) ->
    List.



%%%
%%% INTERNALS
%%%

-spec dtz([hq()]) -> [hq()].
% @private
% delete trailing zeros

dtz(List) ->
    dtz_rev(lists:reverse(List)).

dtz_rev([_Zero = {h, _Re = {q, 0, 1},
                     _Im = {q, 0, 1},
                     _Jm = {q, 0, 1},
                     _Km = {q, 0, 1}}
         | Rest]) ->
    dtz_rev(Rest);
dtz_rev(RevList = _DoesntStartWithZero) ->
    lists:reverse(RevList).

% @doc
% qanal xanal monomials
%
% monomial is an ordered list of atoms
%
% do the wfc thing where monomial is assumed to be a product, and
% polynomial is assumed to be a sum
%
% polynomials over rationals for now
-module(qx_mon).

-export_type([
]).
-export([
    atoms/1,
    deg/1,
    mon/2,
    one/0,
    pf/1,
    pp/1,
    scalar/1,
    times/2,
    var/1
]).

-record(mon,
        {coeff :: hq(),
         atoms :: multiset(atom())}).

-type hq()        :: qxh:hq().
-type mon()       :: #mon{}.
-type multiset(X) :: qx_ms:multiset(X).


%%%%%%%%%%%
%%% API %%%
%%%%%%%%%%%

-spec atoms(mon()) -> multiset(atom()).
% @doc
% atoms

atoms(#mon{atoms = Atoms}) ->
    Atoms.



-spec deg(mon()) -> non_neg_integer().
% @doc
% degree of a monomial (= length of its list of atoms)

deg(#mon{atoms = AtomsMS}) ->
    qx_ms:size(AtomsMS).



-spec mon(hq(), [atom()]) -> mon().
% @doc
% make a monominal out of a scalar and a list of atoms

mon(HQ, Atoms) ->
    true  = qxh:is_hq(HQ),
    false = HQ =:= qxh:zero(),
    true  = lists:all(fun is_atom/1, Atoms),
    #mon{coeff = HQ,
         atoms = qx_ms:from_list(Atoms)}.



-spec one() -> mon().
% @doc
% corresponds to scalar(one()).

one() ->
    mon(qxh:one(), []).



-spec pf(mon()) -> iolist().
% @doc
% pretty format

pf(#mon{coeff = C, atoms = AtomsMS}) ->
    AtomPF = fun erlang:atom_to_list/1,
    ["(mon ", qxh:pf(C), " ", qx_ms:pf(AtomsMS, AtomPF), ")"].



-spec pp(mon()) -> ok.
% @doc pretty print

pp(Mon) ->
    ok = io:format("~ts~n", [pf(Mon)]),
    ok.



-spec scalar(hq()) -> mon().
% @doc
% empty monomial where the coefficient is the scalar

scalar(HQ) ->
    mon(HQ, []).



-spec times(mon(), mon()) -> mon().
% @doc
% multiply two monomials

times(#mon{coeff = Coeff1, atoms = Atoms1},
      #mon{coeff = Coeff2, atoms = Atoms2}) ->
    mon(  qxh: times(Coeff1, Coeff2),
        qx_ms:  plus(Atoms1, Atoms2)).



-spec var(atom()) -> mon().
% @doc
% monomial with coefficient 1 where the variable name is given

var(Atom) when is_atom(Atom)->
    mon(qxh:one(), [Atom]).

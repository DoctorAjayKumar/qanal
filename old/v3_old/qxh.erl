% @doc qxh = Qanal Xanal Hamiltonians [= quaternions]
-module(qxh).

-export_type([
    h/0,
    hc/0,
    hi/0,
    hj/0,
    hk/0,
    hq/0,
    hr/0,
    hz/0
]).
-export([
    chip/2,
    chipsum/1,
    ci/1,
    cj/1,
    ck/1,
    conj/1,
    cr/1,
    cspline/1,
    from_hqz/1,
    from_q/1,
    from_z/1,
    h/4,
    i/0,
    ip/2,
    is_hq/1,
    j/0,
    k/0,
    minus/1,
    minus/2,
    one/0,
    oneover/1,
    p1/1,
    pf/1,
    pi/1,
    pj/1,
    pk/1,
    pp/1,
    plus/1,
    plus/2,
    pr/1,
    q/2,
    quadrance/1,
    quadrance_q/1,
    seq/3,
    seq_safe/4,
    times/1,
    times/2,
    zero/0
]).

-type z()  :: qxz:z().
-type q()  :: qxq:q().
-type qz() :: {q, z(), 1}.
-type q0() :: {q, 0, 1}.

-record(h,
        {cr :: q(),
         ci :: q(),
         cj :: q(),
         ck :: q()}).
-type h() :: #h{}.
-type hc() :: {h, q (), q (), q0(), q0()}.
-type hi() :: {h, q0(), q (), q0(), q0()}.
-type hj() :: {h, q0(), q0(), q (), q0()}.
-type hk() :: {h, q0(), q0(), q0(), q ()}.
-type hq() :: {h, q (), q0(), q0(), q0()}.
-type hr() :: {h, q (), q0(), q0(), q0()}.
-type hz() :: {h, qz(), q0(), q0(), q0()}.



%%% API

-spec chip(h(), h()) -> h().
% @doc chip = complex half inner product; one half the conjugate of
% the first times the second
%
% chip(X, Y) -> (1/2) * bar(X) * Y
% ip(X, Y)   -> (1/2) * (bar(X)*Y + bar(Y)*X)

chip(X, Y) ->
    OneHalf_q = qq(1, 2),
    OneHalf_h = from_q(OneHalf_q),
    times([OneHalf_h, conj(X), Y]).



-spec chipsum([h()]) -> h().
% @doc chipsum = complex half inner product sum
%
% takes the sum of complex-half-inner-products of consecutive items
% in the list

chipsum(List) ->
    chipsum_accum(List, zero()).



-spec ci(h()) -> q().
% @doc i-component

ci(#h{ci = X}) ->
    X.



-spec cj(h()) -> q().
% @doc j-component

cj(#h{cj = X}) ->
    X.



-spec ck(h()) -> q().
% @doc k-component

ck(#h{ck = X}) ->
    X.



-spec conj(h()) -> h().
% @doc conjugate of h

conj({h, QR, QI, QJ, QK}) ->
    h(QR, qminus(QI), qminus(QJ), qminus(QK)).



-spec cr(h()) -> q().
% @doc real component

cr(#h{cr = X}) ->
    X.



-spec cspline([hc()]) -> hr().
% @doc take a spline given by a list of complex points, and return
% the area enclosed by the spline
%
% caller is responsible for making sure the spline closes
%
% takes the i-component of the asymmetric product between consecutive
% pairs, and sums that

cspline(List) ->
    pi(chipsum(List)).



-spec from_hqz(h() | q() | z()) -> h().
% @doc typecast number up to a hamiltonian

from_hqz(Z) when is_integer(Z) ->
    from_z(Z);
from_hqz(Q = {q, T, B}) when is_integer(T), is_integer(B) ->
    from_q(Q);
from_hqz(H = {h, _R, _I, _J, _K}) ->
    H.



-spec from_q(q()) -> h().
% @doc convert rational into quaternion

from_q({q, T, B}) when is_integer(T), is_integer(B) ->
    q(from_z(T), from_z(B)).



-spec from_z(z()) -> h().
% @doc convert integer into quaternion

from_z(Z) when is_integer(Z) ->
    Q = qxq:from_z(Z),
    h(Q, q0(), q0(), q0()).



-spec h(q(), q(), q(), q()) -> h().
% @doc make hamiltonian from 4 rationals

h(CR = {q, _, _}, CI = {q, _, _}, CJ = {q, _, _}, CK = {q, _, _}) ->
    {h, CR, CI, CJ, CK}.



-spec i() -> h().
% @doc construct 0 + 1i + 0j + 0k

i() ->
    h(q0(), q1(), q0(), q0()).



-spec ip(h(), h()) -> h().
% @doc
% "inner product"
%
% chip(X, Y) -> (1/2) * conj(X) * Y
% ip(X, Y)   -> (1/2) * [ conj(X)*Y + conj(Y)*X ]

ip(H1, H2) ->
    OneHalf_q = qq(1, 2),
    OneHalf_h = from_q(OneHalf_q),
    times(OneHalf_h,
          plus(times(conj(H1), H2),
               times(conj(H2), H1))).



-spec is_hq(any()) -> boolean().
% @doc
% returns true if argument is a rational quaternion, otherwise
% returns false

is_hq({h, _Re = {q, _, _},
          _Im = {q, 0, 1},
          _Jm = {q, 0, 1},
          _Km = {q, 0, 1}}) ->
    true;
is_hq(_) ->
    false.



-spec j() -> h().
% @doc construct 0 + 0i + 1j + 0k

j() ->
    h(q0(), q0(), q1(), q0()).



-spec k() -> h().
% @doc construct 0 + 0i + 0j + 1k

k() ->
    h(q0(), q0(), q0(), q1()).



-spec minus(h()) -> h().

minus(Y) ->
    minus(zero(), Y).



-spec minus(h(), h()) -> h().

minus(X, Y) ->
    Minus1 = from_z(-1),
    MinusY = times(Minus1, Y),
    plus(X, MinusY).



-spec one() -> h().
% @doc multiplicative identity

one() ->
    from_z(1).



-spec oneover(h()) -> h().
% @doc multiplicative inverse

oneover(H) ->
    {h, RQ, IQ, JQ, KQ} = conj(H),
    QuadQ  = quadrance_q(H),
    IQuadQ = qoneover(QuadQ),
    h(qtimes(RQ, IQuadQ),
      qtimes(IQ, IQuadQ),
      qtimes(JQ, IQuadQ),
      qtimes(KQ, IQuadQ)).



-spec p1(h()) -> h().
% @doc inner product of argument with one().

p1(X) ->
    ip(X, one()).



-spec pf(h()) -> iolist().
% @doc pretty format

% real
pf({h, Re, _I = {q, 0, 1}, _J = {q, 0, 1}, _K = {q, 0, 1}}) ->
    qpf(Re);
% complex ->
%   (c Re Im)
pf({h, Re, Im, _J = {q, 0, 1}, _K = {q, 0, 1}}) ->
    ["(c ", qpf(Re), " ", qpf(Im), ")"];
% quaternion ->
%   (h Re Im Jm Km)
pf({h, Re, Im, Jm, Km}) ->
    ["(h ", qpf(Re), " ",
            qpf(Im), " ",
            qpf(Jm), " ",
            qpf(Km), ")"].



-spec pi(h()) -> h().
% @doc inner product of argument with i().

pi(X) ->
    ip(X, i()).



-spec pj(h()) -> h().
% @doc inner product of argument with j().

pj(X) ->
    ip(X, j()).



-spec pk(h()) -> h().
% @doc inner product of argument with k().

pk(X) ->
    ip(X, k()).



-spec plus([h()]) -> h().
% @doc add a list of quaternions; empty -> 0

plus(Hs) ->
    plus_accum(Hs, zero()).



-spec plus(h(), h()) -> h().
% @doc add two quaternions

plus({h, R1, I1, J1, K1}, {h, R2, I2, J2, K2}) ->
    h(qplus(R1, R2),
      qplus(I1, I2),
      qplus(J1, J2),
      qplus(K1, K2)).



-spec pp(h()) -> ok.
% @doc pretty print

pp(H) ->
    ok = io:format("~ts~n", [pf(H)]),
    ok.



-spec pr(h()) -> h().
% @doc alias for p1/1

pr(X) ->
    p1(X).



-spec q(h(), h()) -> h().
% @doc divide the first argument by the second

q(T, B) ->
    times(T, oneover(B)).



-spec quadrance(h()) -> h().
% @doc quadrance of a quaternion is the sum of squares of its
% components

quadrance(H) ->
    from_q(quadrance_q(H)).



-spec quadrance_q(h()) -> q().
% @doc quadrance of a quaternion is the sum of squares of its
% components

quadrance_q({h, R, I, J, K}) ->
    qplus([qsq(R), qsq(I), qsq(J), qsq(K)]).



-spec seq(Left :: h(), Right :: h(), Step :: h()) -> Sequence :: [h()].
% @doc
% will go into an infinite loop if the Step does not evenly
% divide the difference Right - Left.

seq(Left, Right, Step) ->
    seq(Left, Right, Step, [Left]).

% when we've hit the right endpoint, reverse the accumulator
seq(Right, Right, _Step, Accum) ->
    lists:reverse(Accum);
% when we've not hit the right endpoint, do another step, add it to
% the front end of the accumulator
seq(Left, Right, Step, Accum) ->
    NewLeft = plus(Left, Step),
    NewAccum = [NewLeft | Accum],
    seq(NewLeft, Right, Step, NewAccum).



-spec seq_safe(Src, Dest, Step, Timeout) -> {ok, Sequence}
                                          | {error, timeout | term()}
        when Src      :: h(),
             Dest     :: h(),
             Step     :: h(),
             Sequence :: [h()],
             Timeout  :: pos_integer().

seq_safe(Src, Dest, Step, Timeout) ->
    Parent = self(),
    Fun =
        fun() ->
            Result = seq(Src, Dest, Step),
            Parent ! {result, Result}
        end,
    {Pid, Ref} = spawn_monitor(Fun),
    receive
        {result, Result} ->
            {ok, Result};
        {'DOWN', Ref, process, Pid, Reason} ->
            {error, Reason}
    after Timeout ->
        exit(Pid, timeout),
        {error, timeout}
    end.



-spec times([h()]) -> h().
% @doc multiply a list of quaternions; product of empty list is one()

times(Hs) ->
    times_accum(Hs, one()).



-spec times(h(), h()) -> h().
% @doc multiply two quaternions

% copying from wikipedia
times({h, A1, B1, C1, D1}, {h, A2, B2, C2, D2}) ->

    R = qplus([       qtimes(A1, A2),
               qminus(qtimes(B1, B2)),
               qminus(qtimes(C1, C2)),
               qminus(qtimes(D1, D2))]),

    I = qplus([       qtimes(A1, B2),
                      qtimes(B1, A2),
                      qtimes(C1, D2),
               qminus(qtimes(D1, C2))]),

    J = qplus([       qtimes(A1, C2),
               qminus(qtimes(B1, D2)),
                      qtimes(C1, A2),
                      qtimes(D1, B2)]),

    K = qplus([       qtimes(A1, D2),
                      qtimes(B1, C2),
               qminus(qtimes(C1, B2)),
                      qtimes(D1, A2)]),

    h(R, I, J, K).



-spec zero() -> h().
% @doc additive identity

zero() ->
    from_z(0).


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% internals
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


-spec chipsum_accum(ComplexNums :: [hc()], Accum :: hc()) -> ChipSum :: hc().
% @private
% Let Z1 = {X1, Y1} and Z2 = {X2, Y2} be arbitrary complex points.
%
% The signed area of the triangle 0 -> Z1 -> Z2 -> 0 is given by
%
%   (1 / 2) * det([[X1 Y1]
%                  [X2 Y2]])
%
% If we take the inner product of Z1 and Z2, we get the complex
% number
%
%   {(+ (* X1 X2) (* Y1 Y2),
%    (- (* X1 Y2) (* Y1 X2)}.
%
% The imaginary component of that happens to be the same formula as
% the determinant
%
% So this works as follows:
%
%   - grab first two items in list [Z1, Z2 | Rest]
%   - conjugate of the first times the second
%   - OLD: take the i-component
%     NOW: this just takes the sum; use cspline/1 to get the i
%     component
%   - divide it by two
%   - add to the accumulator
%   - call on [Z2 | Rest]

chipsum_accum([Z1, Z2 | Rest], AccumSum) ->
    chipsum_accum([Z2 | Rest], plus(AccumSum, chip(Z1, Z2)));
chipsum_accum(_, Accum) ->
    Accum.



-spec plus_accum(Hs :: [h()], Accum :: h()) -> Sum :: h().
% @private plus with an accumulator

plus_accum([], Accum) ->
    Accum;
plus_accum([X | Xs], Accum) ->
    plus_accum(Xs, plus(Accum, X)).



q0() ->
    qxq:zero().

q1() ->
    qxq:one().

qminus(Q) ->
    qxq:minus(Q).

qoneover(Q) ->
    qxq:oneover(Q).

qplus(List) ->
    qxq:plus(List).


qpf({q, Z, 1}) ->
    erlang:integer_to_list(Z);
qpf({q, T, B}) ->
    TS = erlang:integer_to_list(T),
    BS = erlang:integer_to_list(B),
    ["(q ", TS, " ", BS, ")"].


qplus(A, B) ->
    qxq:plus(A, B).


qq(X, Y) ->
    qxq:q(X, Y).

qsq(X) ->
    qxq:sq(X).

qtimes(X, Y) ->
    qxq:times(X, Y).


-spec times_accum(Hs :: [h()], Accum :: h()) -> Prod :: h().
% @private times with an accumulator, short circuits on 0.
% Does it in the correct order


% short circuit on 0
times_accum(_, Zero = {h, {q, 0, 1}, {q, 0, 1}, {q, 0, 1}, {q, 0, 1}}) ->
    Zero;
times_accum([], Accum) ->
    Accum;
times_accum([X | Xs], Accum) ->
    times_accum(Xs, times(Accum, X)).

% @doc
% matrices contain quaternions and are sparse by default
-module(qxm).

-export_type([
    idx1/0,
    rc/0,
    m/0,
    shape/0
]).
-export([
    array/1,
    shape/1,
    zeros/1
]).

-record(m,
        {shape :: shape(),
         array :: array:array(h())}).

-type idx1()  :: pos_integer().
-type h()     :: qxh:h().
-type m()     :: #m{}.
-type rc()    :: {rc, idx1(), idx1()}.
-type shape() :: {shape, pos_integer(), pos_integer()}.


%%% API

-spec array(m()) -> array:array(h()).
% @doc return underlying array of matrix

array(#m{array = A}) ->
    A.



-spec shape(m()) -> shape().
% @doc return shape of matrix

shape(#m{shape = Shape}) ->
    Shape.



-spec zeros(shape()) -> m().
% @doc construct spares matrix of given shape

zeros(Shape = {shape, NR, NC}) ->
    NumElems = NR * NC,
    Zero     = qxh:zero(),
    Array = array:new([{size, NumElems},
                       {fixed, true},
                       {default, Zero}]),
    #m{shape = Shape, array = Array}.

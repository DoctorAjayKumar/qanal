-module(qanal).

-export([
    blackfun/1,
    c/2,
    chip/2,
    chipsum/1,
    cspline/1,
    h/4,
    i/0,
    int/3,
    intnpoints/4,
    intpoints/2,
    ip/2,
    j/0,
    k/0,
    one/0,
    oneover/1,
    p1/1,
    pf/1,
    pi/1,
    pj/1,
    pk/1,
    pp/1,
    minus/1,
    minus/2,
    plus/1,
    plus/2,
    pr/1,
    q/2,
    quadrance/1,
    r/1,
    seq/3,
    seq_safe/4,
    sq/1,
    times/1,
    times/2,
    to_float/1,
    v/3,
    whiten/1,
    whites/1,
    zero/0,
    zgcd/1,
    zgcd/2,
    zmodulo/2
]).

-export_type([
    dumbnum/0,
    h/0,
    hc/0,
    hq/0,
    hr/0,
    q/0,
    z/0,
    znn/0,
    zp/0
]).

-type dumbnum() :: z() | q() | h().
-type h  ()     :: qxh:h  ().
-type hc ()     :: qxh:hc ().
-type hq ()     :: qxh:hq ().
-type hr ()     :: qxh:hr ().
-type q  ()     :: qxq:q  ().
-type z  ()     :: qxz:z  ().
-type znn()     :: qxz:znn().
-type zp ()     :: qxz:zp ().


-spec blackfun(WhiteFun) -> BlackFun
        when WhiteFun :: fun(([h()]) -> any()),
             BlackFun :: fun(([dumbnum()]) -> any()).
% @doc
% Returns a function which whitens the arguments before sending to
% the white function

blackfun(WhiteFun) ->
    fun(DumbNumList) ->
        HList = lists:map(fun whiten/1, DumbNumList),
        WhiteFun(HList)
    end.



-spec c(dumbnum(), dumbnum()) -> h().
% @doc c(A, B) -> h(A, B, 0, 0).

c(A, B) ->
    h(A, B, 0, 0).



-spec chip(dumbnum(), dumbnum()) -> h().
% @doc
% "complex half inner product"
%
% chip(X, Y) -> (1/2) * conj(X) * Y
% ip(X, Y)   -> (1/2) * [ conj(X)*Y + conj(Y)*X ]

chip(X, Y) ->
    qxh:chip(whiten(X), whiten(Y)).



-spec chipsum([dumbnum()]) -> h().
% @doc
% "complex half inner product sum"; full story behind cspline

chipsum(X) ->
    qxh:chipsum(lists:map(fun whiten/1, X)).




-spec cspline([dumbnum()]) -> hr().
% @doc signed area encloded by a list of complex numbers
%
% returns imaginary component of chipsum/1

cspline(Blacklist) ->
    Whitelist = lists:map(fun whiten/1, Blacklist),
    qxh:cspline(Whitelist).



-spec h(dumbnum(), dumbnum(), dumbnum(), dumbnum()) -> h().
% @doc h(A, B, C, D) -> A + B*i + C*j + D*k

h(A, B, C, D) ->
    plus([      whiten(A),
          times(whiten(B), i()),
          times(whiten(C), j()),
          times(whiten(D), k())]).



-spec i() -> h().
% @doc i

i() ->
    qxh:i().



-spec int(Fun, From, To) -> hq()
        when Fun    :: fun((h()) -> dumbnum()),
             From   :: dumbnum(),
             To     :: dumbnum().

int(Fun, From, To) ->
    WhiteFun = fun(H) -> whiten(Fun(H)) end,
    qpint:int(WhiteFun, whiten(From), whiten(To)).



-spec intnpoints(Fun, From, To, NPoints :: pos_integer()) -> hq()
        when Fun    :: fun((h()) -> dumbnum()),
             From   :: dumbnum(),
             To     :: dumbnum().

intnpoints(Fun, From, To, NPoints) ->
    WhiteFun = fun(H) -> whiten(Fun(H)) end,
    qpint:intnpoints(WhiteFun, whiten(From), whiten(To), NPoints).




-spec intpoints(Fun, Points) -> hq()
        when Fun    :: fun((h()) -> dumbnum()),
             Points :: [dumbnum()].

intpoints(Fun, Points) ->
    RealFun = fun(H) -> whiten(Fun(H)) end,
    RealPoints = [whiten(Pt) || Pt <- Points],
    qpint:intpoints(RealFun, RealPoints).



-spec ip(dumbnum(), dumbnum()) -> h().
% @doc
% "inner product"
%
% ap(X, Y) -> (1/2) * conj(X) * Y
% bp(X, Y) -> (1/2) * [ conj(X)*Y + X*conj(Y) ]
% ip(X, Y) -> (1/2) * [ conj(X)*Y + conj(Y)*X ]
% iq(X, Y) -> (1/4) * [ conj(X)*Y - conj(Y)*X ]

ip(A, B) ->
    qxh:ip(whiten(A), whiten(B)).



-spec j() -> h().
% @doc j

j() ->
    qxh:j().



-spec k() -> h().
% @doc k

k() ->
    qxh:k().



-spec minus(dumbnum()) -> h().

minus(D) ->
    qxh:minus(whiten(D)).



-spec minus(dumbnum(), dumbnum()) -> h().

minus(D1, D2) ->
    plus(D1, minus(D2)).



-spec one() -> h().
% @doc the multiplicative identity

one() ->
    whiten(1).



-spec oneover(dumbnum()) -> h().
% @doc the multiplicative inverse

oneover(X) ->
    q(1, X).



-spec p1(dumbnum()) -> h().
% @doc inner product of argument with one().

p1(X) ->
    ip(X, one()).



-spec pf(dumbnum()) -> iolist().
% @doc pretty format

pf(X) ->
    qxh:pf(whiten(X)).



-spec pi(dumbnum()) -> h().
% @doc inner product of argument with i().

pi(X) ->
    ip(X, i()).



-spec pj(dumbnum()) -> h().
% @doc inner product of argument with j().

pj(X) ->
    ip(X, j()).



-spec pk(dumbnum()) -> h().
% @doc inner product of argument with k().

pk(X) ->
    ip(X, k()).



-spec pp(dumbnum()) -> ok.
% @doc pretty print

pp(X) ->
    qxh:pp(whiten(X)).



-spec plus([dumbnum()]) -> h().
% @doc addition of list; empty list sums to zero().

plus(Blacklist) ->
    Whitelist = lists:map(fun whiten/1, Blacklist),
    qxh:plus(Whitelist).



-spec plus(dumbnum(), dumbnum()) -> h().
% @doc addition of dumbnums

plus(A, B) ->
    qxh:plus(whiten(A), whiten(B)).



-spec pr(dumbnum()) -> h().
% @doc alias for p1/1

pr(X) ->
    p1(X).



-spec q(dumbnum(), dumbnum()) -> h().
% @doc division of dumbnums

q(X, Y) ->
    qxh:q(whiten(X), whiten(Y)).



-spec quadrance(dumbnum()) -> hr().
% @doc quadrance

quadrance(X) ->
    qxh:quadrance(whiten(X)).



-spec r(dumbnum()) -> h().
% @doc alias for whiten; meant for loading "real" scalars
% equivalent to r(A) -> h(A, 0, 0, 0).

r(X) ->
    whiten(X).



-spec seq(dumbnum(), dumbnum(), dumbnum()) -> [h()].

seq(Src, Dest, Step) ->
    qxh:seq(whiten(Src), whiten(Dest), whiten(Step)).



-spec seq_safe(Src, Dest, Step, Timeout) -> {ok, Sequence}
                                          | {error, timeout}
        when Src      :: dumbnum(),
             Dest     :: dumbnum(),
             Step     :: dumbnum(),
             Sequence :: [h()],
             Timeout  :: pos_integer().

seq_safe(Src, Dest, Step, Timeout) ->
    Parent = self(),
    Fun =
        fun() ->
            Result = seq(Src, Dest, Step),
            Parent ! {result, Result}
        end,
    {Pid, Ref} = spawn_monitor(Fun),
    receive
        {result, Result} ->
            {ok, Result};
        {'DOWN', Ref, process, Pid, Reason} ->
            {error, Reason}
    after Timeout ->
        exit(Pid, timeout),
        {error, timeout}
    end.



-spec sq(dumbnum()) -> h().
% @doc square

sq(X) ->
    WX = whiten(X),
    times(WX, WX).



-spec times([dumbnum()]) -> h().
% @doc multiplication; whitens arguments; product of empty list is
% whiten(1)

times(Blacklist) ->
    Whitelist = lists:map(fun whiten/1, Blacklist),
    qxh:times(Whitelist).



-spec times(dumbnum(), dumbnum()) -> h().
% @doc multiplication; whitens arguments

times(A, B) ->
    qxh:times(whiten(A), whiten(B)).



-spec to_float(dumbnum()) -> float().
% @doc whiten argument, convert real component to float

to_float(DN) ->
    H = whiten(DN),
    {q, T, B} = qxh:cr(H),
    T / B.



-spec v(dumbnum(), dumbnum(), dumbnum()) -> h().
% @doc
% make a pure vector out of dumbnumbs
% v(A, B, C) -> A*i + B*j + C*k

v(A, B, C) ->
    h(0, A, B, C).



-spec whiten(dumbnum()) -> h().
% @doc typecast a number up to a quaternion

whiten(Z) when is_integer(Z) ->
    qxh:from_z(Z);
whiten(Q = {q, _, _}) ->
    qxh:from_q(Q);
whiten({h, R, I, J, K}) ->
    qxh:h(R, I, J, K).



-spec whites([dumbnum()]) -> [h()].
% @doc typecast a list of numbers up to quaternions

whites(List) ->
    lists:map(fun whiten/1, List).



-spec zero() -> h().
% @doc additive identity

zero() ->
    qxh:zero().



-spec zgcd([z()]) -> znn().
% @doc greatest common divisor of a list of erlang integers

zgcd(List) ->
    qxz:gcd(List).



-spec zgcd(z(), z()) -> znn().
% @doc greatest common divisor of two integers

zgcd(A, B) ->
    qxz:gcd(A, B).



-spec zmodulo(z(), zp()) -> znn().
% @doc zmodulo(A, B) -> "A mod B", always between 0 =< Result < B

zmodulo(A, B) ->
    qxz:modulo(A, B).

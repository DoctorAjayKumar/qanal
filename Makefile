all: emake dialyzer

emake:
	erl -make

dialyzer:
	dialyzer --src src

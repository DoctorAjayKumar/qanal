% @doc qx_q = Qanal Xanal Quotients [= rationals]
-module(qx_q).
-vsn("5.0.0").

-export_type([
    q/0
]).
-export([
    bot_z/1,
    divide/2,
    from_z/1,
    is_valid_q/1,
    is_qz/1,
    minus/1,
    minus/2,
    one/0,
    oneover/1,
    pf/1,
    plus/1,
    plus/2,
    pow/2,
    pp/1,
    q/2,
    square/1,
    times/1,
    times/2,
    top_z/1,
    to_z/1,
    zero/0
]).

-type z()   :: qx_z:z().
-type zp()  :: qx_z:zp().
-type q()   :: {q, z(), zp()}.

%%% api

-spec bot_z(q()) -> zp().
% @doc returns the bottom of a fraction.

bot_z({q, _, B}) ->
    B.



-spec divide(q(), q()) -> q().
% @doc divide the first argument by the second

divide(A, B) ->
    times(A, oneover(B)).



-spec from_z(z()) -> q().
% @doc converts an integer to a fraction

from_z(Z) when is_integer(Z) ->
    q(Z, 1).



-spec is_valid_q(any()) -> true | false.
% @doc
% Checks if the input is a valid rational; if it shape-matches
% {q, T, B}, it makes sure that Q =:= q(T, B). Otherwise returns
% false.
% @end

% The q function is defined to be correct
is_valid_q(Q = {q, T, B}) ->
    Q =:= q(T, B);
is_valid_q(_) ->
    false.



-spec is_qz(any()) -> true | false.
% @doc
% Checks if the input is a rational that corresponds to an integer
% @end

is_qz({q, _, 1}) ->
    true;
is_qz(_) ->
    false.




-spec minus(q()) -> q().
% @doc returns additive inverse of rational

minus({q, T, B}) ->
    q(-1 * T, B).



-spec minus(q(), q()) -> q().
% @doc subtracts the second argument from the first

minus(A, B) ->
    plus(A, minus(B)).



-spec one() -> q().
% @doc the multiplicative identity with rationals

one() ->
    q(1, 1).



-spec oneover(q()) -> q().
% @doc multiplicative inverse

oneover({q, T, B}) ->
    q(B, T).



-spec pf(q()) -> iolist().
% @doc pretty format

pf({q, T, B}) ->
    TS = erlang:integer_to_list(T),
    BS = erlang:integer_to_list(B),
    ["(q ", TS, " ", BS, ")"].



-spec plus([q()]) -> q().
% @doc add a list of rationals

plus(List) ->
    plus_accum(List, zero()).



-spec plus(q(), q()) -> q().
% @doc add two rationals

plus({q, TL, BL}, {q, TR, BR}) ->
    % make common denominator
    q((TL * BR) + (TR * BL),
      BR * BL).



-spec pow(q(), integer()) -> q().
% @doc raise a rational to an integer power

pow(Q, N) when is_integer(N) ->
    pow_accum(Q, N, one()).



-spec pp(q()) -> ok.
% @doc pretty print

pp(Q) ->
    ok = io:format("~ts~n", [pf(Q)]),
    ok.



-spec q(z(), z()) -> q().
% @doc form the quotient of two integers

% Cannot divide by zero
q(_, 0) ->
    error(badarg);
q(Top, Bot) when Bot < 0 ->
    q(-1 * Top, -1 * Bot);
q(Top, Bot) when 0 < Bot,
                 is_integer(Top),
                 is_integer(Bot) ->
    GCD = qx_z:gcd(Top, Bot),
    {q, Top div GCD,
        Bot div GCD}.



-spec square(q()) -> q().
% @doc square a rational

square(Q) ->
    times(Q, Q).


-spec times([q()]) -> q().
% @doc product of a list of rationals; short circuits on 0

times(Qs) ->
    times_accum(Qs, one()).



-spec times(q(), q()) -> q().
% @doc product of two rationals

times({q, TL, BL}, {q, TR, BR}) ->
    q(TL * TR,
      BL * BR).



-spec to_z({q, z(), 1}) -> z().
% @doc convert a fraction where the denominator is 1 to an integer,
% crash otherwise

to_z({q, Z, 1}) -> Z.



-spec top_z(q()) -> z().
% @doc returns the top of a fraction.

top_z({q, T, _}) ->
    T.



-spec zero() -> q().
% @doc the additive identity with rationals

zero() ->
    q(0, 1).



%%% internals

-spec plus_accum(List :: [q()], AccumSum :: q()) -> Sum :: q().
% @private
% plus with accumulator

plus_accum([], Accum) ->
    Accum;
plus_accum([X | Xs], Accum) ->
    plus_accum(Xs, plus(Accum, X)).



-spec pow_accum(q(), integer(), q()) -> q().
% @private exponentiation with an accumulator

pow_accum(_Q, 0, Accum) ->
    Accum;
pow_accum(Q, N, Accum) when 0 < N ->
    pow_accum(Q, N - 1, times(Accum, Q));
pow_accum(Q, N, Accum) when N < 0 ->
    pow_accum(Q, N + 1, divide(Accum, Q)).



-spec times_accum(List :: [q()], AccumProd :: q()) -> Prod :: q().
% @private
% times with accumulator; will short-circuit if accumulator is 0

times_accum(_, Zero = {q, 0, 1}) ->
    Zero;
times_accum([], Accum) ->
    Accum;
times_accum([X | Xs], Accum) ->
    times_accum(Xs, times(Accum, X)).

```
        Every operation, calculation, and concept, no matter how arbitrarily
        complex, reduces to adding integers together. There are no new concepts
        in QAnal. Everything is just putting lipstick on adding integers
        together.

    -- Dr. Ajay Kumar PHD, The Founder
```

# QAnal: Rational Analysis

QAnal is a fork of mathematics, implemented as an Erlang library.
This is an educational/research tool, but it is likely to be
accidentally useful.

This code is optimized for clarity and correctness, not necessarily
for "performance" (nebulous concept).

For instance, instead of floats, we have a data structure for
rational numbers. This avoids problems like this

```
6> qanal:plus(qanal:q(1, 10), qanal:q(2, 10)).
{h,{q,3,10},{q,0,1},{q,0,1},{q,0,1}}
7> 0.1 + 0.2.
0.30000000000000004
8> 0.1 + 0.2 =:= 0.3.
false
9> qanal:q(3, 10) =:= qanal:plus(qanal:q(1, 10), qanal:q(2, 10)).
true
```

It is easiest to interface with QAnal through the `qanal` module.
The `qanal` module contains "do what I mean" functions, whereas the
internal modules contain "do what I say" functions.

```
3> qanal:times(3,2).
{h,{q,6,1},{q,0,1},{q,0,1},{q,0,1}}
4> qxh:times(3,2).
** exception error: no function clause matching qxh:times(3,2) (src/qxh.erl, line 364)
```

To achieve the "do what I mean" functionality, all scalars are
typecast up to quaternions with rational components.

```
11> qanal:c(3, 4).
{h,{q,3,1},{q,4,1},{q,0,1},{q,0,1}}
12> qanal:c(qanal:q(3, 4), qanal:q(5, 6)).
{h,{q,3,4},{q,5,6},{q,0,1},{q,0,1}}
```

## Design

There are two "types" in this library

- there is one scalar type, and that is quaternions
- there is one array type, and that is matrices with entries that are
  quaternions

This is an engineering decision, not a math decision. It radically
simplifies the code, and captures all of the most common usage cases
(integers, rationals, complex numbers, and quaternions).


## Naming conventions

QAnal is split into two subfields

- XAnal = exact analysis (modules `qx*.erl`)

  XAnal governs things that can be computed "exactly", where
  approximation and precision is not a consideration.

- PreAnal = precise analysis (modules `qp*.erl`)

  PreAnal governs anything approximate, where "precision" is a
  consideration, such as Newton's method or matrix factorizations.

## Todo

- add matrices
- pretty printing and string parsing
- gaussian elimination

## Precise example with Newton's method, sqrt

```
Solve x = sqrt(a)

f(X)  = X^2 - A = 0
f'(X) = 2*X

Start at X_0 = 1 + epsilon

f(X + E) ->
    X^2 + 2*X*E + E^2 - A;
  = (X + E)^2 - A
f'(X + E) ->
    2(X + E).

f(X + E)/f'(X + E) =
    (1/2) * (X + E - A/(X + E))

X_{n+1} = X_n - f(X_n)/f'(X_n).

X_1 = (1 + E) - (
```

hmm

need sleep
